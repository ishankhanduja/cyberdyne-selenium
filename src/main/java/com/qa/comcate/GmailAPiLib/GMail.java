package com.qa.comcate.GmailAPiLib;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.jsoup.Jsoup;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.Json;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.StringUtils;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.GmailScopes;
import com.google.api.services.gmail.model.ListMessagesResponse;
import com.google.api.services.gmail.model.Message;
import com.google.api.services.gmail.model.Thread;

import io.restassured.path.json.JsonPath;


public class GMail {

	private static final String APPLICATION_NAME = "ComcateAutomation";
    private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
    private static final String USER_ID = "me";
    /**
     * Global instance of the scopes required by this quickstart.
     * If modifying these scopes, delete your previously saved tokens/ folder.
     */
    private static final List<String> SCOPES = Collections.singletonList(GmailScopes.MAIL_GOOGLE_COM);
    private static final String CREDENTIALS_FILE_PATH =  
    		System.getProperty("user.dir") +
             File.separator + "src" +
             File.separator + "main" +
             File.separator + "resources" +
             File.separator + "credentials" +
             File.separator + "credentials_new.json";
    
    private static final String TOKENS_DIRECTORY_PATH = System.getProperty("user.dir") +
            File.separator + "src" +
            File.separator + "main" +
            File.separator + "resources" +
            File.separator + "credentials";
    /**
     * Creates an authorized Credential object.
     * @param HTTP_TRANSPORT The network HTTP Transport.
     * @return An authorized Credential object.
     * @throws IOException If the credentials.json file cannot be found.
     */
    private static Credential getCredentials(final NetHttpTransport HTTP_TRANSPORT) throws IOException {
        // Load client secrets.
        InputStream in = new FileInputStream(new File(CREDENTIALS_FILE_PATH));
        if (in == null) {
            throw new FileNotFoundException("Resource not found: " + CREDENTIALS_FILE_PATH);
        }
        GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));
        // Build flow and trigger user authorization request.
        GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
                HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, SCOPES)
                .setDataStoreFactory(new FileDataStoreFactory(new java.io.File(TOKENS_DIRECTORY_PATH)))
                .setAccessType("offline")
                .build();
        LocalServerReceiver receiver = new LocalServerReceiver.Builder().setPort(9999).build();
        return new AuthorizationCodeInstalledApp(flow, receiver).authorize("user");
    }
    
    
    public static Gmail getService() throws IOException, GeneralSecurityException {
        // Build a new authorized API client service.
        final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
        Gmail service = new Gmail.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT))
                .setApplicationName(APPLICATION_NAME)
                .build();
        return service;
    }
    public static List<Message> listMessagesMatchingQuery(Gmail service, String userId,
                                                          String query) throws IOException {
        ListMessagesResponse response = service.users().messages().list(userId).setQ(query).execute();
        List<Message> messages = new ArrayList<Message>();
        while (response.getMessages() != null) {
            messages.addAll(response.getMessages());
            if (response.getNextPageToken() != null) {
                String pageToken = response.getNextPageToken();
                response = service.users().messages().list(userId).setQ(query)
                        .setPageToken(pageToken).execute();
            } else {
                break;
            }
        }
        return messages;
    }
    public static Message getMessage(Gmail service, String userId, List<Message> messages, int index)
            throws IOException {
        Message message = service.users().messages().get(userId, messages.get(index).getId()).execute();
        return message;
    }
    
    public static JsonPath GetGmailJson(String query) {
        try {
            Gmail service = getService();
            List<Message> messages = listMessagesMatchingQuery(service, USER_ID, query);
            Message message = getMessage(service, USER_ID, messages, 0);
            System.out.println(message.toString());
            JsonPath jp = new JsonPath(message.toString());
            return jp;
        }
        catch (Exception e) {
    		System.out.println("email not found....");
        throw new RuntimeException(e);
    }
    }
    
    
    public static HashMap<String, String> getGmailData(String query) {
        try {
            Gmail service = getService();
            List<Message> messages = listMessagesMatchingQuery(service, USER_ID, query);
            Message message = getMessage(service, USER_ID, messages, 0);
            System.out.println(message.toString());
            JsonPath jp = new JsonPath(message.toString());
            String subject = jp.getString("payload.headers.find { it.name == 'Subject' }.value");
            //System.out.println(subject);
       /*     String body1 = jp.getString("payload.headers.find { it.name == 'From' }.value");
            System.out.println(body1);*/
            String body = StringUtils.newStringUtf8(com.google.api.client.util.Base64.decodeBase64(jp.getString("payload.body.data")));
            /*System.out.println("Body before tostring"+ body);
            body = body.toString();
            System.out.println("Body before jsoup"+ body);
          
            body = Jsoup.parse(body).text();*/
            
            //System.out.println("body1 is *******" + body1);
            //String body = new String(Base64.getDecoder().decode(jp.getString("payload.body.data")));
            //String body = new String(Base64.getDecoder().decode(jp.getString("payload.parts[0].body.data")));
            String link = null;
            String arr[] = body.split("\n");
            for(String s: arr) {
                s = s.trim();
                if(s.startsWith("http") || s.startsWith("https")) {
                    link = s.trim();
                }
            }
            HashMap<String, String> hm = new HashMap<String, String>();
            hm.put("subject", subject);
            hm.put("body", body);
            hm.put("link", link);
            return hm;
        } catch (Exception e) {
        		System.out.println("email not found....");
            throw new RuntimeException(e);
        }
    }
    
    public static int getTotalCountOfMails() {
        int size;
        try {
            final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
            Gmail service = new Gmail.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT))
                    .setApplicationName(APPLICATION_NAME)
                    .build();
            List<Thread> threads = service.
                    users().
                    threads().
                    list("me").
                    execute().
                    getThreads();
             size = threads.size();
        } catch (Exception e) {
            System.out.println("Exception log " + e);
            size = -1;
        }
        return size;
    }
    
    public static boolean isMailExist(String messageTitle) {
        try {
            final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
            Gmail service = new Gmail.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT))
                    .setApplicationName(APPLICATION_NAME)
                    .build();
            ListMessagesResponse response = service.
                    users().
                    messages().
                    list("me").
                    setQ("subject:" + messageTitle).
                    execute();
            List<Message> messages = getMessages(response);
            return messages.size() != 0;
        } catch (Exception e) {
            System.out.println("Exception log" + e);
            return false;
        }
    }
        
        private static List<Message> getMessages(ListMessagesResponse response) {
            List<Message> messages = new ArrayList<Message>();
            try {
                final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
                Gmail service = new Gmail.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT))
                        .setApplicationName(APPLICATION_NAME)
                        .build();
                while (response.getMessages() != null) {
                    messages.addAll(response.getMessages());
                    if (response.getNextPageToken() != null) {
                        String pageToken = response.getNextPageToken();
                        response = service.users().messages().list(USER_ID)
                                .setPageToken(pageToken).execute();
                    } else {
                        break;
                    }
                }
                return messages;
            } catch (Exception e) {
                System.out.println("Exception log " + e);
                return messages;
            }
        }
    
    /*public static void main(String[] args) throws IOException, GeneralSecurityException {
        HashMap<String, String> hm = getGmailData("subject:");
        System.out.println(hm.get("subject"));
        System.out.println("=================");
        //System.out.println(hm.get("body"));
        
        String body1 = hm.get("body");
        org.jsoup.nodes.Document doc = Jsoup.parse(body1);
        org.jsoup.nodes.Element link = doc.select("a").first();
        String text = doc.body().text();
        System.out.println("Text is "+ text);

        String linkHref = link.attr("href");
        System.out.println("linkHref is "+ linkHref);
        
        String linkText = link.text();
        System.out.println("linkText is "+ linkText);

        String linkOuterH = link.outerHtml(); 
        System.out.println("linkOuterH is "+ linkOuterH);

        String linkInnerH = link.html();
        System.out.println("linkInnerH is "+ linkInnerH);

        System.out.println("=================");
        System.out.println(hm.get("link"));
        
        System.out.println("=================");
        System.out.println("Total count of emails is :"+getTotalCountOfMails());
        
        System.out.println("=================");
        boolean exist = isMailExist("new link");
        System.out.println("title exist or not: " + exist);
    }*/
        
       /*s*/
        
        public static String GetEmailBody(String EmailSubject) throws IOException, GeneralSecurityException {
            HashMap<String, String> hm = getGmailData("subject:"+EmailSubject);
            String body1 = hm.get("body");
            org.jsoup.nodes.Document doc = Jsoup.parse(body1);
            String text = doc.body().text();
            System.out.println(text);
            return text;
        }
        
        public static String GetEmailSubject(String EmailSubject) throws IOException, GeneralSecurityException {
        	HashMap<String, String> hm = getGmailData("subject:"+EmailSubject);
        	System.out.println(hm);
        	System.out.println(hm.get("subject"));
            return (hm.get("subject"));
        }
        
        public static String Get_ToAddress(String EmailSubject) throws IOException, GeneralSecurityException {
        	JsonPath jp = GetGmailJson("subject:"+EmailSubject);
        	String To_Address = jp.getString("payload.headers.find { it.name == 'Delivered-To' }.value");       	
        	System.out.println(To_Address);
            return To_Address;
        }
        
        public static String Get_FromAddress(String EmailSubject) throws IOException, GeneralSecurityException {
        	JsonPath jp = GetGmailJson("subject:"+EmailSubject);
        	String From_Address = jp.getString("payload.headers.find { it.name == 'From' }.value");       	
        	System.out.println(From_Address);
            return From_Address;
        }
        
        public String GetLinkFromEmail(String EmailSubject) throws IOException, GeneralSecurityException {
        	HashMap<String, String> hm = getGmailData("subject:"+EmailSubject);
        	String body1 = hm.get("body");
            org.jsoup.nodes.Document doc = Jsoup.parse(body1);
            org.jsoup.nodes.Element link = doc.select("a").first();
            String linkHref ="No Link Present in the email";
            try {
            	linkHref = link.attr("href");
			} catch (Exception e) {
				System.out.println(linkHref);
			}           
            System.out.println(linkHref);
            return linkHref;
        }
        
        public void Check_for_new_email() throws InterruptedException
    	{
    		int oldMailcount =  GMail.getTotalCountOfMails();
    		System.out.println(oldMailcount);
    		int newemailcount = GMail.getTotalCountOfMails();
    		System.out.println(newemailcount);
    		int count = 0;
    		while ((newemailcount != oldMailcount + 1) && count<10) {
    			count++;
    		}
    		System.out.println("New Email received");
    		System.out.println(newemailcount);
    	}
        
        
        public static String GetThreadID(String query) throws GeneralSecurityException,IOException {
            
                Gmail service = getService();
                List<Message> messages = listMessagesMatchingQuery(service, USER_ID, query);
                Message message = getMessage(service, USER_ID, messages, 0);
                return message.getThreadId();
        }
	
	
}
