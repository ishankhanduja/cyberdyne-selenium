package com.qa.comcate.GmailAPiLib;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.google.api.client.util.Base64;
import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.model.Message;

public class ReplyToEmail {

	public static void main(String[] args) throws Exception {

		Gmail service = GMail.getService();
		/*MimeMessage Mimemessage = createEmail("'support-staging@customerportal.help' <support-staging@customerportal.help>", "me", "Re: Issue # 21-22: We have received your submission", "Take 3. Thanks for the udates. This email is in reference to issue 21-22 (HcaJhB).In case of any queries, you can respond to this email and we will get back to you as soon as we can.\n" + 
				"City of Lancaster1\n" + 
				"123 Main, Lancaster, CA 53453");*/
		MimeMessage Mimemessage = createEmail("support-staging@customerportal.help", "me", "Re: Issue # 21-25: We have received your submission", "This is from inbox along with RE from eclipse with me. This email is in reference to issue 21-25 (ieYCEn). In case of any queries, you can respond to this email and we will get back to you as soon as we can. City of Lancaster1 123 Main, Lancaster, CA 53453");
		Message message = createMessageWithEmail(Mimemessage);
		/*message.setId("177d40ded084a16a");
		message.setThreadId("177d40ded084a16a");*/
		message = service.users().messages().send("me", message).execute();
		
		System.out.println("Message id "+ message.getId());
		System.out.println(message.toPrettyString());

	}
	
	/**
     * Send an email from the user's mailbox to its recipient.
     *
     * @param service Authorized Gmail API instance.
     * @param userId User's email address. The special value "me"
     * can be used to indicate the authenticated user.
     * @param emailContent Email to be sent.
     * @return The sent message
     * @throws MessagingException
     * @throws IOException
     */
    public static Message sendMessage(Gmail service,
                                      String userId,
                                      MimeMessage emailContent)
            throws MessagingException, IOException {
        Message message = createMessageWithEmail(emailContent);
        message = service.users().messages().send(userId, message).execute();

        System.out.println("Message id: " + message.getId());
        System.out.println(message.toPrettyString());
        return message;
    }
	
	/**
     * Create a MimeMessage using the parameters provided.
     *
     * @param to email address of the receiver
     * @param from email address of the sender, the mailbox account
     * @param subject subject of the email
     * @param bodyText body text of the email
     * @return the MimeMessage to be used to send email
     * @throws MessagingException
     */
    public static MimeMessage createEmail(String to,
                                          String from,
                                          String subject,
                                          String bodyText)
            throws MessagingException {
        Properties props = new Properties();
        Session session = Session.getDefaultInstance(props, null);

        MimeMessage email = new MimeMessage(session);

        email.setFrom(new InternetAddress(from));
        email.addRecipient(javax.mail.Message.RecipientType.TO,
                new InternetAddress(to));
        email.setSubject(subject);
        email.setText(bodyText);
        /*email.addHeader("format", "metadata");
        email.addHeader("metadataHeaders", "Subject,References,Message-ID");
        email.addHeader("In-Reply-To", to);*/
        return email;
    }
    
    public static Message createMessageWithEmail(MimeMessage emailContent)
            throws MessagingException, IOException {
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        emailContent.writeTo(buffer);
        byte[] bytes = buffer.toByteArray();
        String encodedEmail = Base64.encodeBase64URLSafeString(bytes);
        Message message = new Message();
        message.setRaw(encodedEmail);
        return message;
    }

    
    
}
