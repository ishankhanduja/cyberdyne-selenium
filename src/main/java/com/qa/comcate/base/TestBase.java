package com.qa.comcate.base;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;

//Retry configuration
import org.testng.IConfigurable;
import org.testng.IConfigureCallBack;
import org.testng.ITestResult;

import com.qa.Analyzer.Retriable;
import com.qa.comcate.utils.WebEventListener;

public class TestBase implements IConfigurable{

	public static WebDriver driver;
	public static Properties prop;
	public static EventFiringWebDriver e_driver;
	public static WebEventListener eventListener;

	
	@Override
	  public void run(IConfigureCallBack callBack, ITestResult testResult) {
	    Retriable retriable =
	        testResult.getMethod().getConstructorOrMethod().getMethod().getAnnotation(Retriable.class);
	    int attempts = 1;
	    if (retriable != null) {
	      attempts = retriable.attempts();
	    }
	    for (int attempt = 1; attempt <= attempts; attempt++) {	
	      callBack.runConfigurationMethod(testResult);
	      if (testResult.getThrowable() == null) {
	        break;
	      }
	    }
	  }
	
	
	
	public TestBase() {
		prop = new Properties();
		try {
			FileInputStream ip = new FileInputStream(
					System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/config/config.properties");
			try {
				prop.load(ip);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void initiate() {
		String browsername = prop.getProperty("browser");
		if (browsername.equals("Chrome")) {
			{
				// System.setProperty("webdriver.chrome.driver",
				// "src/main/java/com/qa/comcate/drivers/chromedriver");
				System.setProperty("webdriver.chrome.driver", "/usr/bin/chromedriver");
				driver = new ChromeDriver();
			}
		}

		e_driver = new EventFiringWebDriver(driver);
		// Now create object of EventListerHandler to register it with
		// EventFiringWebDriver
		eventListener = new WebEventListener();
		e_driver.register(eventListener);
		driver = e_driver;

		driver.manage().window().maximize();// setSize(new Dimension(1600, 900));
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get(prop.getProperty("url"));

	}
}
