package com.qa.comcate.pages;

import java.io.IOException;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.qa.comcate.base.TestBase;
import com.qa.comcate.utils.CommonUtils;
import com.qa.comcate.utils.SwitchWindows;

public class AgenciesLandingPage extends TestBase {

	AgenciesLandingPage AgenciesLandingPageObj;
	CommonUtils utils;
	public static String Modified_AgencyName;
	public static String ValidESRI_URL = "http://maps.cityofhenderson.com/arcgis/rest/services/public/ComcateAddressService/MapServer/0";
	public static String InvalidESRI_URL = "httpmaps.c";
	
	public static String AgencyName_ToBe_used_forFurtherCAses ;
	public static String AgencyID_ToBe_used_forFurtherCAses;
	Actions action;
	JavascriptExecutor js;
	SwitchWindows switchWindows;
	
	@FindBy(xpath = "//button[text()='Create Agency']")
	public WebElement CreateAgencyButton;

	@FindBy(xpath = "//div[@class='case-header__actions']/button[contains(text(),'Cancel')]/following-sibling::button")
	public WebElement CreateAgencyButtononPopUp;
	
	@FindBy(xpath = "//div[@role='document']/*[last()]")
	public WebElement ModalPopUpLastChild;
	
	@FindBy(xpath = "//input[@name='name']")
	public WebElement AgencyNameField;

	@FindBy(xpath = "//input[@name='streetAddress']")
	public WebElement StreeAddress;

	@FindBy(xpath = "//input[@name='city']")
	public WebElement city;

	@FindBy(xpath = "(//span[@class='Select-arrow'])[1]")
	public WebElement StateDropDown;

	@FindBy(xpath = "//div[@class='Select-menu-outer']/div/div[@class='Select-option']")
	public List<WebElement> AllStateValues;

	@FindBy(xpath = "//input[@name='zip']")
	public WebElement Zip;

	@FindBy(xpath = "(//span[@class='Select-arrow'])[2]")
	public WebElement TimezoneDropDown;

	@FindBy(xpath = "(//span[@class='Select-arrow'])[2]/../../following-sibling::div[@class='Select-menu-outer']/div/div[@class='Select-option']")
	public List<WebElement> AllTimeZoneValues;

	@FindBy(xpath = "//input[@name='email']")
	public WebElement Email;

	@FindBy(xpath = "//input[@name='websiteURL']")
	public WebElement Website_URL;

	@FindBy(xpath = "//input[@placeholder='Search by keyword']")
	public WebElement SearchBox;

	@FindBy(xpath = "//div[@class='rt-tr-group']/div/div[2]")
	public WebElement AgencyNameFromSearchResult;

	@FindBy(xpath = "//label[@class='toggle toggle--active']")
	public WebElement GisToggleOn;
	
	@FindBy(xpath = "//button[@type='button' and text()='GIS Lite']")
	public WebElement GISLite;
	
	@FindBy(xpath = "//button[@type='button' and text()='GIS Direct']")
	public WebElement GISDirect;
	
	@FindBy(xpath = "//input[@name='gisDirectURL']")
	public WebElement GISDirectURL;
	
	@FindBy(xpath = "//div[@class='gis-direct__footer']/button[text()='Save']")
	public WebElement GISDirectURLSave;
	
	@FindBy(xpath = "//span[text()='Default City']/parent::label[@class=\"toggle \"]")
	public WebElement DefaultCity_Toggle_off;

	@FindBy(xpath = "//label[@title='Send From Text Message']/ancestor::label/div/div[@class='react-toggle-track']")
	public WebElement SendFromTelephoneBumber_Toggle;
	
	@FindBy(xpath = "//input[@name='sendFromPhoneNumber']")
	public WebElement sendFromPhoneNumber;
	
	@FindBy(xpath = "//input[@name='defaultCity']")
	public WebElement Enter_default_city;

	@FindBy(xpath = "//button[text()='Add Default City']")
	public WebElement Add_Default_city;

	@FindBy(xpath = "//div[@class='agency-form__product-listing'][normalize-space()='Code Enforcement']")
	public WebElement Code_enforcement_icon;
	
	@FindBy(xpath = "//div[@class='agency-form__product-listing'][normalize-space()='CRM']")
	public WebElement CRM_icon;
	
	@FindBy(xpath = "//label[text()='Product Name']/parent::div/following-sibling::div/input")
	public WebElement ProductName;
	
	@FindBy(xpath = "//label[text()='Product Name']/parent::div/following-sibling::div/input")
	public WebElement CRMProductName;
	
	@FindBy(xpath = "//div[@class='product-form__primary-header' and text()='Customer Submission Numbering']/..//input[@type='NUMBER']")
	public WebElement CRM_number_initialization;
	
	@FindBy(xpath = "//input[@name='sendFromEmail']")
	public WebElement SendFromEmailId;
	
	
	@FindBy(xpath = "//div[@class='multi-choice-buttons']/button[text()='Inactive']")
	public WebElement InactiveButton;
	
	@FindBy(xpath = "//button[text()='Add Code Enforcement']")
	public WebElement Add_CE_Button;
	
	@FindBy(xpath = "//button[text()='Add CRM']")
	public WebElement Add_CRM_Button;

	@FindBy(xpath = "//div[contains(text(),'Data Ticket')]")
	public WebElement DataTicket_icon;
	
	
	@FindBy(xpath = "//button[@class='edit-btn  btn btn-default']")
	public WebElement Edit_agency;

	@FindBy(xpath = "(//div[@class='agency-form__product-header'])[2]/div/div/a")
	public WebElement CRM_URL;
	
	@FindBy(xpath = "(//button[text()='Next'])[1]")
	public WebElement Next_button;
	
	@FindBy(xpath = "//button[@class='edit-btn agencies-listing__agency-action-login btn btn-default']")
	public WebElement SuperadminLoginIcon;
	
	@FindBy(xpath = "//button[@class='edit-btn  btn btn-default']")
	public WebElement EditAgencyButton;
	
	@FindBy(xpath = "//div[@class='agency-form__product-header-title' and h4[text()='CRM']]/following-sibling::div//a")
	public WebElement ChromelessAppLink;
	
	@FindBy(linkText = "Customer Submissions")
	public WebElement Customer_submission_Page_Link;
	
	@FindBy(xpath = "//h5[text()='Customer Submission Numbering']//parent::div[@class='agency-form__product-config']/span")
	public WebElement Customer_submission_Numbering_text;
	
	
	@FindBy(xpath = "//img[@src='/assets/loading.gif']")
	public List<WebElement> Loader;
	

	// This is where the page is instantiated
	public AgenciesLandingPage() {
		PageFactory.initElements(driver, this);
		utils = new CommonUtils();
        js = (JavascriptExecutor) driver;
        action = new Actions(driver);
        switchWindows = new SwitchWindows();
	}
	
	public boolean createButtonIsdisplayed() {
		return CreateAgencyButton.isDisplayed();
	}

	public void ClickOnCreateAgencyButtononPopUp() {
		js.executeScript("arguments[0].scrollIntoView();", AgencyNameField);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.elementToBeClickable(CreateAgencyButtononPopUp));
		System.out.println("**"+CreateAgencyButtononPopUp.equals(driver.switchTo().activeElement()));
		action.moveToElement(CreateAgencyButtononPopUp).click().perform();
	}
	
	
	public void CreateAgencyInfo(String AgencyName, String email) {
		Modified_AgencyName = AgencyName + utils.getAlphaNumericString(4);
		AgencyName_ToBe_used_forFurtherCAses = Modified_AgencyName;
		AgencyNameField.sendKeys(Modified_AgencyName);
		StreeAddress.sendKeys("123 Lodgepole St.");
		city.sendKeys("Bend");
		// For dealing with any drop below two steps are needed because dom element
		// dissapears
		// Click on the arrow element of any drop down and then pass in outer menu
		// selector as method paramter
		StateDropDown.click();
		utils.DropDownWithoutSelectTag_ForCE(AllStateValues, "AZ (Arizona)");
		Zip.sendKeys("12345");

		TimezoneDropDown.click();
		// System.out.println(AllTimeZoneValues.size());
		utils.DropDownWithoutSelectTag_ForCE(AllTimeZoneValues, "PST");

		Email.sendKeys(email);
		Website_URL.sendKeys("www.AutomationComcate.com");
	}

	public void agency_settings_gis(String GISType, String DefaultCity_ON_OFF, String DefaultCity) {

		if (GISType.equalsIgnoreCase("Lite")) {

			GISLite.click();
			
			/*if (GisToggleOn.isDisplayed()) {
				GisToggleOn.click();
			}*/
		} 
		else if (GISType.equalsIgnoreCase("Direct")) {
			GISDirect.click();
			GISDirectURL.sendKeys(ValidESRI_URL);
			GISDirectURLSave.click();
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}		
		else {
			System.out.println("Gis Enterprise is turned on");
		}

		if (DefaultCity_ON_OFF.equalsIgnoreCase("On")) {			
			if (DefaultCity_Toggle_off.isDisplayed()) {
				DefaultCity_Toggle_off.click();
				Enter_default_city.sendKeys(DefaultCity);
				Add_Default_city.click();
			}
		} else {
			System.out.println("Default city is turned Off");
		}

	}

	//This method can be used for the toggle operations
	public void Violation_activation_setting(String NameOfViolationTobeActivated, String Toggle_ON_OFF) {
		WebElement ToggleButton = driver.findElement(
				By.xpath("//span[text()='" + NameOfViolationTobeActivated + "']/parent::label[@class=\"toggle \"]"));
		if (Toggle_ON_OFF.equalsIgnoreCase("On")) {

			if (ToggleButton.isDisplayed()) {
				ToggleButton.click();
			}
		} else {
			System.out.println(NameOfViolationTobeActivated + " is turned Off");
		}
	}
	
	
	public void template_managementSettings(String CRMSendFromId, String SendFromPhoneNumber) 
	{
		SendFromEmailId.sendKeys(CRMSendFromId);
		if (SendFromPhoneNumber.isEmpty()) {
			System.out.println("No Send from telephone configured for this agency");
		}
		else {
			SendFromTelephoneBumber_Toggle.click();
			sendFromPhoneNumber.sendKeys(SendFromPhoneNumber);
		}
	}

	public void product_configuration_CE(String Product_Name, String Active_inactive, String Custom_VT_On_Off,
			String Water_On_Off, String Power_on_off, String Animal_on_off, String General_on_off,
			String Vehicle_on_off, String Fine_on_off) {
		
		action.moveToElement(Code_enforcement_icon).click().perform();
		//Code_enforcement_icon.click();
		
		ProductName.sendKeys(Product_Name);
		
		if (Active_inactive.equalsIgnoreCase("Inactive")) {
			InactiveButton.click();
		}
		
		if (Custom_VT_On_Off.equalsIgnoreCase("on")) {
			Violation_activation_setting("Custom VT", "on");
		}
		if (Water_On_Off.equalsIgnoreCase("On")) {
			Violation_activation_setting("Water", "on");
		}
		if (Power_on_off.equalsIgnoreCase("On")) {
			Violation_activation_setting("Power", "on");
		}
		if (Animal_on_off.equalsIgnoreCase("On")) {
			Violation_activation_setting("Animal", "on");
		}
		if (General_on_off.equalsIgnoreCase("On")) {
			Violation_activation_setting("General", "on");
		}
		if (Vehicle_on_off.equalsIgnoreCase("On")) {
			Violation_activation_setting("Vehicle", "on");
		}
		if (Fine_on_off.equalsIgnoreCase("On")) {
			Violation_activation_setting("Fines Tracking", "on");
		}
		
		Add_CE_Button.click();
	}
	
	
	public void configureCRM(String CRM_name, String Active_inactive) throws InterruptedException 
	{
		//Scrolling to the bottom of the screen
		Thread.sleep(1000);
        js.executeScript("arguments[0].scrollIntoView();", DataTicket_icon);
		
		action.moveToElement(CRM_icon).click().perform();
		
		CRMProductName.sendKeys(CRM_name);
		if (Active_inactive.equalsIgnoreCase("Inactive")) {
			InactiveButton.click();
		}
		//SendFromEmailId.sendKeys(CRMSendFromId);
		CRM_number_initialization.clear();
		CRM_number_initialization.sendKeys("1");
		//Add_CRM_Button.click();
		action.moveToElement(Add_CRM_Button).click().perform();
	}

	public WebElement SearchAgency(String AgencyName) {
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		utils.WaitForElementToBeClickable(SearchBox);
		SearchBox.sendKeys(AgencyName);
		utils.WaitForElementToBePresence(AgencyNameFromSearchResult);
		return AgencyNameFromSearchResult;
	}
	
	
	
	public boolean waitForNewWindow(WebDriver driver, int timeout){
        boolean flag = false;
        int counter = 0;
        while(!flag){
            try {
                Set<String> winId = driver.getWindowHandles();
                if(winId.size() > 1){
                    flag = true;
                    return flag;
                }
                Thread.sleep(1000);
                counter++;
                if(counter > timeout){
                    return flag;
                }
            } catch (Exception e) {
                System.out.println(e.getMessage());
                return false;
            }
        }
        return flag;
    }
	
	public void SaveAgencyNAme()
	{
		utils.WriteInFile(AgencyName_ToBe_used_forFurtherCAses);;
		try {
			utils.ReadFromFile();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void SaveAgencyID()
	{
		AgencyID_ToBe_used_forFurtherCAses = driver.findElement(By.xpath("(//div[@class='rt-tr-group']/div/div)[1]")).getText();
		utils.WriteIDInFile(AgencyID_ToBe_used_forFurtherCAses);;
		try {
			utils.ReadFromFile();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	

	
	
	public DashboardPage LoginAsSuperAdmin() throws InterruptedException 
	{
        String currentHandle= driver.getWindowHandle();
        System.out.println("Current window handle "+ currentHandle);
		SuperadminLoginIcon.click();
		Assert.assertTrue(waitForNewWindow(driver,10), "New window is not opened");
		Set<String> handles=driver.getWindowHandles();
		System.out.println("all handles "+ handles);
        for(String actual: handles)
        {           
         if(!actual.equalsIgnoreCase(currentHandle))
         {
             //switching to the opened tab
             driver.switchTo().window(actual);
             System.out.println(driver.getCurrentUrl()); 
             //Thread.sleep(3000);
             utils.waitForLoader(Loader);
         }
        }
		return new DashboardPage();

		
	}
	
	public ChromelessAppPage NavigateToChromlessApp() throws InterruptedException 
	{
		EditAgencyButton.click();
		Thread.sleep(2000);
        String currentHandle= driver.getWindowHandle();
        System.out.println("Current window handle "+ currentHandle);
		ChromelessAppLink.click();
		Assert.assertTrue(waitForNewWindow(driver,10), "New window is not opened");
		Set<String> handles=driver.getWindowHandles();
		System.out.println("all handles "+ handles);
        for(String actual: handles)
        {           
         if(!actual.equalsIgnoreCase(currentHandle))
         {
             //switching to the opened tab
             driver.switchTo().window(actual);
             System.out.println(driver.getCurrentUrl()); 
             Thread.sleep(3000);
         }
        }
		return new ChromelessAppPage();

		
	}
	
	public void open_App_and_CRM() throws InterruptedException 
	{
		SuperadminLoginIcon.click();
		switchWindows.SwitchBytitle("Dashboard");
		utils.waitForLoader(Loader);
		switchWindows.SwitchBytitle("Comcate Admin");
		//System.out.println(driver.getTitle());
		Edit_agency.click();
		ChromelessAppLink.click();	
		/*switchWindows.SwitchBytitle("Customer Relationship Management");
		//System.out.println(driver.getTitle());
		switchWindows.SwitchBytitle("Dashboard");
		*///System.out.println(driver.getTitle());
	}
	
	public CSLP_Page NavigateToCSLP() throws InterruptedException
	{
		Customer_submission_Page_Link.click();
		utils.waitForLoader(Loader);
		return new CSLP_Page();
		
	}
	
	public String GetCustomer_submission_numbering() throws InterruptedException
	{
		switchWindows.SwitchBytitle("Comcate Admin");
		EditAgencyButton.click();
		Thread.sleep(2000);
		String Submission_numbering = Customer_submission_Numbering_text.getText();
		switchWindows.SwitchBytitle("Customer Submissions Page");
		return Submission_numbering;
		
	}

}
