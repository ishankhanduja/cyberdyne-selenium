package com.qa.comcate.pages;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.qa.comcate.GmailAPiLib.GMail;
import com.qa.comcate.base.TestBase;
import com.qa.comcate.utils.CommonUtils;
import com.qa.comcate.utils.SwitchWindows;

public class AgencySetupPage extends TestBase {
	CommonUtils utils;
	Actions action;
	JavascriptExecutor js;
	LoginPage LoginPageobj;
	AgenciesLandingPage agenciesLandingPageObj;
	SwitchWindows switchWindowObj;
	GMail gmailobj;

	@FindBy(xpath = "//div[@class='user-form__header full-page-modal__header']")
	public WebElement CancelButton;
	
	
	@FindBy(xpath = "//h6[text()='USERS & GROUPS']//parent::div/div/label[text()='Users']")
	public WebElement LeftSideBar_Users;

	@FindBy(xpath = "//h6[text()='AGENCY SETTINGS']//parent::div/div/label[text()='Contacts']")
	public WebElement LeftSideBar_Contacts;
	
	@FindBy(xpath = "//h6[text()='AGENCY SETTINGS']//parent::div/div/label[text()='Locations & Maps']")
	public WebElement LeftSideBar_LocationsAndMaps;
	
	//Create users
	@FindBy(xpath = "//div[@class='agency-setup-tab__section__header']/button[text()='Create User']")
	public WebElement CreateUser;
	
	@FindBy(xpath = "//div[@class='user-form__header full-page-modal__header']/div/button[text()='Create User']")
	public WebElement CreateUserPopUpButton;
	
	@FindBy(name = "firstName")
	public WebElement UserName;

	@FindBy(name = "lastName")
	public WebElement UserLastName;

	@FindBy(name = "title")
	public WebElement UserTitle;
	
	@FindBy(name = "email")
	public WebElement UserEmail;	
	
	@FindBy(name = "phone")
	public WebElement Userphone;
	
	@FindBy(xpath = "//label[text()='Site Admin?']//parent::div/following-sibling::div/div/button[text()='Yes']")
	public WebElement SiteAdminYes;
	
	@FindBy(xpath = "//label[text()='Status']//parent::div/following-sibling::div/div/button[text()='Inactive']")
	public WebElement StatusInactive;
	
	@FindBy(xpath = "(//label[text()='Product Admin?']//parent::div/following-sibling::div/div/button[text()='Yes'])[1]")
	public WebElement CEProductAdminYes;
		
	//Map Setting
	
	@FindBy(xpath = "//div[@class='file-upload']")
	public WebElement Parcel_Filename;
	
	@FindBy(xpath = "//img[@src='/assets/loading.gif']")
	public List<WebElement> Loader;

	@FindBy(xpath = "//div[@class='tab-name ' and text()='Map Settings']")
	public WebElement MapSettingParcelLink;
	
	@FindBy(xpath = "//h4[text()='Agency Parcel File']//parent::div/div/div/label/input[@id='file-upload__layer']")
	public WebElement UploadAgencyParcel;
	
	@FindBy(xpath = "//label[@class='file-upload__refresh-message']/a[text()='Refresh']")
	public WebElement RefreshMessage;
	
	@FindBy(xpath = "//div[@class='file-upload']/label/b")
	public WebElement NameOfParcelUploaded;
	
	@FindBy(name = "centerLongitude")
	public WebElement EntercenterLongitude;
	
	@FindBy(name = "centerLatitude")
	public WebElement EntercenterLatitude;


	//=================CRM 
	@FindBy(xpath = "//h1")
	public WebElement ModalHeader;
	
	
	@FindBy(xpath = "//h6[text()='PRODUCT SETTINGS']//parent::div/div/label[text()='CRM']")
	public WebElement LeftSideBar_CRM;

	@FindBy(xpath = "//label[@class='font--dark sidebar__options-container__sub-option' and text()='Categories']")
	public WebElement CRM_Categories;
	
	@FindBy(xpath = "//h6[text()='AGENCY SETTINGS']//parent::div/div/label[text()='Template Management']")
	public WebElement CRM_EmailSettings;
	
	@FindBy(xpath = "//div[text()='Submission Received']/../div[5]/button")
	public WebElement Edit_SubmissionReceived_template;
	
	@FindBy(xpath = "//div[text()='Submission Closed']/../div[5]/button")
	public WebElement Edit_SubmissionClosed_template;
	
	@FindBy(xpath = "//div[text()='Notice Issued']/../div[5]/button")
	public WebElement Edit_NoticeIssued_template;
	
	@FindBy(xpath = "//button[text()='Active']")
	public WebElement Active_button;
		
	@FindBy(xpath = "//button[text()='Save']")
	public WebElement save_button;
	
	@FindBy(xpath = "(//div[@class='agency-setup-tab__section__header'])[1]")
	public WebElement Section_header_active;
	
	
	@FindBy(xpath = "//label[@class='font--dark sidebar__options-container__sub-option' and text()='Submission Settings']")
	public WebElement CRM_SubmissionSettings;
	
	@FindBy(xpath = "//button[text()='Create Category']")
	public WebElement CreateCategoryButton;
	
	@FindBy(xpath = "//div[@class='full-page-modal__header']/div/button[text()='Create Category']")
	public WebElement CreateCategoryButtononPopUp;
	
	//Create Category
	@FindBy(name = "name")
	public WebElement Cat_Name;
	
	@FindBy(xpath="//input[@name='name']/parent::div/following-sibling::span")
	public WebElement Cat_Name_Error_message;
	
	@FindBy(xpath="//input[@name='name']/parent::div/following-sibling::span")
	public WebElement Cat_DefaultAssignee_Error_message;
	
	@FindBy(id="location-required")
	public WebElement LocationRequired_checkbox;
	
	@FindBy(xpath = "(//span[@class='Select-arrow'])[1]")
	public WebElement Default_assignee_DropDown;
	
	@FindBy(xpath = "//div[@role='option']")
	public List<WebElement> AllAsignees;
	
	@FindBy(xpath = "//label[@title='Default Assignee']/parent::div/following-sibling::div//span[@class='Select-arrow']")
	public WebElement AllassigneeArrow;
	
	@FindBy(xpath = "//div[@class='Select-menu-outer']/div/div[@class='Select-option']")
	public List<WebElement> AllAsigneesValues;
		
	@FindBy(xpath = "(//span[@class='Select-arrow'])[2]")
	public WebElement SelectCollaberators_DropDown;
	
	@FindBy(xpath = "//li[@class='multi']")
	public List<WebElement> AllCollaborators;
	
	@FindBy(xpath = "//div[@class='full-page-modal__header']/div/button[text()='Create Category']")
	public WebElement CreateCategoryPopUpButton;
	
	@FindBy(xpath = "//div[@class='full-page-modal__header']/div/button[text()='Save']")
	public WebElement EditCategory_Save;
	
	@FindBy(xpath = "//li[@class='sortable__element']")
	public List<WebElement> AllCategories;
	
	@FindBy(xpath = "//input[@name='tags-input']")
	public WebElement Keywords;
	
	@FindBy(xpath = "//button[@class='chip__close-button']")
	public WebElement KeywordsCrossIcon;

	@FindBy(xpath = "(//p[@class='tags-input__notes'])[2]")
	public WebElement KeywordsInfoNotes;
	
	@FindBy(xpath = "//div[@class='rt-table']//div[@class='sortable__drag-handle']")
	public List<WebElement> SortableItemsInInactive;
		
	@FindBy(xpath = "//div[@class='sortable__element sortable__element--fixed']//div[@class='sortable-table__body__td'][1]")
	public WebElement OtherCategoryName;
	
	@FindBy(xpath = "//div[@class='sortable-table__body__td'][1]")
	public List<WebElement> List_of_Active_category_names;
	
	@FindBy(linkText = "Management Tools")
	public WebElement Management_tools_Link;
	
	@FindBy(xpath = "//input[@type='password' and @name='password']")
	public WebElement EnterPassword;
	
	@FindBy(xpath = "//input[@type='password' and @name='renteredPassword']")
	public WebElement ReEnterPassword;
	
	
	@FindBy(xpath = "//button[@type='button' and text()='Setup Your Password']")
	public WebElement PasswordButton;
	
	@FindBy(xpath = "//div[@class='notification notification-success notification-visible']/div[@class='success-custom-message']")
	public WebElement PasswordSuccessMessage;

	//Header section
	@FindBy(xpath = "//div[@class='app-header__user-name']")
	public WebElement AppHeader_username;
	
	@FindBy(xpath = "//div[@class='app-header__user-actions']/label[text()='Logout']")
	public WebElement Logout_button;
	
	// groups 
	@FindBy(xpath = "//div[@class='dropdown__selector__selected']")
	public WebElement AddGroups_dropDown;
	
	@FindBy(xpath = "//button[text()='Apply']")
	public WebElement AddGroups_ApplyButton;
	
	
	public AgencySetupPage() {
		PageFactory.initElements(driver, this);
		utils = new CommonUtils();
		js = (JavascriptExecutor) driver;
		action = new Actions(driver);
		LoginPageobj = new LoginPage();
		agenciesLandingPageObj = new AgenciesLandingPage();
		switchWindowObj = new SwitchWindows();
		gmailobj = new GMail();
	}

	public void ClickOnMultiChoiceButton(String NameOfMultichoicebutton,String OptionTobeSelected) 
	{
		WebElement MultiChoiceButton = driver.findElement(
				By.xpath("//label[text()='" + NameOfMultichoicebutton + "']/parent::div/following-sibling::div/div/button[text()='" + OptionTobeSelected + "']"));
		MultiChoiceButton.click();
	}
		
	public void ClickonUsers() throws InterruptedException {

		action.moveToElement(LeftSideBar_Users).click().perform();
		System.out.println("clicked on LeftSideBar_Users");
		utils.waitForLoader(Loader);
	}

	public void ClickonContacts() {

		action.moveToElement(LeftSideBar_Contacts).click().perform();
		System.out.println("clicked on LeftSideBar_Contacts");
	}
	
	public void ClickonCreateUser() {
		action.moveToElement(CreateUser).click().perform();
		System.out.println("clicked on CreateUser");
	}
	
	public void ClickonLocationsAndMaps() throws InterruptedException {
		action.moveToElement(LeftSideBar_LocationsAndMaps).click().perform();
		System.out.println("clicked on LeftSideBar_Location and Maps");
		utils.waitForLoader(Loader);
	}
	
	public void ClickonMapSettings() throws InterruptedException 
	{
		action.moveToElement(MapSettingParcelLink).click().perform();
		System.out.println("clicked on MapSettingParcelLink");
		utils.waitForLoader(Loader);
	}

	public void UploadParcel() throws InterruptedException 
	{
		UploadAgencyParcel.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/Texas_City.gdb.zip");
		System.out.println("clicked on MapSettingParcelLink");
		//utils.waitForLoader(Loader);*/
		Thread.sleep(15000);
		while (RefreshMessage.isDisplayed()) {
			RefreshMessage.click();		
		}
		Assert.assertEquals(NameOfParcelUploaded.getText(), "Texas_City.gdb.zip");
	}
	
	public void EnterLatLong(String Latitude, String Longitude)
	{
		EntercenterLatitude.sendKeys(Latitude);
		EntercenterLongitude.sendKeys(Longitude);
	}
	
	
	public void DummyPrint(){
		System.out.println("Dummy print ");
	}
	
	public void CreateUserPersonalinfo(String Firstname,String Lastname,String Title,String Email, String Phone) {
		UserName.sendKeys(Firstname);
		UserLastName.sendKeys(Lastname);
		UserTitle.sendKeys(Title);
		UserEmail.sendKeys(Email);
		Userphone.sendKeys(Phone);	
	}
	
	public void SelectGroups(String Group_Name) {
		AddGroups_dropDown.click();
		driver.findElement(By.xpath("//label[@class='checkbox__content htmlfor--enabled' and text()='" + Group_Name + "']//parent::span/input")).click();
		AddGroups_ApplyButton.click();
	}
	
	public void UserSiteSetting(String IsSiteAdmin_yes_no, String UserStatus_Active_Inactive) 
	{
		if (IsSiteAdmin_yes_no.equalsIgnoreCase("Yes")) {
			SiteAdminYes.click();
		}		
		if (UserStatus_Active_Inactive.equalsIgnoreCase("Inactive")) {
			StatusInactive.click();
		}
	}
	
	//Not needed i guess
	public void ProductSettingCE(String CE_On_off) 
	{
		agenciesLandingPageObj.Violation_activation_setting("CE", CE_On_off);
		
	}
	
	public void ClickOnCreateUserPopUpButton() throws InterruptedException 
	{
		
		js.executeScript("arguments[0].scrollIntoView();", CancelButton);
		if (CreateUserPopUpButton.isDisplayed() && CreateUserPopUpButton.isEnabled()) {
			js.executeScript("arguments[0].click();", CreateUserPopUpButton);
		} else {
			System.out.println("Something wrong with the create user button");
		}
		
		System.out.println("Clicked on CreateUser popup");
		utils.waitForLoader(Loader);
		/*
		if (CreateUserPopUpButton.isEnabled()) {
			System.out.println(CreateUserPopUpButton.getText());
			action.moveToElement(CreateUserPopUpButton).click().perform();
		}
		else {
			System.out.println("CreateUserPopUpButton not displayed");
		}*/
		
	}
	
	public void clickOnCRMCategories() throws InterruptedException 
	{
		LeftSideBar_CRM.click();
		CRM_Categories.click();
		utils.waitForLoader(Loader);
	}
	

	public void clickOnCRMEmailSettings() throws InterruptedException 
	{
		//LeftSideBar_CRM.click();
		CRM_EmailSettings.click();
		utils.waitForLoader(Loader);
	}
	
	public void Make_submissionReceived_template_active() throws InterruptedException 
	{
		Edit_SubmissionReceived_template.click();
		utils.waitForLoader(Loader);
		Active_button.click();
		save_button.click();
		utils.waitForLoader(Loader);

	}
	
	public void Make_submissionClosed_template_active() throws InterruptedException 
	{
		Edit_SubmissionClosed_template.click();
		utils.waitForLoader(Loader);
		Active_button.click();
		save_button.click();
		utils.waitForLoader(Loader);
	}
	
	public void Make_NoticeIssued_template_active() throws InterruptedException 
	{
		Edit_NoticeIssued_template.click();
		utils.waitForLoader(Loader);
		Active_button.click();
		save_button.click();
		utils.waitForLoader(Loader);
	}
	
	public void clickOnCRMSubmissionSettings() throws InterruptedException 
	{
		LeftSideBar_CRM.click();
		CRM_SubmissionSettings.click();
		utils.waitForLoader(Loader);
	}
	
	public void CreateCategory(String CategoryNAme, String IncludeLocation, String Location_required_yes_no, String Default_assignee, String Status, String Collaberator, String Keys) throws InterruptedException 
	{
		CreateCategoryButton.click();
		Cat_Name.sendKeys(CategoryNAme);
		ClickOnMultiChoiceButton("Include Location?", IncludeLocation);
		if (IncludeLocation.equalsIgnoreCase("yes") && Location_required_yes_no.equalsIgnoreCase("yes")) {
			LocationRequired_checkbox.click();
		}
		Default_assignee_DropDown.click();
		utils.DropDownWithoutSelectTag_ForCE(AllAsignees, Default_assignee);
		ClickOnMultiChoiceButton("Status", Status);
		SelectCollaberators_DropDown.click();
		utils.DropDownWithoutSelectTag_ForCE(AllCollaborators, Collaberator);
		Keywords.click();
		Keywords.sendKeys(Keys);
		CreateCategoryPopUpButton.click();
		utils.waitForLoader(Loader);
	}
	
	
	public void CreateCategoryByDeletingKeywords(String CategoryNAme, String IncludeLocation, String Default_assignee, String Status, String Collaberator, String Keys) throws InterruptedException 
	{
		CreateCategoryButton.click();
		Cat_Name.sendKeys(CategoryNAme);
		ClickOnMultiChoiceButton("Include Location?", IncludeLocation);
		Default_assignee_DropDown.click();
		utils.DropDownWithoutSelectTag_ForCE(AllAsignees, Default_assignee);
		ClickOnMultiChoiceButton("Status", Status);
		SelectCollaberators_DropDown.click();
		utils.DropDownWithoutSelectTag_ForCE(AllCollaborators, Collaberator);
		Keywords.click();
		Keywords.sendKeys(Keys);
		KeywordsInfoNotes.click();
		KeywordsCrossIcon.click();
		CreateCategoryPopUpButton.click();
		utils.waitForLoader(Loader);
	}
	
	
	
	public void EditCategory(String CategoryNAme, String IncludeLocation, String Default_assignee, String Status, String Collaberator, String Keys) throws InterruptedException 
	{
		Cat_Name.clear();
		Cat_Name.sendKeys(CategoryNAme);
		ClickOnMultiChoiceButton("Include Location?", IncludeLocation);
		Default_assignee_DropDown.click();
		utils.DropDownWithoutSelectTag_ForCE(AllAsignees, Default_assignee);
		ClickOnMultiChoiceButton("Status", Status);
		SelectCollaberators_DropDown.click();
		utils.DropDownWithoutSelectTag_ForCE(AllCollaborators, Collaberator);
		Keywords.click();
		Keywords.sendKeys(Keys);
		EditCategory_Save.click();
		utils.waitForLoader(Loader);
	}
	
	public void CheckForLocationRequiredCheckBox(String CategoryNAme, String IncludeLocation) throws InterruptedException 
	{
		CreateCategoryButton.click();
		Cat_Name.sendKeys(CategoryNAme);
		ClickOnMultiChoiceButton("Include Location?", IncludeLocation);
		CreateCategoryPopUpButton.click();
		utils.waitForLoader(Loader);
	}
	
	
	
	//This method would fetch the details of the last category from active categories
	public List<String> validateCategory() 
	{
		int CountOFCategories = AllCategories.size();
		List<String> CatDetails = new ArrayList<String>();
		for (int i = 1; i <= 4; i++) {
			String CategoryDetail = driver.findElement(By.xpath("(//li[@class='sortable__element'])[" + (CountOFCategories) + "]/div[2]/div[@class='sortable-table__body__td']["+ i +"]")).getText();
			CatDetails.add(CategoryDetail);
		}
		System.out.println(CatDetails);
		return CatDetails;
	}
	
	public List<String> ClickON_edit_icon() 
	{
		int CountOFCategories = AllCategories.size();
		List<String> CatDetails = new ArrayList<String>();
		for (int i = 1; i <= 4; i++) {
			String CategoryDetail = driver.findElement(By.xpath("(//li[@class='sortable__element'])[" + (CountOFCategories) + "]/div[2]/div[@class='sortable-table__body__td']["+ i +"]")).getText();
			CatDetails.add(CategoryDetail);
		}
		System.out.println(CatDetails);
		return CatDetails;
	}
	
	public void ClickonEditIconforSortableelement(String NameOfField, String NumberOfColumns) throws InterruptedException 
	{
		WebElement EditIcon = driver.findElement(
				By.xpath("//div[@class='sortable-table__body__td' and text()='" + NameOfField + "']//parent::div//div[" + NumberOfColumns + "]/button"));
		EditIcon.click();
		utils.waitForLoader(Loader);
	}
	
	
	public List<WebElement> GetListOfAllAssignees() throws InterruptedException 
	{
		return utils.FetchListFromDropDown(AllAsigneesValues);
	}
	
	public int ListOfSortableItemsIn_InactiveCat() 
	{
		return SortableItemsInInactive.size();
	}
	
	public int AllActiveCategories() 
	{
		return SortableItemsInInactive.size();
	}
	
	public List<WebElement> Get_List_of_all_categories_on_cslp ()  throws InterruptedException
	{
		return List_of_Active_category_names;
		
	}
	
	public String SetPassword() throws GeneralSecurityException,IOException,InterruptedException
	{
		String Set_Password_Link = gmailobj.GetLinkFromEmail("");
		switchWindowObj.SwitchBytitle("Comcate Admin");
		String selectLinkOpeninNewTab = Keys.chord(Keys.CONTROL,Keys.RETURN);
		Management_tools_Link.sendKeys(selectLinkOpeninNewTab);
		Thread.sleep(2000);
		switchWindowObj.SwitchByUrl("https://admin-qa-new.comcate.com/tools/violation-type");
		driver.get(Set_Password_Link);
		EnterPassword.sendKeys("Test@123");
		ReEnterPassword.sendKeys("Test@123");
		PasswordButton.click();
		System.out.println("Clicked on Set Password");
		Thread.sleep(2000);
		System.out.println(PasswordSuccessMessage.getText());
		return PasswordSuccessMessage.getText();
		
	}
	
	public AgenciesLandingPage Login_with_user(String username, String password)  throws InterruptedException
	{
		AppHeader_username.click();
		Logout_button.click();
		/*LoginPageobj.LoginEmailField.sendKeys(username);
		LoginPageobj.LoginPasswordField.sendKeys(password);*/
		LoginPageobj.OktaLoginEmailField.sendKeys(username);
		Thread.sleep(2000);
		LoginPageobj.OktaLoginEmailField.sendKeys(username);
		LoginPageobj.OktaLoginPasswordField.sendKeys(password);
		//LoginPageobj.Loginbutton.click();
		LoginPageobj.OktaLoginbutton.click();
		utils.waitForLoader(Loader);
		return new AgenciesLandingPage();
	}
	
	
	
	
}
