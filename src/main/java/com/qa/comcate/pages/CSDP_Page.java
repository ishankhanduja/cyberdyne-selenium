package com.qa.comcate.pages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.comcate.base.TestBase;
import com.qa.comcate.utils.CommonUtils;

public class CSDP_Page extends TestBase {
	CommonUtils utils;
	Actions action;
	JavascriptExecutor js;
	AgenciesLandingPage agenciesLandingPageObj;
	
	@FindBy(xpath = "//div[@class='customer-submission-details__header__main']")
	public WebElement CSDP_PageHeader;
	
	@FindBy(xpath = "//label[@class='field__label-label' and text()='Customer Name']//following-sibling::label")
	public WebElement CSDP_Customer_name;
	
	@FindBy(xpath = "//div[@class='grid-4-cols value']")
	public WebElement CSDP_Description;
	
	@FindBy(xpath = "//label[@class='field__label-label' and text()='Collaborator/s']//parent::div//li[@class='selected']//label")
	public WebElement Checked_collaberator_name;
	
	@FindBy(xpath = "//label[@class='field__label-label' and text()='Collaborator/s']//parent::div//li[@class='selected']")
	public List<WebElement> Checked_collaberator_Checkbox;
	
	@FindBy(xpath = "//div[@class='customer-submission-details__photos']")
	public WebElement Photos_Tile;
	
	@FindBy(xpath = "//div[@class='attachments-tile__photo-container']/div/a")
	public List<WebElement> Photos_Thumbnails;
	
	@FindBy(xpath = "//span[contains(text(), 'Attachments')]")
	public WebElement Photo_tileHeader;
	
	@FindBy(xpath = "//span[contains(text(), 'Attachments')]//parent::h2/div/button[@class='attach-btn btn btn-default']")
	public WebElement Upload_photos_CSDP;
	
	@FindBy(xpath = "//input[@id='file-upload']")
	public WebElement Browse_file;
	

	@FindBy(xpath = "//button[@class='square-btn btn btn-primary' and text()='Add']")
	public WebElement Add_photos_button;
	
	@FindBy(xpath = "//div[@class='notification notification-error notification-visible']/div")
	public WebElement Notification_Message_forPhotos;
	
	@FindBy(xpath = "//span[contains(text(), 'Attachments')]//parent::h2/div/button")
	public WebElement Add_photos_button_photo_tile;
	
	@FindBy(xpath = "//div[@class='attachments-edit__body']")
	public WebElement Attachment_Edit_body;
	
	@FindBy(xpath = "//button[@class='delete-btn btn btn-default']")
	public List<WebElement> Delete_buttons_for_photos;
	
	@FindBy(xpath = "//div[@class='icon-count attachment-count']")
	public WebElement countOF_Photo_icon_in_Header;
	
	@FindBy(xpath = "//button[@class='square-btn btn btn-danger']")
	public WebElement RedButton_Yes;
	
	@FindBy(xpath = "//button[@class='square-btn btn btn-default' and text()='Do not update']")
	public WebElement Do_not_update;
	
	@FindBy(xpath = "//button[@class='square-btn btn btn-primary' and text()='Close']")
	public WebElement Photo_modal_close_button;
	
	//Email Tiles
	@FindBy(id = "cs-emails")
	public List<WebElement> Email_tile;
	
	@FindBy(xpath = "//div[@class='email-item']")
	public List<WebElement> List_of_emails;
	
	@FindBy(xpath = "//div[@class='email-item__actions']")
	public List<WebElement> List_of_Email_Checkbox;
	
	@FindBy(xpath = "//div[@class='email-item__content']")
	public List<WebElement> List_of_Email_Content;	
	
	@FindBy(xpath = "//div[@class='email-item__content__info']/div[@class='email-item__content__info__date']")
	public List<WebElement> List_of_Email_Dates;	
	
	@FindBy(xpath = "//div[@class='email-item__body']//a[text()='more']")
	public List<WebElement> List_of_More_links;	
	
	@FindBy(xpath = "//div[@class='email-item__body']//a[text()=' less']")
	public List<WebElement> List_of_Less_links;	
			
	@FindBy(xpath = "//div[@class='flex-row--center email-tile__header']/h2")
	public WebElement Email_tile_Header;
	
	
	//=============Expand Emails =========
	@FindBy(xpath = "//button[@class='attach-btn expand-email-icon action-btn btn btn-default']")
	public WebElement Email_Expand_icon;
	
	@FindBy(xpath = "//div[@class='email-expanded__body']//input[@name='subject']")
	public WebElement Email_subject_On_Email_popup_CSDP;
	
	@FindBy(xpath = "//div[@class='email-expanded__body']//textarea[@name='body']")
	public WebElement Email_Body_On_Email_popup_CSDP;
	
	@FindBy(xpath = "//div[@class='email-expanded__body']//button[text()='Send']")
	public WebElement Email_SendButton_On_Email_popup_CSDP;
	
	@FindBy(xpath = "//div[@class='email-expanded__body']//button[text()='Cancel']")
	public WebElement Email_CancelButton_On_Email_popup_CSDP;
	
	@FindBy(xpath = "//div[@class='email-expanded__body']//input[@name='subject']//ancestor::div[@class='field-section field-section--error email-form__body__subject col-md-12 col-xs-12']//span[@class='field__error']")
	public WebElement Email_SubjectErrorMsg_On_Email_popup_CSDP;
	
	@FindBy(xpath = "//div[@class='email-expanded__body']//div[@class='field-section field-section--error email-form__body__body col-md-12 col-xs-12']/span")
	public WebElement Email_BodyErrorMsg_On_Email_popup_CSDP;

	@FindBy(xpath = "//span[@class='recipient-email__value']")
	public WebElement Receipient_email;
	
	//===================Reply Email=============	
	
	@FindBy(xpath = "//div[@class='email-form__body__subject__input']/input")
	public WebElement Reply_email_Subject;
	
	@FindBy(xpath = "//textarea[@placeholder='Type your reply here']")
	public WebElement Reply_email_Body;
	
	
	
	@FindBy(xpath = "//div[@class='email-form__footer__form-actions']/button[text()='Send']")
	public WebElement Reply_email_Send_button;
	
		
	//===================
	@FindBy(xpath = "//button[@class='square-btn btn btn-primary' and text()='Close Submission']")
	public WebElement Close_Submission_button;
	
	@FindBy(xpath = "//button[@class='square-btn btn btn-primary' and text()='Reopen Submission']")
	public WebElement Reopen_Submission_button;
	
	@FindBy(xpath = "//div[@class='customer-submission-details__close-icon']/img")
	public WebElement Issue_cross_icon;
	
	
	@FindBy(xpath = "//img[@src='/assets/loading.gif']")
	public List<WebElement> Loader;
	
	@FindBy(xpath = "//label[text()='Assigned to']//parent::div//ul[@class='dropdown__options--single']/li")
	public List<WebElement> AllAsigneesFromDropDown;
	
	@FindBy(xpath = "//label[text()='Category']//parent::div//ul[@class='dropdown__options--single']/li")
	public List<WebElement> AllCategoriesFromDropdown;
	
	@FindBy(xpath = "//label[text()='Collaborator/s']//parent::div//ul[@class='dropdown__options--single']/li")
	public List<WebElement> AllCollaberatrosFromDropDown;
			
	public CSDP_Page() {
		PageFactory.initElements(driver, this);
		utils = new CommonUtils();
		js = (JavascriptExecutor) driver;
		action = new Actions(driver);
		agenciesLandingPageObj = new AgenciesLandingPage();
	}

	
	public void Delete_photo() throws InterruptedException
	{
		Photos_Thumbnails.get(0).click();
		Thread.sleep(2000);
		Delete_buttons_for_photos.get(0).click();
		RedButton_Yes.click();
		utils.waitForLoader(Loader);
		Photo_modal_close_button.click();
	}
	
	public void UploadSinglePhotos_OnCSDP() throws InterruptedException
	{
		Upload_photos_CSDP.click();
		Browse_file.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm2.jpg");
		utils.waitForLoader(Loader);
		utils.WaitForElementToBeClickable(Add_photos_button);
		Add_photos_button.click();
		utils.waitForLoader(Loader);
		
	}
	
	public String UploadSinglePhoto_of_jpeFormat_OnCSDP() throws InterruptedException
	{
		Upload_photos_CSDP.click();
		Browse_file.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/Invalid_file_data_ticket");
		System.out.println(Notification_Message_forPhotos.getText());
		return Notification_Message_forPhotos.getText();

		
	}
	
	public String UploadSinglePhoto_of_15MB_OnCSDP() throws InterruptedException
	{
		Upload_photos_CSDP.click();
		Browse_file.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/File_Greater_than15MB.jpg");
		Thread.sleep(500);
		return Notification_Message_forPhotos.getText();
		
	}
	
	public void OpenNewTab() throws InterruptedException
	{
		js.executeScript("window.open()");

		
	}
	


/*	

	public String Get_notification_Message_for_photos() throws InterruptedException 
	{		
		return Notification_Message_forPhotos.getText();
	}
	
*/	
	public void Close_a_submission() 
	{
		try {
			if (Close_Submission_button.isDisplayed()) 
			{
				Close_Submission_button.click();
				RedButton_Yes.click();
				utils.waitForLoader(Loader);
			}
		} catch (Exception e) {
			System.out.println("Close Button does not exist.");
			System.out.println(e);
		}
		
	}
	
	public void Reopen_submission() 
	{
		try {
			if (Reopen_Submission_button.isDisplayed()) 
			{
				Reopen_Submission_button.click();
				RedButton_Yes.click();
				utils.waitForLoader(Loader);
			}
		} catch (Exception e) {
			System.out.println("Close Button does not exist.");
			System.out.println(e);
		}
		
	}
	
	
	public void Click_on_cross_icon() throws InterruptedException
	{
		Issue_cross_icon.click();
		utils.waitForLoader(Loader);
		
	}
	
	public void Change_Asignee(String AsigneeTobeselected)  throws InterruptedException
	{
		utils.DropDownWithoutSelectTag_forCRM(AllAsigneesFromDropDown, AsigneeTobeselected);
		utils.waitForLoader(Loader);		
	}
	
	public void Change_Category(String CategoryToBeSwitched, String UpdateAssignee_yes_no)  throws InterruptedException
	{
		utils.DropDownWithoutSelectTag_forCRM(AllCategoriesFromDropdown, CategoryToBeSwitched);
		if (UpdateAssignee_yes_no.equalsIgnoreCase("Yes")) {
			RedButton_Yes.click();
		}
		else {
			Do_not_update.click();
		}
		utils.waitForLoader(Loader);		
	}
	
	public ArrayList<String> List_Of_AllAsignees()  throws InterruptedException
	{
		ArrayList<String> AllasigneeValues = new ArrayList<>();
		for (WebElement ele : AllAsigneesFromDropDown) {
			AllasigneeValues.add(ele.getText());
		}
		
		return AllasigneeValues;
		
	}
	
	public void Deselect_collaberator ()  throws InterruptedException
	{
		Checked_collaberator_Checkbox.get(0).click();
		utils.waitForLoader(Loader);
	}
	
	public void Select_collaberator ()  throws InterruptedException
	{
		AllCollaberatrosFromDropDown.get(0).click();
		utils.waitForLoader(Loader);
	}
	
}

