package com.qa.comcate.pages;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.comcate.base.TestBase;
import com.qa.comcate.utils.CommonUtils;

public class CSLP_Page extends TestBase {
	CommonUtils utils;
	Actions action;
	JavascriptExecutor js;
	AgenciesLandingPage agenciesLandingPageObj;
	
	@FindBy(xpath = "//h2")
	public WebElement PageHeader;
	
	@FindBy(xpath = "//div[@class='table-container__table table_disabled']")
	public List<WebElement> Table_in_Disabled_state;
	
	@FindBy(xpath = "//div[@class='rt-resizable-header-content' and text()='Submission Number']")
	public  WebElement Submission_number_header;
	
	@FindBy(xpath = "//div[@class='rt-resizable-header-content' and text()='Customer Email']")
	public  WebElement Customer_email_header;
	
	@FindBy(xpath = "//div[@class='rt-td rthfc-td-fixed rthfc-td-fixed-left rthfc-td-fixed-left-last']/a")
	public List<WebElement> ListOfIssue_Numbers;
	
	
	@FindBy(xpath = "//div[@class='rt-resizable-header-content' and text()='Issue Description']")
	public  WebElement Issue_Description_header;
	
	@FindBy(xpath = "//div[@class='rt-resizable-header-content' and text()='Assignee']")
	public  WebElement Asignee_header;
	
	@FindBy(xpath = "//div[@class='rt-resizable-header-content' and text()='Created']")
	public  WebElement CreatedAt_header;
	
	@FindBy(xpath = "//div[@class='rt-resizable-header-content' and text()='Category']")
	public  WebElement Category_header;
	
	@FindBy(xpath = "//div[@class='rt-resizable-header-content' and text()='Closed']")
	public  WebElement Closed_header;
	
	@FindBy(xpath = "//div[@class='rt-td'][1]")
	public List<WebElement> ListOfIssue_Descriptions;
	
	@FindBy(xpath = "//div[@class='rt-td'][3]")
	public List<WebElement> ListOf_Asignees;
	
	@FindBy(xpath = "//div[@class='rt-td'][5]")
	public List<WebElement> ListOf_Created_date;
	
	@FindBy(xpath = "//div[@class='rt-td'][7]")
	public List<WebElement> ListOf_Categories_in_issue_table;
	
	@FindBy(xpath = "//div[@class='rt-td'][9]")
	public List<WebElement> ListOfCustomer_email;
	
	@FindBy(xpath = "//a[text()='21-1']/../../..//div[@class='rt-td']")
	public List<WebElement> Content_Of_First_issue;
	
	@FindBy(xpath = "//div[@class='cs-listing__section__header']")
	public WebElement Count_of_issues;
	
	@FindBy(xpath = "//input[@name='searchQuery']")
	public WebElement Search_box;
	
	@FindBy(xpath = "//div[@class='cs-listing__section__header']")
	public WebElement Table_header;
	
	@FindBy(xpath = "//div[@class='rt-noData']")
	public WebElement No_data;
	
	@FindBy(css = "div.container-fluid div.app:nth-child(2) main.app__contents div.case-listing div.case-listing__filters div.search.search--searching:nth-child(3) > span.search__img")
	public WebElement Cross_or_search_icon;
	
	@FindBy(linkText = "Clear all filters")
	public List<WebElement> Clear_all_filter;
	
	// CSLP Filters
	@FindBy(xpath = "(//span[@class='dropdown__arrow'])[2]")
	public WebElement SelectCategory_DropDown_Icon;
	
	@FindBy(xpath = "//div[@id='categoryIds']/ul/li")
	public List<WebElement> AllCategories_in_filter;
	
	@FindBy(xpath = "//div[@class='filter-options category']/div[@class='dropdown__wrapper']")
	public WebElement Category_filter_closed_state;
	
	@FindBy(xpath = "//img[@src='/assets/loading.gif']")
	public List<WebElement> Loader;
	
	//===============Filters ================
	@FindBy(xpath = "//div[@id='createdAt']//div[@class='custom-filter__selector__selected']")
	public WebElement CreatedAt_dropDown;

	@FindBy(xpath = "//div[@id='createdAt']//button[text()='Apply']")
	public WebElement CreatedAt_filter_Apply_Button;
	
	
	
	public CSLP_Page() {
		PageFactory.initElements(driver, this);
		utils = new CommonUtils();
		js = (JavascriptExecutor) driver;
		action = new Actions(driver);
		agenciesLandingPageObj = new AgenciesLandingPage();
	}

	
	public String GetPageHeader() throws InterruptedException
	{
		return PageHeader.getText();
	}
	
	
	public boolean ChecksortingOnFirstClick(WebElement ele, List<WebElement> List) throws InterruptedException
	{
		ele.click();
		utils.waitForLoader(Table_in_Disabled_state);
		return utils.CheckIfAscending(List);
	}
	
	public List<String> Get_Entire_Content_Of_Issue() throws InterruptedException
	{
		List<String> IssueDetails = new ArrayList<>();
		for (WebElement we : Content_Of_First_issue) {
			IssueDetails.add(we.getText());
		}
		return IssueDetails;
	}
	
	public boolean ChecksortingOnSecondClick(WebElement ele, List<WebElement> List) throws InterruptedException
	{
		ele.click();
		utils.waitForLoader(Table_in_Disabled_state);
		ele.click();
		utils.waitForLoader(Table_in_Disabled_state);
		return utils.CheckIfDescending(List);
	}
	
	public int Get_count_of_issues()
	{
		return Integer.parseInt(Count_of_issues.getText().replaceAll("[^0-9]", ""));
	}
	
	public List<WebElement> Get_content_of_latest_issue()
	{

		String Latest_issue_number = ListOfIssue_Numbers.get(0).getText();
		List<WebElement> ele = driver.findElements(By.xpath("//a[text()='" + Latest_issue_number + "']/../../..//div[@class='rt-td']"));
		return ele;
	}
	
	public void Enter_keyword_to_search(String SearchKey) throws InterruptedException
	{
		Search_box.sendKeys(SearchKey);
		utils.waitForLoader(Table_in_Disabled_state);
	}
	
	public void ClickOn_cross_icon() throws InterruptedException
	{
		System.out.println("in");
		action.moveToElement(Cross_or_search_icon).click().perform();
		utils.waitForLoader(Table_in_Disabled_state);
	}
	
	public boolean isValidDate(String inDate) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
        dateFormat.setLenient(false);
        try {
            dateFormat.parse(inDate.trim());
        } catch (ParseException pe) {
            return false;
        }
        return true;
    }
	
	public List<WebElement> Get_clear_all_filter_link() {
		return Clear_all_filter;
    }
	
	public void Click_on_Clear_all_filter() throws InterruptedException {
		Clear_all_filter.get(0).click();
		utils.waitForLoader(Table_in_Disabled_state);
    }
	
	public void select_category_from_category_filter (String CategoryName)  throws InterruptedException
	{
		SelectCategory_DropDown_Icon.click();
		Thread.sleep(2000);
		utils.DropDownWithoutSelectTag_forCRM(AllCategories_in_filter, CategoryName);
		utils.waitForLoader(Table_in_Disabled_state);
	}
	
	public void ClickOn_CreatedAt_ApplyFilter ()  throws InterruptedException
	{
		CreatedAt_filter_Apply_Button.click();
		utils.waitForLoader(Table_in_Disabled_state);
	}
	
	public CSDP_Page Click_On_Latest_issues ()  throws InterruptedException
	{
		ListOfIssue_Numbers.get(0).click();
		utils.waitForLoader(Loader);
		return new CSDP_Page();
	}
	
}
