package com.qa.comcate.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.comcate.base.TestBase;
import com.qa.comcate.utils.CommonUtils;

public class ChromelessAppPage extends TestBase {
	CommonUtils utils;
	Actions action;
	JavascriptExecutor js;
	AgenciesLandingPage agenciesLandingPageObj;
	
	@FindBy(xpath = "//nav[@class='crm-submission__navigation-bar']")
	public WebElement PageHeader;
	
	@FindBy(name = "description")
	public WebElement IssueDescription;
	
	@FindBy(xpath = "//input[@id='file-upload']")
	public WebElement UploadPhotos;
	
	@FindBy(xpath = "//div[@class='notification-message']")
	public WebElement Notification_Message_forPhotos;
	
	@FindBy(xpath = "(//button[text()='Next'])[1]")
	public WebElement NextButton_Issuedescription;
	
	@FindBy(xpath = "(//button[text()='Next'])[2]")
	public WebElement NextButton_SelectCategory;
	
	@FindBy(xpath = "(//button[text()='Next'])[3]")
	public WebElement NextButton_Location;
	
	@FindBy(xpath = "//div[@class='crm-location-section']//span[@class='field__error']")
	public WebElement Location_field_Error_message;
	
	
	@FindBy(xpath = "//div[@class='crm-categories-field']")
	public WebElement CategorySection;
		
	@FindBy(xpath = "//div[@class='chip__wrapper active clickable']")
	public WebElement ActiveCategory;
	
	@FindBy(xpath = "//div[@class='chip__body']")
	public List<WebElement> CategoryTubes;
	
	@FindBy(xpath = "//a[@class='show-more']")
	public WebElement ShowMoreLink;
	
	@FindBy(xpath = "//div[@class='map-container map-container--hide-layer-control']")
	public WebElement Map_component;
	
	
	@FindBy(name = "firstName")
	public WebElement Citizen_FirstName;
	
	@FindBy(name = "lastName")
	public WebElement Citizen_LastName;
	
	@FindBy(name = "hasSubscribed")
	public WebElement Has_Subscribed_checkbox;
	
	@FindBy(name = "email")
	public WebElement Citizen_Email;
	
	@FindBy(xpath="//button[text()='Submit']")
	public WebElement SubmitIssue_button;
	
	@FindBy(xpath="//section[@class='crm__success-message-section']")
	public WebElement ConfirmationMessage;
	
	@FindBy(xpath="//input[@name='isAnonymous' and @type='checkbox']")
	public WebElement PostAnonymousCheckBox;
	
	@FindBy(xpath="//p[@class='location-details']")
	public WebElement ContactInfo_Location;
	
	@FindBy(xpath="//img[@class='photos-tile__saved-thumb']")
	public List<WebElement> Photo_thumbnail;
	
	@FindBy(xpath="//label[text()='First Name:']/following-sibling::span")
	public WebElement ContactInfo_Firstname;
	
	@FindBy(xpath="//label[text()='Last Name:']/following-sibling::span")
	public WebElement ContactInfo_LastName;
	
	@FindBy(xpath="//label[text()='Email:']/following-sibling::span")
	public WebElement ContactInfo_Email;
	
	@FindBy(css=".leaflet-container")
	public WebElement Drop_pin;
	
	@FindBy(xpath="//input[@placeholder='Search an address or drop a pin on the map']")
	public WebElement Location_search;
	
	@FindBy(xpath="//div[@id='react-autowhatever-1']//i")
	public WebElement Current_location;
	
	@FindBy(xpath="//div[@class='leaflet-bottom leaflet-right']/div/button")
	public WebElement Find_me_icon;	
	
	
	@FindBy(xpath="//div[@class='crm-location__selected-search']/span")
	public WebElement Location_after_dropping_pin;
	
	@FindBy(xpath="//label[text()='Category']//parent::div/p")
	public WebElement Category_name_on_submission_page;
	

	public ChromelessAppPage() {
		PageFactory.initElements(driver, this);
		utils = new CommonUtils();
		js = (JavascriptExecutor) driver;
		action = new Actions(driver);
		agenciesLandingPageObj = new AgenciesLandingPage();
	}

	public void EnterDescription(String EnterDescription)
	{
		IssueDescription.sendKeys(EnterDescription);
	}
	
	public void UploadSinglePhotobyCitizen()
	{
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm1.jpe");
	}
	
	
	public void UploadMultiplePhotosbyCitizen()
	{
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm2.jpg");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm3.png");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm1.png");
	}
	
	public void Upload_invalid_format_Txt_file()
	{
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/Invalid_file_data_ticket");
	}
	
	public void Upload_file_More_than_15_mb()
	{
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/File_Greater_than15MB.jpg");
	}
	
	public void UploadPhotoMoreThan15MB()
	{
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/File_Greater_than15MB.jpg");
	}
	
	public void ClickOnNExtButton_Description_whenPhotosUploaded() throws InterruptedException
	{
		while (!(NextButton_Issuedescription.isEnabled())) {
			Thread.sleep(1000);		
		}
		NextButton_Issuedescription.click();
		while (!(CategorySection.isDisplayed())) {
			Thread.sleep(500);			
		}
		System.out.println("Category panel is Visible ");
	}
	
	public void ClickOnNExtButton_Description_WithoutPhotos() throws InterruptedException
	{
		int count = 0;
		NextButton_Issuedescription.click();
		while (!(CategorySection.isDisplayed()) && count < 5) {
			Thread.sleep(500);	
			count++;
		}
		System.out.println("Category panel is Visible ");
	}
	
	public void ClickOnShowMore() throws InterruptedException
	{
		if (ShowMoreLink.isDisplayed()) {
			ShowMoreLink.click();
		}
		else {
			System.out.println("Show More link is Not Present");
		}
	}
	
	
	public void EnterCitizenInfo(String PostAnonymous_Yes_No,String FirstName,String LastName, String Email) 
	{
		Citizen_FirstName.sendKeys(FirstName);
		Citizen_LastName.sendKeys(LastName);
		if (Email.isEmpty()) {
			Has_Subscribed_checkbox.click();
		}
		else {
			Citizen_Email.sendKeys(Email);
		}
		if (PostAnonymous_Yes_No.equalsIgnoreCase("Yes") && !(PostAnonymousCheckBox.isSelected())) 
		{
			PostAnonymousCheckBox.click();
		}
		SubmitIssue_button.click();

	}

	/**
	Remove the last 5 random characters appearing in a category and then pass as an argument
	@param the parameters used by the method
	@return the value returned by the method
	@throws what kind of exception does this method throw
	*/
	public void SelectCategory(String CategoryName) 
	{
		if (!(CategoryName.isEmpty())) {
			if (ShowMoreLink.isDisplayed()) {
				ShowMoreLink.click();
			}
			else {
				System.out.println("Show More link is Not Present");
			}
			WebElement Catgeory = driver.findElement(By.xpath("//div[@class='chip__body' and starts-with(text(),'" + CategoryName + "')]"));
			Catgeory.click();
		}
		System.out.println("category based on description is selected");
		NextButton_SelectCategory.click();
	}
	
	public String SelectMap(String Drop_a_pin_yes_no) 
	{
		String Address = "-";
		if (Drop_a_pin_yes_no.equalsIgnoreCase("yes")) {
			Drop_pin.click();
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Address = Location_after_dropping_pin.getText();
		}
		NextButton_Location.click();
		return Address;
	}
	
	public String Select_current_location() throws InterruptedException
	{
		String Address = "-";
		Location_search.click();
		Current_location.click();
		Thread.sleep(2000);
		Address = Location_after_dropping_pin.getText();
		return Address;
	}
	
	public boolean check_existance_of_field(WebElement ele) 
	{
		boolean flag;
		try {
			ele.isDisplayed();
			flag =  true;
		} catch (Exception e) {
			flag = false;
		}
		return flag;
	}
	
	
	public String drop_a_pin() throws InterruptedException 
	{
		Drop_pin.click();
		Thread.sleep(2000);
		return Location_after_dropping_pin.getText();
	}
	
	public int Get_count_of_photo_thumbnails() throws InterruptedException 
	{		
		int number_of_thumbnails = 0;
		try {
			number_of_thumbnails = Photo_thumbnail.size();
		} catch (Exception e) {
			System.out.println("No photo thumbnail present");
		}
		return number_of_thumbnails;
	}
	
	public String Get_notification_Message_for_photos() throws InterruptedException 
	{		
		return Notification_Message_forPhotos.getText();
	}
	
	
	public void Upload150PhotosbyCitizen()
	{
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm1.jpe");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm2.jpg");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm3.png");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm1.jpe");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm2.jpg");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm3.png");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm1.jpe");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm2.jpg");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm3.png");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm1.jpe");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm2.jpg");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm3.png");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm1.jpe");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm2.jpg");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm3.png");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm1.jpe");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm2.jpg");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm3.png");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm1.jpe");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm2.jpg");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm3.png");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm1.jpe");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm2.jpg");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm3.png");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm1.jpe");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm2.jpg");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm3.png");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm1.jpe");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm2.jpg");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm3.png");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm1.jpe");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm2.jpg");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm3.png");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm1.jpe");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm2.jpg");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm3.png");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm1.jpe");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm2.jpg");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm3.png");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm1.jpe");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm2.jpg");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm3.png");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm1.jpe");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm2.jpg");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm3.png");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm1.jpe");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm2.jpg");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm3.png");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm1.jpe");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm2.jpg");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm3.png");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm1.jpe");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm2.jpg");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm3.png");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm1.jpe");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm2.jpg");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm3.png");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm1.jpe");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm2.jpg");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm3.png");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm1.jpe");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm2.jpg");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm3.png");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm1.jpe");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm2.jpg");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm3.png");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm1.jpe");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm2.jpg");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm3.png");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm1.jpe");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm2.jpg");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm3.png");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm1.jpe");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm2.jpg");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm3.png");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm1.jpe");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm2.jpg");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm3.png");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm1.jpe");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm2.jpg");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm3.png");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm1.jpe");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm2.jpg");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm3.png");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm1.jpe");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm2.jpg");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm3.png");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm1.jpe");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm2.jpg");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm3.png");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm1.jpe");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm2.jpg");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm3.png");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm1.jpe");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm2.jpg");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm3.png");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm1.jpe");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm2.jpg");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm3.png");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm1.jpe");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm2.jpg");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm3.png");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm1.jpe");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm2.jpg");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm3.png");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm1.jpe");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm2.jpg");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm3.png");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm1.jpe");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm2.jpg");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm3.png");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm1.jpe");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm2.jpg");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm3.png");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm1.jpe");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm2.jpg");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm3.png");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm1.jpe");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm2.jpg");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm3.png");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm1.jpe");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm2.jpg");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm3.png");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm1.jpe");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm2.jpg");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm3.png");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm1.jpe");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm2.jpg");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm3.png");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm1.jpe");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm2.jpg");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm3.png");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm1.jpe");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm2.jpg");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm3.png");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm1.jpe");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm2.jpg");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm3.png");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm1.jpe");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm2.jpg");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm3.png");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm1.jpe");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm2.jpg");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm3.png");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm1.jpe");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm2.jpg");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm3.png");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm1.jpe");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm2.jpg");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm3.png");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm1.jpe");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm2.jpg");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm3.png");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm1.jpe");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm2.jpg");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm3.png");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm1.jpe");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm2.jpg");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm3.png");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm1.jpe");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm2.jpg");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm3.png");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm1.jpe");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm2.jpg");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm3.png");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm1.jpe");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm2.jpg");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm3.png");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm1.jpe");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm2.jpg");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm3.png");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm1.jpe");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm2.jpg");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm3.png");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm1.jpe");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm2.jpg");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm3.png");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm1.jpe");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm2.jpg");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm3.png");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm1.jpe");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm2.jpg");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm3.png");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm1.jpe");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm2.jpg");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm3.png");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm1.jpe");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm2.jpg");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm3.png");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm1.jpe");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm2.jpg");
		UploadPhotos.sendKeys(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/crm3.png");
	}
	
}
