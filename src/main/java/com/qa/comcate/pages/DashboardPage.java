package com.qa.comcate.pages;

import java.util.List;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.comcate.base.TestBase;
import com.qa.comcate.utils.CommonUtils;

public class DashboardPage extends TestBase{
	CommonUtils utils;
	Actions action;
	JavascriptExecutor js;
	
	@FindBy(xpath = "//div[@class='app-header__menu__icon']")
	public WebElement HeaderMenu;
	
	
	@FindBy(xpath = "//a[@class='tools-dropdown__tool-img tools-dropdown__tool-img--setup']")
	public WebElement GearIcon;
	
	@FindBy(xpath = "//div[@class='app-header__new']")
	public WebElement PlusIcon;

	@FindBy(xpath = "//label[@class='create-options-dropdown__option-name' and text()='Customer submission']")
	public WebElement InternalSubmission;
	
	@FindBy(xpath = "//img[@src='/assets/loading.gif']")
	public List<WebElement> Loader;
	
	public DashboardPage() {
		PageFactory.initElements(driver, this);
		utils = new CommonUtils();
        js = (JavascriptExecutor) driver;
        action = new Actions(driver);
	}
	
	public AgencySetupPage ClickonGearIcon() throws InterruptedException {
		HeaderMenu.click();
		Thread.sleep(2000);
		GearIcon.click();
		utils.waitForLoader(Loader);
		System.out.println("clicked on Gear icon");
		return new AgencySetupPage();
	}
	
	public InternalSubmissionPage NavigateToInternalsubmission() throws InterruptedException
	{
		PlusIcon.click();
		InternalSubmission.click();
		utils.waitForLoader(Loader);
		return new InternalSubmissionPage();
		
	}
}
