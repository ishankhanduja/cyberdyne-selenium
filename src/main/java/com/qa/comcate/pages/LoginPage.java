package com.qa.comcate.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.comcate.base.TestBase;

public class LoginPage extends TestBase {

	@FindBy(xpath = "//input[@name='email']")
	public WebElement LoginEmailField;
	
	@FindBy(xpath = "//input[@id='okta-signin-username']")
	public WebElement OktaLoginEmailField;

	@FindBy(xpath = "//input[@id='okta-signin-password']")
	public WebElement OktaLoginPasswordField;

	@FindBy(xpath = "//input[@id='okta-signin-submit']")
	public WebElement OktaLoginbutton;
	
	@FindBy(xpath = "//input[@name='password']")
	public WebElement LoginPasswordField;

	@FindBy(xpath = "//button[text()='Login']")
	public WebElement Loginbutton;
	
	@FindBy(xpath = "//div[@class='notification-message']")
	public WebElement IncorrectDetails;

	@FindBy(xpath = "(//span[@class='field__error'])[1]")
	public WebElement ValidationMessageForLoginEmailField;

	@FindBy(xpath = "(//span[@class='field__error'])[2]")
	public WebElement ValidationMessageForLoginPasswordField;

	public LoginPage() {
		PageFactory.initElements(driver, this);
	}

	public AgenciesLandingPage LoginToApplicationWithValid(String email, String Password) {

		LoginEmailField.sendKeys(email);
		LoginPasswordField.sendKeys(Password);
		Loginbutton.click();
		return new AgenciesLandingPage();
	}

	public LoginPage LoginToApplicationWithInvalidDetails(String email, String Password) {

		LoginEmailField.sendKeys(email);
		LoginPasswordField.sendKeys(Password);
		Loginbutton.click();
		// return IncorrectDetails.getText();
		return new LoginPage();
	}

}
