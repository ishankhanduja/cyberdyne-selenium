package com.qa.comcate.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.comcate.base.TestBase;
import com.qa.comcate.utils.CommonUtils;
import com.qa.comcate.utils.SwitchWindows;

public class YopMailPage extends TestBase {
	CommonUtils utils;
	Actions action;
	JavascriptExecutor js;
	AgenciesLandingPage agenciesLandingPageObj;
	SwitchWindows switchWindowObj;
	
	@FindBy(xpath = "//input[@id='login']")
	public WebElement Enter_email_address;
	
	@FindBy(xpath = "//input[@type='submit']")
	public WebElement Check_inbox_button;
	
	@FindBy(xpath = "//div[text()='Inbox']//parent::td")
	public WebElement InboxHeader;
	
	@FindBy(xpath = "//span[@class='mgif irefresh b']")
	public WebElement Check_for_new_emailButton;
	
	@FindBy(linkText = "Management Tools")
	public WebElement Management_tools_Link;
	
	
	public YopMailPage() {
		PageFactory.initElements(driver, this);
		utils = new CommonUtils();
		js = (JavascriptExecutor) driver;
		action = new Actions(driver);
		switchWindowObj = new SwitchWindows();
		agenciesLandingPageObj = new AgenciesLandingPage();
	}

	
	public void OpenYopmail() throws InterruptedException
	{
		switchWindowObj.SwitchBytitle("Comcate Admin");
		String selectLinkOpeninNewTab = Keys.chord(Keys.CONTROL,Keys.RETURN);
		Management_tools_Link.sendKeys(selectLinkOpeninNewTab);
		Thread.sleep(2000);
		switchWindowObj.SwitchByUrl("https://admin-stage.comcate.com/tools/violation-type");
		driver.get("http://www.yopmail.com/en/");
	}	
	
	public void NavigateTOInbox(String Yopmail_EmailId) throws InterruptedException
	{
		Enter_email_address.sendKeys(Yopmail_EmailId);
		Check_inbox_button.click();
	}

	public int Get_Count_Of_Emails() throws InterruptedException
	{
		return Integer.parseInt(InboxHeader.getText().replaceAll("[^0-9]", ""));
	}
	
	public void Check_for_new_email() throws InterruptedException
	{
		int oldMailcount =  Integer.parseInt(InboxHeader.getText().replaceAll("[^0-9]", ""));
		int newemailcount = Integer.parseInt(InboxHeader.getText().replaceAll("[^0-9]", ""));
		while (newemailcount != oldMailcount + 1) {
			Check_for_new_emailButton.click();
			newemailcount = Integer.parseInt(InboxHeader.getText().replaceAll("[^0-9]", ""));
		}
	}
	
	
	
}

