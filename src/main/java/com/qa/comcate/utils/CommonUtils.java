package com.qa.comcate.utils;

import java.io.BufferedReader;
//import org.apache.commons.io.FileUtils;
import org.apache.*;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.qa.comcate.base.TestBase;

public class CommonUtils extends TestBase {
	
	@FindBy(xpath = "//img[@src='/assets/loading.gif']")
	public List<WebElement> Loader;

	public WebElement WaitForElementToBePresence(WebElement fieldname) {
		return new WebDriverWait(driver, 15).until(ExpectedConditions.visibilityOf(fieldname));
	}
	
	public WebElement WaitForElementToBeClickable(WebElement fieldname) {
		return new WebDriverWait(driver, 15).until(ExpectedConditions.elementToBeClickable(fieldname));
	}
	
	public String timestampGenerator() 
	{
		Date date = new Date();
	    Instant Timestamp = date.toInstant();
	    return Timestamp.toString();
	}
	
	public String timestampGeneratorForCSDP() 
	{
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MM/dd/yyyy");  
		   LocalDateTime now = LocalDateTime.now();  
		   return dtf.format(now).toString();  
	}
	
	public void waitForLoader(List<WebElement> LoadingIcon) throws InterruptedException 
	{
		int count = 0;
		while (LoadingIcon.size()!=0 && count<10) {
			Thread.sleep(1000);
			count++;
			
		}
	}

	
	
	//This method would work for toggle who's default state is OFF
	/*public void select_ToggleButton(String Toggle_on_off_state) 
	{
		if (Toggle_on_off_state == "On" || Toggle_on_off_state == "on" || Toggle_on_off_state == "On") {

			if (Toggle_on_off_state.isDisplayed()) {
				Toggle_on_off_state.click();
			}
		} else {
			System.out.println("Gis is turned Off");
		}
	}
	*/
	
	
	
	// Pass element with arrow xpath in place of dropdown
	public void DropDownWithoutSelectTag_ForCE(List<WebElement> dropdown, String ValueToBeSelected) {
		List<WebElement> myElements = dropdown;
		System.out.println(myElements);
		for (WebElement e : myElements) {
			//System.out.println(e.getText());
			if (e.getText().equalsIgnoreCase(ValueToBeSelected)) {
				e.click();
				break;
			}
		}
	}
	
	public void DropDownWithoutSelectTag_forCRM(List<WebElement> dropdown, String ValueToBeSelected) {
		List<WebElement> myElements = dropdown;
		System.out.println(myElements);
		for (WebElement e : myElements) {
			//System.out.println(e.getText());
			if (e.getText().contains(ValueToBeSelected)) {
				e.click();
				break;
			}
		}
	}
	
	public List<WebElement> FetchListFromDropDown(List<WebElement> dropdown) {
		List<WebElement> myElements = dropdown;
		for (WebElement webElement : myElements) {
			System.out.println(webElement.getText());
		}
		return myElements;
	}	
	
	public void ClickOnMultiChoiceButton(String NameOfMultichoicebutton,String OptionTobeSelected) 
	{
		WebElement MultiChoiceButton = driver.findElement(
				By.xpath("//label[text()='" + NameOfMultichoicebutton + "']/parent::div/following-sibling::div/div/button[text()='" + OptionTobeSelected + "']"));
		MultiChoiceButton.click();
	}
	
	public String GetErrorMessageForFieldName(String FieldName) 
	{
		WebElement ErrorMessage = driver.findElement(
				By.xpath("//label[@class='field__label-label' and text()='" + FieldName + "']//parent::div//parent::div//span[@class='field__error']"));
		return ErrorMessage.getText();
	}	
	
	public WebElement Get_CSLP_CreatedAt_Filter_radio_button(String FieldName) 
	{
		WebElement ele = driver.findElement(
				By.xpath("//div[@id='createdAt']//input[@class='checkbox__input' and @name='" + FieldName + "']"));
		return ele;
	}
	
	public WebElement Get_CSLP_ClosedAt_filter_radio_button(String FieldName) 
	{
		WebElement ele = driver.findElement(
				By.xpath("//div[@id='closedAt']//input[@class='checkbox__input' and @name='" + FieldName + "']"));
		return ele;
	}
	
	//div[@id='createdAt']//input[@class='checkbox__input' and @name='Any Time']
	
	
	public void WriteInFile(String textToWrite)
	{
		File file1 = new File(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/AgencyName.txt");

		FileWriter fw = null;
		try {
			fw = new FileWriter(file1);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			fw.write(textToWrite);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			fw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void WriteIDInFile(String textToWrite)
	{
		File file1 = new File(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/AgencyID.txt");

		FileWriter fw = null;
		try {
			fw = new FileWriter(file1);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			fw.write(textToWrite);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			fw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public String ReadFromFile() throws IOException 
	{
		FileReader FR = new FileReader(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/AgencyName.txt");
		  BufferedReader BR = new BufferedReader(FR);
		  String AgencyName = "";
		  String agencyCreated = null;
		  
		  while((AgencyName = BR.readLine())!= null){
			   //System.out.println("AgencyName "+AgencyName);
			  agencyCreated = AgencyName;
	}
		  return agencyCreated;

}
	
	public String ReadAgencyIDFromFile() throws IOException 
	{
		FileReader FR = new FileReader(System.getProperty("user.dir") + "/src/main/java/com/qa/comcate/files/AgencyID.txt");
		  BufferedReader BR = new BufferedReader(FR);
		  String AgencyName = "";
		  String agencyCreated = null;
		  
		  while((AgencyName = BR.readLine())!= null){
			   //System.out.println("AgencyName "+AgencyName);
			  agencyCreated = AgencyName;
	}
		  return agencyCreated;

}
	
	
	public String getModalHeader() 
	{
		return driver.findElement(By.xpath("//div[@class='full-page-modal__header']/h1")).getText();
	}
	
	public boolean CheckIfAscending(List<WebElement> ListToBeSorted) 
	{
		ArrayList<String> obtainedList = new ArrayList<>(); 
		List<WebElement> elementList= ListToBeSorted;
		for(WebElement we:elementList){
		   obtainedList.add(we.getText());
		}
		ArrayList<String> sortedList = new ArrayList<>();   
		for(String s:obtainedList){
		sortedList.add(s);
		}
		//Collections.sort(sortedList);
		//Collections.reverse(sortedList);
		Collections.sort(sortedList, new Comparator<String>() {
		    @Override
		    public int compare(String s1, String s2) {
		        //return s1.compareToIgnoreCase(s2);
		    	if (s1.equals("-") || s1.equals("Unknown"))
		            return 0;
		          if (s2.equals("-")|| s2.equals("Unknown"))
		            return 0;
		          return s1.compareToIgnoreCase(s2);
		          
		    }

		});
		
		return sortedList.equals(obtainedList);
	}
	
	
	public boolean CheckIfDescending(List<WebElement> ListToBeSorted) 
	{
		ArrayList<String> obtainedList = new ArrayList<>(); 
		List<WebElement> elementList= ListToBeSorted;
		for(WebElement we:elementList){
		   obtainedList.add(we.getText());
		}
		ArrayList<String> sortedList = new ArrayList<>();   
		for(String s:obtainedList){
		sortedList.add(s);
		}		
		Collections.sort(sortedList, new Comparator<String>() {
		    @Override
		    public int compare(String s1, String s2) {
		    	if (s1.equals("-") || s1.equals("Unknown"))
		            return 0;
		          if (s2.equals("-")|| s2.equals("Unknown"))
		            return 0;
		          return s1.compareToIgnoreCase(s2);
		    }
		});
		Collections.reverse(sortedList);
		
		System.out.println(sortedList);
		System.out.println(obtainedList);
		return sortedList.equals(obtainedList);
	}
	
	
	public String getAlphaNumericString(int n) 
    { 
  
        // chose a Character random from this String 
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                                    + "0123456789"
                                    + "abcdefghijklmnopqrstuvxyz"; 
  
        // create StringBuffer size of AlphaNumericString 
        StringBuilder sb = new StringBuilder(n); 
  
        for (int i = 0; i < n; i++) { 
  
            // generate a random number between 
            // 0 to AlphaNumericString variable length 
            int index 
                = (int)(AlphaNumericString.length() 
                        * Math.random()); 
  
            // add Character one by one in end of sb 
            sb.append(AlphaNumericString 
                          .charAt(index)); 
        } 
  
        return sb.toString(); 
    } 
	
	
	public List<WebElement> Check_existance_of_dropDown_for_ReadOnly(String FieldName) 
	{
		List<WebElement> DropDownArrow = driver.findElements(
				By.xpath("//div[@class='field-section']/label[text()='" + FieldName + "']//parent::div//span[@class='dropdown__arrow']"));
		return DropDownArrow;
	}
	
	public String Get_current_selection_from_dropdownCSDP(String FieldName) 
	{
		WebElement Label = driver.findElement(
				By.xpath("//div[@class='field-section']/label[text()='" + FieldName + "']//parent::div//div[@class='dropdown__selector__selected']/label"));
		return Label.getText();
	}
	
	public static void takeScreenshotAtEndOfTest() throws IOException {
		File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		String currentDir = System.getProperty("user.dir");
		//FileUtils.copyFile(scrFile, new File(currentDir + "/screenshots/" + System.currentTimeMillis() + ".png"));
	}
	
	
}
