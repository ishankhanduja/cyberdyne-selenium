package com.qa.comcate.utils;

import java.util.Set;

import com.qa.comcate.base.TestBase;

public class SwitchWindows extends TestBase {
	
	public SwitchWindows() 
	{
		super();
	}
	
	public boolean SwitchBytitle(String TitleOfPage) 
	{
		String Parent_window = driver.getWindowHandle();
		Set<String> windows = driver.getWindowHandles();
		
		for (String winID : windows) {
			
			if (!(winID.equals(Parent_window))) {
				
				driver.switchTo().window(winID);
				if (driver.getTitle().equals(TitleOfPage) || driver.getTitle().contains(TitleOfPage)) {
					
					return true;
				}
				driver.switchTo().window(Parent_window);
			}
		}
		return false;
	}
	
	public boolean SwitchByUrl(String Url) 
	{
		String Parent_window = driver.getWindowHandle();
		Set<String> windows = driver.getWindowHandles();
		
		for (String winID : windows) {
			
			if (!(winID.equals(Parent_window))) {
				
				driver.switchTo().window(winID);
				if (driver.getCurrentUrl().equals(Url)) {
					
					return true;
				}
				driver.switchTo().window(Parent_window);
			}
		}
		return false;
	}

}
