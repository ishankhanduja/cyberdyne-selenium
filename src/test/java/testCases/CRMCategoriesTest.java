package testCases;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.AssertJUnit;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.qa.Analyzer.Retriable;
import com.qa.comcate.base.TestBase;
import com.qa.comcate.pages.AgenciesLandingPage;
import com.qa.comcate.pages.AgencySetupPage;
import com.qa.comcate.pages.DashboardPage;
import com.qa.comcate.pages.LoginPage;
import com.qa.comcate.utils.CommonUtils;

public class CRMCategoriesTest extends TestBase {

	LoginPage LoginPageObj;
	AgenciesLandingPage AgenciesLandingPageObj;
	DashboardPage DashboardPageObj;
	AgencySetupPage AgencySetupPageObj;
	SoftAssert softAssertion;
	
	CommonUtils utils;
	
	public CRMCategoriesTest() {
		super();
	}

	@BeforeMethod
	//@Retriable(attempts = 4)
	public void setup() throws Exception {
		initiate();
		softAssertion = new SoftAssert();
		LoginPageObj = new LoginPage();
		AgenciesLandingPageObj = new AgenciesLandingPage();
		DashboardPageObj = new DashboardPage();
		AgencySetupPageObj = new AgencySetupPage();
		utils = new CommonUtils();
		LoginPageObj.LoginToApplicationWithValid("support@comcate.com", "test@123");
		System.out.println(utils.ReadFromFile());
		WebElement SearchedAgency = AgenciesLandingPageObj.SearchAgency(utils.ReadFromFile());
		Thread.sleep(2000);
		//Assert.assertEquals(SearchedAgency.getText(), utils.ReadFromFile());
		AgenciesLandingPageObj.LoginAsSuperAdmin();
		
	}

	@AfterMethod
	public void tearDown() {
		driver.quit();
	}
	
	@Test(priority=1)
	public void VerifyCreateCategoryButton() throws InterruptedException 
	{
		DashboardPageObj.ClickonGearIcon();
		AgencySetupPageObj.clickOnCRMCategories();
		Assert.assertTrue(AgencySetupPageObj.CreateCategoryButton.isDisplayed());
	}	
	
	
	@Test(priority=2)
	public void VerifyCreateCategoryModalHeader() throws InterruptedException 
	{
		DashboardPageObj.ClickonGearIcon();
		AgencySetupPageObj.clickOnCRMCategories();
		AgencySetupPageObj.CreateCategoryButton.click();
		Thread.sleep(1000);
		Assert.assertTrue(utils.getModalHeader().equalsIgnoreCase("Create a Category"));
	}
	
	@Test(priority=3)
	public void VerifyCharacterLimitOnCategoryName() throws InterruptedException 
	{
		DashboardPageObj.ClickonGearIcon();
		AgencySetupPageObj.clickOnCRMCategories();
		AgencySetupPageObj.CreateCategoryButton.click();
		AgencySetupPageObj.Cat_Name.sendKeys("CategoryName CategoryName Categ");
		AgencySetupPageObj.CreateCategoryButtononPopUp.click();
		Thread.sleep(1000);
		Assert.assertTrue(AgencySetupPageObj.Cat_Name_Error_message.getText().equalsIgnoreCase("30 character limit."));
	}
	
	@Test(priority=4)
	public void VerifyLocationCheckboxFor_IncludeLocation_No() throws InterruptedException 
	{
		DashboardPageObj.ClickonGearIcon();
		AgencySetupPageObj.clickOnCRMCategories();
		AgencySetupPageObj.CreateCategoryButton.click();
		AgencySetupPageObj.ClickOnMultiChoiceButton("Include Location?", "No");
		AgencySetupPageObj.CreateCategoryButtononPopUp.click();
		Thread.sleep(1000);
		Assert.assertFalse(AgencySetupPageObj.LocationRequired_checkbox.isSelected());
	}
	
	
	@Test(priority=5)
	public void InactiveCategoryWithValidName() throws InterruptedException, IOException 
	{
		DashboardPageObj.ClickonGearIcon();
		AgencySetupPageObj.clickOnCRMCategories();
		String Categoryname = "Inactive_Category".concat(utils.getAlphaNumericString(4));
		AgencySetupPageObj.CreateCategory(Categoryname,"Yes","No", "CRM Overwrite Kolhe", "Active","CRM Basic Kolhe","New Keywords");

	}
	
	@Test(priority=6)
	public void ActiveCategoryWithValidName() throws InterruptedException, IOException 
	{
		DashboardPageObj.ClickonGearIcon();
		AgencySetupPageObj.clickOnCRMCategories();
		String Categoryname = "Active_Category".concat(utils.getAlphaNumericString(4));
		AgencySetupPageObj.CreateCategory(Categoryname,"Yes","No", "CRM Basic Kolhe", "Active","CRM Basic Kolhe","New Keywords");
		List<String>ActualCategory = AgencySetupPageObj.validateCategory();
		softAssertion.assertTrue(ActualCategory.get(0).equalsIgnoreCase(Categoryname));
		softAssertion.assertTrue(ActualCategory.get(1).equalsIgnoreCase("New, Keywords"));
		softAssertion.assertTrue(ActualCategory.get(2).equalsIgnoreCase("CRM Basic Kolhe"));
		softAssertion.assertTrue(ActualCategory.get(3).equalsIgnoreCase("CRM Basic Kolhe"));
		softAssertion.assertAll();
	}
	
	@Test(priority=7)
	public void ActiveCategoryNameWithSpecialCharacters() throws InterruptedException, IOException 
	{
		DashboardPageObj.ClickonGearIcon();
		AgencySetupPageObj.clickOnCRMCategories();
		String Categoryname = "SpecialCharacters_$%^".concat(utils.getAlphaNumericString(4));
		AgencySetupPageObj.CreateCategory(Categoryname,"Yes","No","CRM Overwrite Kolhe", "Active","CRM Basic Kolhe","New Keywords");
		List<String>ActualCategory = AgencySetupPageObj.validateCategory();
		softAssertion.assertTrue(ActualCategory.get(0).equalsIgnoreCase(Categoryname));
		softAssertion.assertTrue(ActualCategory.get(1).equalsIgnoreCase("New, Keywords"));
		softAssertion.assertTrue(ActualCategory.get(2).equalsIgnoreCase("CRM Overwrite Kolhe"));
		softAssertion.assertTrue(ActualCategory.get(3).equalsIgnoreCase("CRM Basic Kolhe"));
		softAssertion.assertAll();
	}
	
	@Test(priority=8)
	public void Create_category_with_location_set_to_No() throws InterruptedException, IOException 
	{
		DashboardPageObj.ClickonGearIcon();
		AgencySetupPageObj.clickOnCRMCategories();
		//String Categoryname = "NoLocation_$%^".concat(utils.getAlphaNumericString(4));
		AgencySetupPageObj.CreateCategory("Category_with_Loc_No","No","No", "CRM Overwrite Kolhe", "Active","CRM Basic Kolhe","New Keywords");
		List<String>ActualCategory = AgencySetupPageObj.validateCategory();
		softAssertion.assertTrue(ActualCategory.get(0).equalsIgnoreCase("Category_with_Loc_No"));
		softAssertion.assertTrue(ActualCategory.get(1).equalsIgnoreCase("New, Keywords"));
		softAssertion.assertTrue(ActualCategory.get(2).equalsIgnoreCase("CRM Overwrite Kolhe"));
		softAssertion.assertTrue(ActualCategory.get(3).equalsIgnoreCase("CRM Basic Kolhe"));
		softAssertion.assertAll();
	}
	
	@Test(priority=9)
	public void ActiveCategory_Location_No() throws InterruptedException, IOException 
	{
		DashboardPageObj.ClickonGearIcon();
		AgencySetupPageObj.clickOnCRMCategories();
		//String Categoryname = "NoLocation_$%^".concat(utils.getAlphaNumericString(4));
		AgencySetupPageObj.CreateCategory("Loc_No","No","No", "CRM Overwrite Kolhe", "Active","CRM Basic Kolhe","New Keywords");
		List<String>ActualCategory = AgencySetupPageObj.validateCategory();
		softAssertion.assertTrue(ActualCategory.get(0).equalsIgnoreCase("Loc_No"));
		softAssertion.assertTrue(ActualCategory.get(1).equalsIgnoreCase("New, Keywords"));
		softAssertion.assertTrue(ActualCategory.get(2).equalsIgnoreCase("CRM Overwrite Kolhe"));
		softAssertion.assertTrue(ActualCategory.get(3).equalsIgnoreCase("CRM Basic Kolhe"));
		softAssertion.assertAll();
	}
	
	
	
	
	@Test(priority=10)
	public void Edit_Category_Location_Yes() throws InterruptedException, IOException 
	{
		DashboardPageObj.ClickonGearIcon();
		AgencySetupPageObj.clickOnCRMCategories();
		//String Categoryname = "NoLocation_$%^".concat(utils.getAlphaNumericString(4));
		//AgencySetupPageObj.CreateCategory(Categoryname,"No", "CRM Overwrite Kolhe", "Active","CRM Basic Kolhe","New Keywords");
		AgencySetupPageObj.ClickonEditIconforSortableelement("Loc_No", "5");
		AgencySetupPageObj.EditCategory("EditedLoc_No", "Yes", "", "Active","", "");
		List<String>ActualCategory = AgencySetupPageObj.validateCategory();
		softAssertion.assertTrue(ActualCategory.get(0).equalsIgnoreCase("EditedLoc_No"));
		softAssertion.assertTrue(ActualCategory.get(1).equalsIgnoreCase("New, Keywords"));
		softAssertion.assertTrue(ActualCategory.get(2).equalsIgnoreCase("CRM Overwrite Kolhe"));
		softAssertion.assertTrue(ActualCategory.get(3).equalsIgnoreCase("CRM Basic Kolhe"));
		softAssertion.assertAll();		
	}
	
	@Test(priority=11)
	public void VerifyAssigneeIsMandatory() throws InterruptedException
	{
		DashboardPageObj.ClickonGearIcon();
		AgencySetupPageObj.clickOnCRMCategories();
		String Categoryname = "Active_Category".concat(utils.getAlphaNumericString(4));
		AgencySetupPageObj.CreateCategory(Categoryname,"Yes","No", "", "Active","CRM Basic Kolhe","New Keywords");
		AgencySetupPageObj.CreateCategoryButtononPopUp.click();
		Thread.sleep(1000);
		Assert.assertTrue(utils.GetErrorMessageForFieldName("Default Assignee").equalsIgnoreCase("The Default Assignee is required."));
	}
	
	@Test(priority=12)
	public void VerifyReadOnlyuserDoesNotAppearAsAssignee() throws InterruptedException
	{
		DashboardPageObj.ClickonGearIcon();
		AgencySetupPageObj.clickOnCRMCategories();
		AgencySetupPageObj.CreateCategoryButton.click();
		Thread.sleep(1000);
		AgencySetupPageObj.AllassigneeArrow.click();
		List<WebElement> Allassignees = AgencySetupPageObj.GetListOfAllAssignees();
		System.out.println(Allassignees);
		Assert.assertFalse(Allassignees.contains("CRM Readonly"));
	}
	
	@Test(priority=13)
	public void CheckNumberOFKeywords() throws InterruptedException
	{
		DashboardPageObj.ClickonGearIcon();
		AgencySetupPageObj.clickOnCRMCategories();
		String Categoryname = "10Keywords".concat(utils.getAlphaNumericString(4));
		AgencySetupPageObj.CreateCategory(Categoryname,"Yes","No", "CRM Overwrite Kolhe", "Active","CRM Basic Kolhe","one two three four five six seven eight nine ten");
		List<String>ActualCategory = AgencySetupPageObj.validateCategory();
		softAssertion.assertTrue(ActualCategory.get(0).equalsIgnoreCase(Categoryname));
		softAssertion.assertTrue(ActualCategory.get(1).equalsIgnoreCase("one, two, three, four, five, six, seven, eight, ni...more"));
		softAssertion.assertAll();
	}
	
	//When keyword of lenth 51 is passed , application trims it down to 50
	@Test(priority=14)
	public void CheckMAxLengthOfKeyword() throws InterruptedException
	{
		DashboardPageObj.ClickonGearIcon();
		AgencySetupPageObj.clickOnCRMCategories();
		String Categoryname = "LenthOfKeyword".concat(utils.getAlphaNumericString(4));
		AgencySetupPageObj.CreateCategory(Categoryname,"Yes","No", "CRM Overwrite Kolhe", "Active","CRM Basic Kolhe","KeywordKeywordKeywordKeywordKeywordKeywordKeywordKe");
		List<String>ActualCategory = AgencySetupPageObj.validateCategory();
		softAssertion.assertTrue(ActualCategory.get(0).equalsIgnoreCase(Categoryname));
		softAssertion.assertTrue(ActualCategory.get(1).equalsIgnoreCase("KeywordKeywordKeywordKeywordKeywordKeywordKeywordK"));
		softAssertion.assertAll();
	}
	
	
	//Even if we try to add duplicate keywords, only one will get added
	@Test(priority=15)
	public void DuplicateKeyword() throws InterruptedException
	{
		DashboardPageObj.ClickonGearIcon();
		AgencySetupPageObj.clickOnCRMCategories();
		String Categoryname = "Duplicate".concat(utils.getAlphaNumericString(4));
		AgencySetupPageObj.CreateCategory(Categoryname,"Yes","No", "CRM Overwrite Kolhe", "Active","","duplicate duplicate");
		List<String>ActualCategory = AgencySetupPageObj.validateCategory();
		softAssertion.assertTrue(ActualCategory.get(0).equalsIgnoreCase(Categoryname));
		softAssertion.assertTrue(ActualCategory.get(1).equalsIgnoreCase("duplicate"));
		softAssertion.assertFalse(ActualCategory.get(1).equalsIgnoreCase("duplicate, duplicate"));
		softAssertion.assertAll();
	}
	
	
	
	@Test(priority=16)
	public void VerifyKeywordCrossIcon() throws InterruptedException
	{
		DashboardPageObj.ClickonGearIcon();
		AgencySetupPageObj.clickOnCRMCategories();
		String Categoryname = "DeleteKeyword".concat(utils.getAlphaNumericString(4));
		AgencySetupPageObj.CreateCategoryByDeletingKeywords(Categoryname,"Yes", "CRM Overwrite Kolhe", "Active","","Test");
		List<String>ActualCategory = AgencySetupPageObj.validateCategory();
		softAssertion.assertTrue(ActualCategory.get(0).equalsIgnoreCase(Categoryname));
		softAssertion.assertTrue(ActualCategory.get(1).equalsIgnoreCase("-"));
		softAssertion.assertAll();
	}
	
	@Test(priority=17)
	public void VerifyReorderIconForInactiveCategories() throws InterruptedException
	{
		DashboardPageObj.ClickonGearIcon();
		AgencySetupPageObj.clickOnCRMCategories();
		String Categoryname = "Inactive".concat(utils.getAlphaNumericString(4));
		AgencySetupPageObj.CreateCategory(Categoryname,"Yes","No", "CRM Overwrite Kolhe", "Inactive","","Test");
		Assert.assertTrue(AgencySetupPageObj.ListOfSortableItemsIn_InactiveCat()==0);
	}
	
	@Test(priority=18)
	public void VerifyOtherCategory() throws InterruptedException
	{
		DashboardPageObj.ClickonGearIcon();
		AgencySetupPageObj.clickOnCRMCategories();
		Assert.assertTrue(AgencySetupPageObj.OtherCategoryName.getText().equals("Other"));
	}

	@Test(priority=19)
	public void Create_category_with_location_required() throws InterruptedException 
	{
		DashboardPageObj.ClickonGearIcon();
		AgencySetupPageObj.clickOnCRMCategories();
		String Categoryname = "Location Required".concat(utils.getAlphaNumericString(4));
		AgencySetupPageObj.CreateCategory(Categoryname,"Yes","Yes", "CRM Overwrite Kolhe", "Active","CRM Basic Kolhe","Location required");
		List<String>ActualCategory = AgencySetupPageObj.validateCategory();
		softAssertion.assertTrue(ActualCategory.get(0).equalsIgnoreCase(Categoryname));
		softAssertion.assertTrue(ActualCategory.get(1).equalsIgnoreCase("Location, required"));
		softAssertion.assertAll();
	}


}
