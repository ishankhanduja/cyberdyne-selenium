package testCases;

import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.AssertJUnit;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.qa.Analyzer.Retriable;
import com.qa.comcate.base.TestBase;
import com.qa.comcate.pages.AgenciesLandingPage;
import com.qa.comcate.pages.AgencySetupPage;
import com.qa.comcate.pages.CSDP_Page;
import com.qa.comcate.pages.CSLP_Page;
import com.qa.comcate.pages.ChromelessAppPage;
import com.qa.comcate.pages.DashboardPage;
import com.qa.comcate.pages.LoginPage;
import com.qa.comcate.pages.YopMailPage;
import com.qa.comcate.utils.CommonUtils;
import com.qa.comcate.utils.SwitchWindows;

public class CRM_Email_settingTests extends TestBase {

	LoginPage LoginPageObj;
	AgenciesLandingPage AgenciesLandingPageObj;
	DashboardPage DashboardPageObj;
	AgencySetupPage AgencySetupPageObj;
	ChromelessAppPage ChromelessAppObj;
	SoftAssert softAssertion;
	CommonUtils utils;
	CSLP_Page cslp_Pageobj;
	CSDP_Page csdp_Pageobj;
	SwitchWindows switchWindowObj;

	public CRM_Email_settingTests() {
		super();
	}

	@BeforeMethod()
	@Retriable(attempts = 4)
	public void setup() throws Exception {
		initiate();
		softAssertion = new SoftAssert();
		LoginPageObj = new LoginPage();
		AgenciesLandingPageObj = new AgenciesLandingPage();
		DashboardPageObj = new DashboardPage();
		AgencySetupPageObj = new AgencySetupPage();
		ChromelessAppObj = new ChromelessAppPage();
		cslp_Pageobj = new CSLP_Page();
		csdp_Pageobj = new CSDP_Page();
		utils = new CommonUtils();
		switchWindowObj = new SwitchWindows();
		LoginPageObj.LoginToApplicationWithValid("support@comcate.com", "test@123");
		System.out.println(utils.ReadFromFile());
		WebElement SearchedAgency = AgenciesLandingPageObj.SearchAgency(utils.ReadFromFile());
		Thread.sleep(2000);
		Assert.assertEquals(SearchedAgency.getText(), utils.ReadFromFile());
		AgenciesLandingPageObj.LoginAsSuperAdmin();
	}

	@AfterMethod
	public void tearDown() {
		driver.quit();
	}

	@Test(priority=1)
	public void VerifySubmission_Received_Active() throws InterruptedException {
		DashboardPageObj.ClickonGearIcon();
		AgencySetupPageObj.clickOnCRMEmailSettings();
		int Active_template_before = Integer.parseInt(AgencySetupPageObj.Section_header_active.getText().replaceAll("[^0-9]", ""));
		AgencySetupPageObj.Make_submissionReceived_template_active();
		int Active_template_After = Integer.parseInt(AgencySetupPageObj.Section_header_active.getText().replaceAll("[^0-9]", ""));
		Assert.assertTrue(Active_template_After == Active_template_before + 1);
	}
	
	@Test(priority=2)
	public void VerifySubmission_Closed_Active() throws InterruptedException {
		DashboardPageObj.ClickonGearIcon();
		AgencySetupPageObj.clickOnCRMEmailSettings();
		int Active_template_before = Integer.parseInt(AgencySetupPageObj.Section_header_active.getText().replaceAll("[^0-9]", ""));
		AgencySetupPageObj.Make_submissionClosed_template_active();
		int Active_template_After = Integer.parseInt(AgencySetupPageObj.Section_header_active.getText().replaceAll("[^0-9]", ""));
		Assert.assertTrue(Active_template_After == Active_template_before + 1);
	}
	
	@Test(priority=3)
	public void VerifyNoticeIssued_Active() throws InterruptedException {
		DashboardPageObj.ClickonGearIcon();
		AgencySetupPageObj.clickOnCRMEmailSettings();
		int Active_template_before = Integer.parseInt(AgencySetupPageObj.Section_header_active.getText().replaceAll("[^0-9]", ""));
		AgencySetupPageObj.Make_NoticeIssued_template_active();
		int Active_template_After = Integer.parseInt(AgencySetupPageObj.Section_header_active.getText().replaceAll("[^0-9]", ""));
		Assert.assertTrue(Active_template_After == Active_template_before + 1);
	}
	
	

}
