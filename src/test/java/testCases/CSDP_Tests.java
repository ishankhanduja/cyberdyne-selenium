package testCases;

import java.io.IOException;
import java.security.GeneralSecurityException;

import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.qa.Analyzer.Retriable;
import com.qa.comcate.GmailAPiLib.GMail;
import com.qa.comcate.base.TestBase;
import com.qa.comcate.pages.AgenciesLandingPage;
import com.qa.comcate.pages.AgencySetupPage;
import com.qa.comcate.pages.CSDP_Page;
import com.qa.comcate.pages.CSLP_Page;
import com.qa.comcate.pages.ChromelessAppPage;
import com.qa.comcate.pages.DashboardPage;
import com.qa.comcate.pages.LoginPage;
import com.qa.comcate.utils.CommonUtils;
import com.qa.comcate.utils.SwitchWindows;

public class CSDP_Tests extends TestBase {

	LoginPage LoginPageObj;
	AgenciesLandingPage AgenciesLandingPageObj;
	DashboardPage DashboardPageObj;
	AgencySetupPage AgencySetupPageObj;
	ChromelessAppPage ChromelessAppObj;
	SoftAssert softAssertion;
	CommonUtils utils;
	CSLP_Page cslp_Pageobj;
	CSDP_Page csdp_Pageobj;
	SwitchWindows switchWindowObj;
	String AgencyID;
	

	public CSDP_Tests() {
		super();
	}

	@BeforeMethod
	//@Retriable(attempts = 4)
	public void setup() throws Exception {
		initiate();
		softAssertion = new SoftAssert();
		LoginPageObj = new LoginPage();
		AgenciesLandingPageObj = new AgenciesLandingPage();
		DashboardPageObj = new DashboardPage();
		AgencySetupPageObj = new AgencySetupPage();
		ChromelessAppObj = new ChromelessAppPage();
		cslp_Pageobj = new CSLP_Page();
		csdp_Pageobj = new CSDP_Page();
		utils = new CommonUtils();
		switchWindowObj = new SwitchWindows();
		AgencyID = utils.ReadAgencyIDFromFile();
		LoginPageObj.LoginToApplicationWithValid("support@comcate.com", "test@123");
		System.out.println(utils.ReadFromFile());
		WebElement SearchedAgency = AgenciesLandingPageObj.SearchAgency(utils.ReadFromFile());
		Thread.sleep(2000);
		Assert.assertEquals(SearchedAgency.getText(), utils.ReadFromFile());
		AgenciesLandingPageObj.LoginAsSuperAdmin();
		AgenciesLandingPageObj.NavigateToCSLP();
	
	}

	@AfterMethod
	public void tearDown() {
		driver.quit();
	}

	@Test(priority=1)
	public void CRM_019_CRM_020_CRM_045_Verify_PhotoTile_whenPhotos_submitted_by_citizen() throws InterruptedException {
		switchWindowObj.SwitchBytitle("Comcate Admin");
		AgenciesLandingPageObj.NavigateToChromlessApp();
		ChromelessAppObj.IssueDescription.sendKeys("Checking Photos Tile on CSDP when multi photos attached");
		ChromelessAppObj.UploadMultiplePhotosbyCitizen();
		ChromelessAppObj.ClickOnNExtButton_Description_whenPhotosUploaded();
		ChromelessAppObj.SelectCategory("");
		ChromelessAppObj.SelectMap("No");
		ChromelessAppObj.EnterCitizenInfo("No", "", "", "");
		switchWindowObj.SwitchBytitle("Customer Submissions Page");
		driver.navigate().refresh();
		cslp_Pageobj.Click_On_Latest_issues();
		int photo_count_in_tileHeader = Integer
				.parseInt(csdp_Pageobj.Photo_tileHeader.getText().replaceAll("[^0-9]", ""));
		softAssertion.assertTrue(csdp_Pageobj.Photos_Thumbnails.size() == photo_count_in_tileHeader);
		softAssertion.assertTrue(csdp_Pageobj.Photos_Thumbnails.size() == 3);
		softAssertion.assertTrue(csdp_Pageobj.Photos_Tile.isDisplayed());
		softAssertion.assertTrue(csdp_Pageobj.CSDP_Customer_name.getText().equals("Unknown"));
		softAssertion.assertTrue(csdp_Pageobj.CSDP_Description.getText()
				.equals("Checking Photos Tile on CSDP when multi photos attached"));
		csdp_Pageobj.Photos_Thumbnails.get(0).click();
		softAssertion.assertTrue(csdp_Pageobj.Attachment_Edit_body.isDisplayed());
		softAssertion.assertAll();
	}

	@Test(priority=2)
	public void CRM_018_CRM_046_Verify_functionality_ofPhoto_Icon_and_otherSections() throws InterruptedException {
		cslp_Pageobj.Click_On_Latest_issues();
		int photo_count_in_tileHeader = Integer
				.parseInt(csdp_Pageobj.Photo_tileHeader.getText().replaceAll("[^0-9]", ""));
		int photo_count_in_PhotoIcon = Integer
				.parseInt(csdp_Pageobj.countOF_Photo_icon_in_Header.getText().replaceAll("[^0-9]", ""));
		softAssertion.assertTrue(photo_count_in_PhotoIcon == photo_count_in_tileHeader);
		softAssertion.assertTrue(csdp_Pageobj.CSDP_PageHeader.getText().contains("Customer Submission #21-"));
	}

	@Test(priority=3)
	public void CRM_047_Verify_functionality_of_deletePhotos() throws InterruptedException {
		switchWindowObj.SwitchBytitle("Comcate Admin");
		AgenciesLandingPageObj.NavigateToChromlessApp();
		ChromelessAppObj.IssueDescription.sendKeys("Checking Photos Tile on CSDP when multi photos attached");
		ChromelessAppObj.UploadMultiplePhotosbyCitizen();
		ChromelessAppObj.ClickOnNExtButton_Description_whenPhotosUploaded();
		ChromelessAppObj.SelectCategory("");
		ChromelessAppObj.SelectMap("No");
		ChromelessAppObj.EnterCitizenInfo("No", "", "", "");
		switchWindowObj.SwitchBytitle("Customer Submissions Page");
		driver.navigate().refresh();
		cslp_Pageobj.Click_On_Latest_issues();
		int photo_count_in_tileHeader_before_deleting = Integer
				.parseInt(csdp_Pageobj.Photo_tileHeader.getText().replaceAll("[^0-9]", ""));
		csdp_Pageobj.Delete_photo();
		int photo_count_in_tileHeader_after_deleting = Integer
				.parseInt(csdp_Pageobj.Photo_tileHeader.getText().replaceAll("[^0-9]", ""));
		System.out.println(photo_count_in_tileHeader_after_deleting);
		Assert.assertTrue(photo_count_in_tileHeader_before_deleting - 1 == photo_count_in_tileHeader_after_deleting);
	}

	@Test(priority=4)
	public void CRM_048_Verify_functionality_for_adding_photo_onCSDP() throws InterruptedException {
		cslp_Pageobj.Click_On_Latest_issues();
		int photo_count_in_tileHeader_before_Adding = Integer
				.parseInt(csdp_Pageobj.Photo_tileHeader.getText().replaceAll("[^0-9]", ""));
		csdp_Pageobj.UploadSinglePhotos_OnCSDP();
		int photo_count_in_tileHeader_after_Adding = Integer
				.parseInt(csdp_Pageobj.Photo_tileHeader.getText().replaceAll("[^0-9]", ""));
		System.out.println(photo_count_in_tileHeader_after_Adding);
		Assert.assertTrue(photo_count_in_tileHeader_before_Adding + 1 == photo_count_in_tileHeader_after_Adding);
	}

	@Test(priority=5)
	public void CRM_048_Verify_functionality_for_adding_photoWithInvalidFormat_onCSDP() throws InterruptedException {
		cslp_Pageobj.Click_On_Latest_issues();
		String Error_message = csdp_Pageobj.UploadSinglePhoto_of_jpeFormat_OnCSDP();
		System.out.println(Error_message);
		Assert.assertTrue(Error_message.equals("One/more files with unsupported format is/are discarded."));
	}

	@Test(priority=6)
	public void CRM_048_Verify_functionality_for_adding_photogreaterThan15MB_onCSDP() throws InterruptedException {
		cslp_Pageobj.Click_On_Latest_issues();
		String Error_message = csdp_Pageobj.UploadSinglePhoto_of_15MB_OnCSDP();
		System.out.println(Error_message);
		Assert.assertTrue(Error_message.contains("File size cannot exceed 15MB"));
	}

	@Test(priority=7)
	public void CRM_035_Verify_readonlyUSer_DoesNotSee_AsigneedropDown() throws InterruptedException {
		AgencySetupPageObj.Login_with_user("Praful.comcate+Readonly".concat(AgencyID).concat("@gmail.com"), "Test@123");
		AgenciesLandingPageObj.NavigateToCSLP();
		cslp_Pageobj.Click_On_Latest_issues();
		Assert.assertTrue(utils.Check_existance_of_dropDown_for_ReadOnly("Assigned to").size() == 0);
	}

	@Test(priority=8)
	public void CRM_036_Verify_BasicUSer_abletoChange_Asignee() throws InterruptedException {
		AgencySetupPageObj.Login_with_user("Praful.comcate+Basic".concat(AgencyID).concat("@gmail.com"), "Test@123");
		AgenciesLandingPageObj.NavigateToCSLP();
		cslp_Pageobj.Click_On_Latest_issues();
		utils.Check_existance_of_dropDown_for_ReadOnly("Assigned to").get(0).click();
		csdp_Pageobj.Change_Asignee("CRM Basic Kolhe");
		Assert.assertTrue(utils.Get_current_selection_from_dropdownCSDP("Assigned to").equals("CRM Basic Kolhe"));
	}

	@Test(priority=9)
	public void CRM_037_Verify_ReadOnlyUser_AreNotdisplayed_in_asignee_dropdown() throws InterruptedException {
		AgencySetupPageObj.Login_with_user("Praful.comcate+Basic".concat(AgencyID).concat("@gmail.com"), "Test@123");
		AgenciesLandingPageObj.NavigateToCSLP();
		cslp_Pageobj.Click_On_Latest_issues();
		utils.Check_existance_of_dropDown_for_ReadOnly("Assigned to").get(0).click();
		softAssertion.assertFalse(csdp_Pageobj.List_Of_AllAsignees().contains("CRM Readonly Kolhe"));
		softAssertion.assertTrue(csdp_Pageobj.List_Of_AllAsignees().contains("CRM Overwrite Kolhe"));
		softAssertion.assertTrue(csdp_Pageobj.List_Of_AllAsignees().contains("CRM Basic Kolhe"));
		softAssertion.assertAll();
	}

	@Test(priority=10)
	public void CRM_017_CRM_021_Close_Submission() throws InterruptedException {
		cslp_Pageobj.Click_On_Latest_issues();
		csdp_Pageobj.Close_a_submission();
		csdp_Pageobj.Click_on_cross_icon();
		Assert.assertTrue(cslp_Pageobj.Get_content_of_latest_issue().get(3).getText().equals("Closed"));
	}

	@Test(priority=11)
	public void CRM_058_Reopen_button() throws InterruptedException {
		cslp_Pageobj.Click_On_Latest_issues();
		csdp_Pageobj.Reopen_submission();
		csdp_Pageobj.Click_on_cross_icon();
		Assert.assertTrue(cslp_Pageobj.Get_content_of_latest_issue().get(3).getText().equals("Open"));
	}

	@Test(priority=12)
	public void CRM_131_Verify_BasicUSer_abletoChange_Category_with_update_asignee() throws InterruptedException {
		AgencySetupPageObj.Login_with_user("Praful.comcate+Basic".concat(AgencyID).concat("@gmail.com"), "Test@123");
		AgenciesLandingPageObj.NavigateToCSLP();
		cslp_Pageobj.Click_On_Latest_issues();
		utils.Check_existance_of_dropDown_for_ReadOnly("Assigned to").get(0).click();
		csdp_Pageobj.Change_Asignee("CRM Basic Kolhe");
		softAssertion
				.assertTrue(utils.Get_current_selection_from_dropdownCSDP("Assigned to").equals("CRM Basic Kolhe"));
		utils.Check_existance_of_dropDown_for_ReadOnly("Category").get(0).click();
		csdp_Pageobj.Change_Category("Inactive_Category", "Yes");
		softAssertion
				.assertTrue(utils.Get_current_selection_from_dropdownCSDP("Assigned to").equals("CRM Overwrite Kolhe"));
		softAssertion
				.assertTrue(utils.Get_current_selection_from_dropdownCSDP("Category").contains("Inactive_Category"));
		softAssertion.assertAll();
	}

	@Test(priority=13)
	public void CRM_131_Verify_BasicUSer_abletoChange_Category_without_update_asignee() throws InterruptedException {
		AgencySetupPageObj.Login_with_user("Praful.comcate+Basic".concat(AgencyID).concat("@gmail.com"), "Test@123");
		AgenciesLandingPageObj.NavigateToCSLP();
		cslp_Pageobj.Click_On_Latest_issues();
		utils.Check_existance_of_dropDown_for_ReadOnly("Assigned to").get(0).click();
		csdp_Pageobj.Change_Asignee("CRM Basic Kolhe");
		softAssertion
				.assertTrue(utils.Get_current_selection_from_dropdownCSDP("Assigned to").equals("CRM Basic Kolhe"));
		utils.Check_existance_of_dropDown_for_ReadOnly("Category").get(0).click();
		csdp_Pageobj.Change_Category("Inactive_Category", "No");
		softAssertion
				.assertTrue(utils.Get_current_selection_from_dropdownCSDP("Assigned to").equals("CRM Basic Kolhe"));
		softAssertion
				.assertTrue(utils.Get_current_selection_from_dropdownCSDP("Category").contains("Inactive_Category"));
		softAssertion.assertAll();
	}

	@Test(priority=14)
	public void CRM_035_Verify_readonlyUSer_DoesNotSee_CategorydropDown() throws InterruptedException {
		AgencySetupPageObj.Login_with_user("Praful.comcate+Readonly".concat(AgencyID).concat("@gmail.com"), "Test@123");
		AgenciesLandingPageObj.NavigateToCSLP();
		cslp_Pageobj.Click_On_Latest_issues();
		Assert.assertTrue(utils.Check_existance_of_dropDown_for_ReadOnly("Category").size() == 0);
	}

	@Test(priority=15)
	public void CRM_135_Verify_EmailTile_WhenEmailID_submitted_by_citizen() throws InterruptedException {
		switchWindowObj.SwitchBytitle("Comcate Admin");
		AgenciesLandingPageObj.NavigateToChromlessApp();
		ChromelessAppObj.IssueDescription.sendKeys("Checking Email Tile on CSDP when email id provided");
		ChromelessAppObj.UploadMultiplePhotosbyCitizen();
		ChromelessAppObj.ClickOnNExtButton_Description_whenPhotosUploaded();
		ChromelessAppObj.SelectCategory("");
		ChromelessAppObj.SelectMap("No");
		ChromelessAppObj.EnterCitizenInfo("No", "", "", "Praful.comcate+citizen@gmail.com");
		switchWindowObj.SwitchBytitle("Customer Submissions Page");
		driver.navigate().refresh();
		cslp_Pageobj.Click_On_Latest_issues();
		softAssertion.assertTrue(csdp_Pageobj.Email_tile.size() == 1);
		int EmailCountAppearing_inHeader = Integer
				.parseInt(csdp_Pageobj.Email_tile_Header.getText().replaceAll("[^0-9]", ""));
		softAssertion.assertTrue(csdp_Pageobj.List_of_emails.size() == EmailCountAppearing_inHeader);
		softAssertion.assertAll();

	}

	@Test(priority=16)
	public void CRM_136_Verify_EmailTile_WhenEmailID_Notsubmitted_by_citizen() throws InterruptedException {
		switchWindowObj.SwitchBytitle("Comcate Admin");
		AgenciesLandingPageObj.NavigateToChromlessApp();
		ChromelessAppObj.IssueDescription.sendKeys("Checking Email Tile on CSDP when email id not provided");
		ChromelessAppObj.UploadMultiplePhotosbyCitizen();
		ChromelessAppObj.ClickOnNExtButton_Description_whenPhotosUploaded();
		ChromelessAppObj.SelectCategory("");
		ChromelessAppObj.SelectMap("No");
		ChromelessAppObj.EnterCitizenInfo("No", "", "", "");
		switchWindowObj.SwitchBytitle("Customer Submissions Page");
		driver.navigate().refresh();
		cslp_Pageobj.Click_On_Latest_issues();

		Assert.assertTrue(csdp_Pageobj.Email_tile.size() == 0);
	}

	@Test(priority = 17)
	public void CRM_139_Verify_AsigneeName_appearing_inEmailContent() throws InterruptedException {
		switchWindowObj.SwitchBytitle("Comcate Admin");
		AgenciesLandingPageObj.NavigateToChromlessApp();
		ChromelessAppObj.IssueDescription.sendKeys("Checking Asignee name in email content");
		ChromelessAppObj.UploadMultiplePhotosbyCitizen();
		ChromelessAppObj.ClickOnNExtButton_Description_whenPhotosUploaded();
		ChromelessAppObj.SelectCategory("Active_Category");
		ChromelessAppObj.SelectMap("No");
		ChromelessAppObj.EnterCitizenInfo("No", "", "", "Praful.comcate+citizen@gmail.com");
		switchWindowObj.SwitchBytitle("Customer Submissions Page");
		driver.navigate().refresh();
		cslp_Pageobj.Click_On_Latest_issues();

		Assert.assertTrue(csdp_Pageobj.List_of_Email_Content.get(0).getText().contains("CRM Basic Kolhe"));
	}

	@Test(priority = 18)
	public void CRM_140_Verify_Date_appearing_on_emailTile() throws InterruptedException {
		switchWindowObj.SwitchBytitle("Comcate Admin");
		AgenciesLandingPageObj.NavigateToChromlessApp();
		ChromelessAppObj.IssueDescription.sendKeys("Checking Asignee name in email content");
		ChromelessAppObj.UploadMultiplePhotosbyCitizen();
		ChromelessAppObj.ClickOnNExtButton_Description_whenPhotosUploaded();
		ChromelessAppObj.SelectCategory("Active_Category");
		ChromelessAppObj.SelectMap("No");
		ChromelessAppObj.EnterCitizenInfo("No", "", "", "Praful.comcate+citizen@gmail.com");
		switchWindowObj.SwitchBytitle("Customer Submissions Page");
		driver.navigate().refresh();
		cslp_Pageobj.Click_On_Latest_issues();
		String CurrentDateTime = utils.timestampGeneratorForCSDP();
		System.out.println(CurrentDateTime);
		Assert.assertTrue(csdp_Pageobj.List_of_Email_Dates.get(0).getText().contains(CurrentDateTime));
	}

	@Test(priority = 19)
	public void CRM_141_Verify_MoreLink_isClickable() throws InterruptedException {
		cslp_Pageobj.Click_On_Latest_issues();
		csdp_Pageobj.List_of_More_links.get(0).click();
		Assert.assertTrue(csdp_Pageobj.List_of_Less_links.get(0).isDisplayed());
	}

	@Test(priority = 20)
	public void CRM_142_Verify_functionality_of_less_link() throws InterruptedException {
		cslp_Pageobj.Click_On_Latest_issues();
		csdp_Pageobj.List_of_More_links.get(0).click();
		csdp_Pageobj.List_of_Less_links.get(0).click();
		Assert.assertTrue(csdp_Pageobj.List_of_More_links.get(0).isDisplayed());
		;
	}

	@Test(priority = 21)
	public void CRM_187_Verify_functionality_of_Expand_email_icon() throws InterruptedException {
		switchWindowObj.SwitchBytitle("Comcate Admin");
		AgenciesLandingPageObj.NavigateToChromlessApp();
		ChromelessAppObj.IssueDescription.sendKeys("Checking Asignee name in email content");
		ChromelessAppObj.UploadMultiplePhotosbyCitizen();
		ChromelessAppObj.ClickOnNExtButton_Description_whenPhotosUploaded();
		ChromelessAppObj.SelectCategory("Active_Category");
		ChromelessAppObj.SelectMap("No");
		ChromelessAppObj.EnterCitizenInfo("No", "", "", "Praful.comcate+citizen@gmail.com");
		switchWindowObj.SwitchBytitle("Customer Submissions Page");
		driver.navigate().refresh();
		cslp_Pageobj.Click_On_Latest_issues();
		csdp_Pageobj.Email_Expand_icon.click();
		Assert.assertTrue(
				csdp_Pageobj.Email_subject_On_Email_popup_CSDP.getAttribute("value").contains("We have received your submission!"));
	}

	@Test(priority = 22)
	public void CRM_189_Verify_Edit_subject_functionality_on_Email_popup()
			throws InterruptedException, IOException, GeneralSecurityException {
		int oldMailcount = GMail.getTotalCountOfMails();
		cslp_Pageobj.Click_On_Latest_issues();
		csdp_Pageobj.Email_Expand_icon.click();
		String Original_subject = csdp_Pageobj.Email_subject_On_Email_popup_CSDP.getAttribute("value");
		String Edited_Subject = "Edited " + utils.getAlphaNumericString(4) + " " + Original_subject;
		csdp_Pageobj.Email_subject_On_Email_popup_CSDP.clear();
		csdp_Pageobj.Email_subject_On_Email_popup_CSDP.sendKeys(Edited_Subject);
		csdp_Pageobj.Email_Body_On_Email_popup_CSDP.sendKeys("1. We will keep you posted");
		csdp_Pageobj.Email_SendButton_On_Email_popup_CSDP.click();
		int count = 0;
		int newemailcount = GMail.getTotalCountOfMails();
		while ((newemailcount != oldMailcount + 1) && count <= 20) {
			Thread.sleep(500);
			newemailcount = GMail.getTotalCountOfMails();
			count++;
			if (count == 20) {
				System.out.println("Timed out. Email not received yet");
				break;
			}
		}
		String Email_subject_in_inbox = "";
		if (newemailcount == oldMailcount + 1) {
			System.out.println("New count" + newemailcount);
			Email_subject_in_inbox = GMail.GetEmailSubject("");
		}
		Assert.assertTrue(Email_subject_in_inbox.equals(Edited_Subject));

	}

	@Test(priority = 23)
	public void CRM_190_Verify_Subject_body_is_mandatory()
			throws InterruptedException, IOException, GeneralSecurityException {
		cslp_Pageobj.Click_On_Latest_issues();
		csdp_Pageobj.Email_Expand_icon.click();
		csdp_Pageobj.Email_subject_On_Email_popup_CSDP.clear();
		csdp_Pageobj.Email_subject_On_Email_popup_CSDP.sendKeys("");
		csdp_Pageobj.Email_Body_On_Email_popup_CSDP.click();
		csdp_Pageobj.Email_SendButton_On_Email_popup_CSDP.click();
		Assert.assertTrue(
				csdp_Pageobj.Email_BodyErrorMsg_On_Email_popup_CSDP.getText().equals("The Email Body is required."));

	}

	@Test(priority = 24)
	public void CRM_193_Verify_functionality_of_cancel_button()
			throws InterruptedException, IOException, GeneralSecurityException {
		cslp_Pageobj.Click_On_Latest_issues();
		csdp_Pageobj.Email_Expand_icon.click();
		csdp_Pageobj.Email_subject_On_Email_popup_CSDP.clear();
		csdp_Pageobj.Email_subject_On_Email_popup_CSDP.sendKeys("");
		csdp_Pageobj.Email_Body_On_Email_popup_CSDP.click();
		csdp_Pageobj.Email_Body_On_Email_popup_CSDP.sendKeys("Email Body entered");
		csdp_Pageobj.Email_CancelButton_On_Email_popup_CSDP.click();
		csdp_Pageobj.Email_SendButton_On_Email_popup_CSDP.click();
		Assert.assertTrue(
				csdp_Pageobj.Email_BodyErrorMsg_On_Email_popup_CSDP.getText().equals("The Email Body is required."));
	}

	@Test(priority = 25)
	public void CRM_194_Verify_Reply_Email_functionality_with_new_subject_line()
			throws InterruptedException, IOException, GeneralSecurityException {
		int oldMailcount = GMail.getTotalCountOfMails();
		cslp_Pageobj.Click_On_Latest_issues();
		String Old_subject = csdp_Pageobj.Reply_email_Subject.getAttribute("value");
		String New_subject = "Replied " + utils.getAlphaNumericString(4) + " " + Old_subject;
		csdp_Pageobj.Reply_email_Subject.clear();
		csdp_Pageobj.Reply_email_Subject.sendKeys(New_subject);
		csdp_Pageobj.Reply_email_Body.sendKeys("Reply Body entered");
		csdp_Pageobj.Reply_email_Send_button.click();
		int count = 0;
		int newemailcount = GMail.getTotalCountOfMails();
		while ((newemailcount != oldMailcount + 1) && count <= 20) {
			Thread.sleep(500);
			newemailcount = GMail.getTotalCountOfMails();
			count++;
			if (count == 20) {
				System.out.println("Timed out. Email not received yet");
				break;
			}
		}
		String Email_body_in_inbox = "";
		if (newemailcount == oldMailcount + 1) {
			System.out.println("New count" + newemailcount);
			Email_body_in_inbox = GMail.GetEmailSubject("");
		}
		Assert.assertTrue(Email_body_in_inbox.equals(New_subject));
	}

	@Test(priority = 26)
	public void CRM_134_Verify_If_user_is_able_to_uncheck_collaberator() throws InterruptedException {
		switchWindowObj.SwitchBytitle("Comcate Admin");
		AgenciesLandingPageObj.NavigateToChromlessApp();
		ChromelessAppObj.IssueDescription.sendKeys("Checking Email Tile on CSDP when email id not provided");
		ChromelessAppObj.UploadMultiplePhotosbyCitizen();
		ChromelessAppObj.ClickOnNExtButton_Description_whenPhotosUploaded();
		ChromelessAppObj.SelectCategory("");
		ChromelessAppObj.SelectMap("No");
		ChromelessAppObj.EnterCitizenInfo("No", "", "", "");
		switchWindowObj.SwitchBytitle("Customer Submissions Page");
		driver.navigate().refresh();
		cslp_Pageobj.Click_On_Latest_issues();
		utils.Check_existance_of_dropDown_for_ReadOnly("Collaborator/s").get(0).click();
		softAssertion.assertTrue(csdp_Pageobj.Checked_collaberator_name.getText().equals("CRM Basic Kolhe"));
		csdp_Pageobj.Deselect_collaberator();
		softAssertion.assertTrue(csdp_Pageobj.Checked_collaberator_Checkbox.size()==0);
		softAssertion.assertAll();
	}

	@Test(priority = 27)
	public void CRM_133_Verify_If_user_is_able_to_select_collaberator() throws InterruptedException {
		cslp_Pageobj.Click_On_Latest_issues();
		utils.Check_existance_of_dropDown_for_ReadOnly("Collaborator/s").get(0).click();
		csdp_Pageobj.Select_collaberator();
		softAssertion.assertTrue(csdp_Pageobj.Checked_collaberator_Checkbox.size()==1);
		softAssertion.assertTrue(csdp_Pageobj.Checked_collaberator_name.getText().equals("CRM Basic Kolhe"));
		softAssertion.assertAll();
	}

	//@Test
	public void CRM_threid()
			throws InterruptedException, IOException, GeneralSecurityException {
		
			String ThreadID = GMail.GetThreadID("New email to test reply");
			System.out.println(ThreadID);
		
	}

	//@Test
	/*public void opentab() throws InterruptedException {
		yopmailPageobj.OpenYopmail();
		yopmailPageobj.NavigateTOInbox("Praful");
		System.out.println(yopmailPageobj.Get_Count_Of_Emails());
	}
*/
}
