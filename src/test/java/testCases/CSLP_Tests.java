package testCases;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.qa.Analyzer.Retriable;
import com.qa.comcate.base.TestBase;
import com.qa.comcate.pages.AgenciesLandingPage;
import com.qa.comcate.pages.AgencySetupPage;
import com.qa.comcate.pages.CSLP_Page;
import com.qa.comcate.pages.ChromelessAppPage;
import com.qa.comcate.pages.DashboardPage;
import com.qa.comcate.pages.LoginPage;
import com.qa.comcate.utils.CommonUtils;
import com.qa.comcate.utils.SwitchWindows;

public class CSLP_Tests extends TestBase {

	LoginPage LoginPageObj;
	AgenciesLandingPage AgenciesLandingPageObj;
	DashboardPage DashboardPageObj;
	AgencySetupPage AgencySetupPageObj;
	ChromelessAppPage ChromelessAppObj;
	SoftAssert softAssertion;
	CommonUtils utils;
	CSLP_Page cslp_Pageobj;
	SwitchWindows switchWindowObj;
	String AgencyID;

	public CSLP_Tests() {
		super();
	}

	@BeforeMethod
	//@Retriable(attempts = 4)
	public void setup() throws Exception {
		initiate();
		softAssertion = new SoftAssert();
		LoginPageObj = new LoginPage();
		AgenciesLandingPageObj = new AgenciesLandingPage();
		DashboardPageObj = new DashboardPage();
		AgencySetupPageObj = new AgencySetupPage();
		ChromelessAppObj = new ChromelessAppPage();
		cslp_Pageobj = new CSLP_Page();
		utils = new CommonUtils();
		switchWindowObj = new SwitchWindows();
		AgencyID = utils.ReadAgencyIDFromFile();
		LoginPageObj.LoginToApplicationWithValid("support@comcate.com", "test@123");
		System.out.println(utils.ReadFromFile());
		WebElement SearchedAgency = AgenciesLandingPageObj.SearchAgency(utils.ReadFromFile());
		Thread.sleep(2000);
		//Assert.assertEquals(SearchedAgency.getText(), utils.ReadFromFile());
		AgenciesLandingPageObj.LoginAsSuperAdmin();
		AgenciesLandingPageObj.NavigateToCSLP();

	}

	@AfterMethod
	public void tearDown() {
		driver.quit();
	}

	@Test(priority = 1)
	public void VerifyHeaderOf_CSLP_Page() throws InterruptedException {

		softAssertion.assertTrue(cslp_Pageobj.GetPageHeader().equals("Customer Submissions"));

	}

	@Test(priority = 2)
	public void Check_Sorting_for_issue_description_on_first_Click() throws InterruptedException {
		List<String> ContentBeforeSorting = cslp_Pageobj.Get_Entire_Content_Of_Issue();
		softAssertion.assertTrue(cslp_Pageobj.ChecksortingOnFirstClick(cslp_Pageobj.Issue_Description_header,
				cslp_Pageobj.ListOfIssue_Descriptions));
		List<String> ContentAfterSorting = cslp_Pageobj.Get_Entire_Content_Of_Issue();
		softAssertion.assertTrue(ContentBeforeSorting.equals(ContentAfterSorting));
		softAssertion.assertAll();
	}

	/*@Test(priority = 3)
	public void Check_Sorting_for_issue_Number_on_first_Click() throws InterruptedException {
		Assert.assertTrue(cslp_Pageobj.ChecksortingOnFirstClick(cslp_Pageobj.Submission_number_header,
				cslp_Pageobj.ListOfIssue_Numbers));
	}*/

	@Test(priority = 4)
	public void Check_Sorting_for_issue_description_on_Second_Click() throws InterruptedException {
		List<String> ContentBeforeSorting = cslp_Pageobj.Get_Entire_Content_Of_Issue();
		softAssertion.assertTrue(cslp_Pageobj.ChecksortingOnSecondClick(cslp_Pageobj.Issue_Description_header,
				cslp_Pageobj.ListOfIssue_Descriptions));
		List<String> ContentAfterSorting = cslp_Pageobj.Get_Entire_Content_Of_Issue();
		softAssertion.assertTrue(ContentBeforeSorting.equals(ContentAfterSorting));
		softAssertion.assertAll();
	}

	@Test(priority = 5)
	public void Check_Sorting_for_issue_Number_on_Second_Click() throws InterruptedException {
		Assert.assertTrue(cslp_Pageobj.ChecksortingOnSecondClick(cslp_Pageobj.Submission_number_header,
				cslp_Pageobj.ListOfIssue_Numbers));
	}

	@Test(priority = 6)
	public void Check_Sorting_for_Customer_email_on_first_click() throws InterruptedException {
		Assert.assertTrue(cslp_Pageobj.ChecksortingOnFirstClick(cslp_Pageobj.Customer_email_header,
				cslp_Pageobj.ListOfCustomer_email));
	}

	/*@Test(priority = 7)
	public void Check_Sorting_for_Customer_email_on_Second_click() throws InterruptedException {
		Assert.assertTrue(cslp_Pageobj.ChecksortingOnSecondClick(cslp_Pageobj.Customer_email_header,
				cslp_Pageobj.ListOfCustomer_email));
	}*/

	@Test(priority = 8)
	public void Verify_display_of_issue_count() throws InterruptedException {
		Assert.assertTrue(cslp_Pageobj.Get_count_of_issues() == cslp_Pageobj.ListOfIssue_Numbers.size());
	}

	@Test(priority = 9)
	public void Verify_page_is_retained_on_page_refresh_Issue_Description_Sort() throws InterruptedException {
		cslp_Pageobj.Issue_Description_header.click();
		String Page_url_after_sorting = driver.getCurrentUrl();
		driver.navigate().refresh();
		String Page_url_after_Page_refresh = driver.getCurrentUrl();
		Assert.assertTrue(Page_url_after_sorting.equals(Page_url_after_Page_refresh));
	}

	@Test(priority = 10)
	public void Verify_newest_submission_appears_at_theTop_of_the_page() throws InterruptedException {
		int First_issue_number = Integer
				.parseInt(cslp_Pageobj.ListOfIssue_Numbers.get(0).getText().replaceAll("[^0-9]", ""));
		switchWindowObj.SwitchBytitle("Comcate Admin");
		AgenciesLandingPageObj.NavigateToChromlessApp();
		ChromelessAppObj.IssueDescription.sendKeys("Lorem Ipsum is simply dummyyyy test");
		ChromelessAppObj.UploadMultiplePhotosbyCitizen();
		ChromelessAppObj.ClickOnNExtButton_Description_whenPhotosUploaded();
		ChromelessAppObj.SelectCategory("");
		ChromelessAppObj.SelectMap("No");
		ChromelessAppObj.EnterCitizenInfo("No", "", "", "");
		switchWindowObj.SwitchBytitle("Customer Submissions Page");
		driver.navigate().refresh();
		int New_First_issue_number = Integer
				.parseInt(cslp_Pageobj.ListOfIssue_Numbers.get(0).getText().replaceAll("[^0-9]", ""));
		Assert.assertTrue(New_First_issue_number == First_issue_number + 1);

	}

	@Test(priority = 11)
	public void Verify_more_link_customerEmail_customerNameWhenUnknown() throws InterruptedException {
		switchWindowObj.SwitchBytitle("Comcate Admin");
		AgenciesLandingPageObj.NavigateToChromlessApp();
		// Enter issue description of more than 30 characters
		ChromelessAppObj.IssueDescription.sendKeys("Lorem Ipsum is simply dummyyyy test");
		ChromelessAppObj.UploadMultiplePhotosbyCitizen();
		ChromelessAppObj.ClickOnNExtButton_Description_whenPhotosUploaded();
		ChromelessAppObj.SelectCategory("");
		ChromelessAppObj.SelectMap("No");
		ChromelessAppObj.EnterCitizenInfo("No", "", "", "");
		switchWindowObj.SwitchBytitle("Customer Submissions Page");
		driver.navigate().refresh();
		System.out.println(cslp_Pageobj.ListOfIssue_Descriptions.get(0).getText());
		softAssertion.assertTrue(
				cslp_Pageobj.ListOfIssue_Descriptions.get(0).getText().equals("Lorem Ipsum is simply dummyyyy...more"));
		softAssertion.assertTrue(cslp_Pageobj.Get_content_of_latest_issue().get(8).getText().equals("-"));
		softAssertion.assertTrue(cslp_Pageobj.Get_content_of_latest_issue().get(7).getText().equals("Unknown"));
		softAssertion.assertAll();
	}

	@Test(priority = 12)
	public void Verify_customerName_when_Name_Is_Entered() throws InterruptedException {
		switchWindowObj.SwitchBytitle("Comcate Admin");
		AgenciesLandingPageObj.NavigateToChromlessApp();
		// Enter issue description of more than 30 characters
		ChromelessAppObj.IssueDescription.sendKeys("Lorem Ipsum is simply dummyyyy test");
		ChromelessAppObj.UploadMultiplePhotosbyCitizen();
		ChromelessAppObj.ClickOnNExtButton_Description_whenPhotosUploaded();
		ChromelessAppObj.SelectCategory("");
		ChromelessAppObj.SelectMap("No");
		ChromelessAppObj.EnterCitizenInfo("No", "Praful", "Kolhe", "");
		switchWindowObj.SwitchBytitle("Customer Submissions Page");
		driver.navigate().refresh();
		System.out.println(cslp_Pageobj.Get_content_of_latest_issue().get(7).getText());
		Assert.assertTrue(cslp_Pageobj.Get_content_of_latest_issue().get(7).getText().equalsIgnoreCase("Praful Kolhe"));
	}

	@Test(priority = 13)
	public void Verify_customerName_when_Anonymous_checkBox_selected() throws InterruptedException {
		switchWindowObj.SwitchBytitle("Comcate Admin");
		AgenciesLandingPageObj.NavigateToChromlessApp();
		// Enter issue description of more than 30 characters
		ChromelessAppObj.IssueDescription.sendKeys("Lorem Ipsum is simply dummyyyy test");
		ChromelessAppObj.UploadMultiplePhotosbyCitizen();
		ChromelessAppObj.ClickOnNExtButton_Description_whenPhotosUploaded();
		ChromelessAppObj.SelectCategory("");
		ChromelessAppObj.SelectMap("No");
		ChromelessAppObj.EnterCitizenInfo("Yes", "Praful", "Kolhe", "");
		switchWindowObj.SwitchBytitle("Customer Submissions Page");
		driver.navigate().refresh();
		Assert.assertTrue(cslp_Pageobj.Get_content_of_latest_issue().get(7).getText().equalsIgnoreCase("Anonymous"));
	}

	@Test(priority = 14)
	public void Verify_Sorting_for_asignee_column_on_FirstClick() throws InterruptedException {
		Assert.assertTrue(
				cslp_Pageobj.ChecksortingOnFirstClick(cslp_Pageobj.Asignee_header, cslp_Pageobj.ListOf_Asignees));
	}

	@Test(priority = 15)
	public void Verify_Sorting_for_asignee_column_on_SecondClick() throws InterruptedException {
		Assert.assertTrue(
				cslp_Pageobj.ChecksortingOnSecondClick(cslp_Pageobj.Asignee_header, cslp_Pageobj.ListOf_Asignees));
	}

	@Test(priority = 16)
	public void Verify_page_is_retained_on_page_refresh_Asignee_sort() throws InterruptedException {
		cslp_Pageobj.Asignee_header.click();
		String Page_url_after_sorting = driver.getCurrentUrl();
		driver.navigate().refresh();
		String Page_url_after_Page_refresh = driver.getCurrentUrl();
		Assert.assertTrue(Page_url_after_sorting.equals(Page_url_after_Page_refresh));
	}

	@Test(priority = 17)
	public void CRM_062_Verify_search_complete_Match_with_Issue_description() throws InterruptedException {
		cslp_Pageobj.Enter_keyword_to_search("issue");
		for (WebElement webElement : cslp_Pageobj.ListOfIssue_Descriptions) {
			softAssertion.assertTrue(webElement.getText().contains("issue"));
		}
		softAssertion.assertAll();
	}

	@Test(priority = 18)
	public void CRM_062_Verify_search_complete_Match_with_Issue_Number() throws InterruptedException {
		cslp_Pageobj.Enter_keyword_to_search("21-14");
		softAssertion.assertTrue(cslp_Pageobj.ListOfIssue_Numbers.size() == 1);
		softAssertion.assertTrue(cslp_Pageobj.ListOfIssue_Numbers.get(0).getText().equals("21-14"));
		softAssertion.assertAll();
	}

	@Test(priority = 19)
	public void CRM_064_Verify_search_functionality_when_no_result_is_fetched() throws InterruptedException {
		cslp_Pageobj.Enter_keyword_to_search("no result");
		softAssertion.assertTrue(cslp_Pageobj.Table_header.getText()
				.equals("0 Customer Submissions | Try adjusting your search or Clear all filters"));
		softAssertion.assertTrue(cslp_Pageobj.No_data.getText().equals("No customer submissions to display"));
		softAssertion.assertAll();
	}

	@Test(priority = 19)
	public void CRM_065_Verify_functionality_for_cross_icon() throws InterruptedException {
		int Total_number_of_issues = Integer.parseInt(cslp_Pageobj.Table_header.getText().replaceAll("[^0-9]", ""));
		cslp_Pageobj.Enter_keyword_to_search("issue");
		int NumberOfIssues_afterSearch = Integer.parseInt(cslp_Pageobj.Table_header.getText().replaceAll("[^0-9]", ""));
		cslp_Pageobj.ClickOn_cross_icon();

		int NumberofIssues_after_clicking_crossIcon = Integer
				.parseInt(cslp_Pageobj.Table_header.getText().replaceAll("[^0-9]", ""));

		softAssertion.assertTrue(NumberOfIssues_afterSearch <= Total_number_of_issues);
		softAssertion.assertTrue(Total_number_of_issues == NumberofIssues_after_clicking_crossIcon);
		softAssertion.assertAll();

	}

	@Test(priority = 20)
	public void CRM_066_Verify_placeholdertext_for_searchButton() throws InterruptedException {
		Assert.assertTrue(cslp_Pageobj.Search_box.getAttribute("placeholder").equals("Filter submissions by keyword"));

	}

	@Test(priority = 21)
	public void CRM_071_Verify_Sorting_for_Created_column_on_FirstClick() throws InterruptedException {
		Assert.assertTrue(
				cslp_Pageobj.ChecksortingOnFirstClick(cslp_Pageobj.CreatedAt_header, cslp_Pageobj.ListOf_Created_date));
	}

	@Test(priority = 22)
	public void CRM_073_Verify_Sorting_for_Created_column_on_SecondClick() throws InterruptedException {
		Assert.assertTrue(cslp_Pageobj.ChecksortingOnSecondClick(cslp_Pageobj.CreatedAt_header,
				cslp_Pageobj.ListOf_Created_date));
	}

	@Test(priority = 23)
	public void CRM_075_Verify_DateFormat_is_ddmmyyyy() throws InterruptedException {
		Assert.assertTrue(cslp_Pageobj.isValidDate(cslp_Pageobj.ListOf_Created_date.get(0).getText()));
	}

	@Test(priority = 24)
	public void CRM_129_Verify_Sorting_for_Categories_column_on_FirstClick() throws InterruptedException {
		Assert.assertTrue(cslp_Pageobj.ChecksortingOnFirstClick(cslp_Pageobj.Category_header,
				cslp_Pageobj.ListOf_Categories_in_issue_table));
	}

	@Test(priority = 25)
	public void CRM_130_Verify_Sorting_for_Categories_column_on_SecondClick() throws InterruptedException {
		Assert.assertTrue(cslp_Pageobj.ChecksortingOnSecondClick(cslp_Pageobj.Category_header,
				cslp_Pageobj.ListOf_Categories_in_issue_table));
	}

	@Test(priority = 26)
	public void CRM_022_CRM_150_Verify_ClearAllFilter_when_no_filter_applied_Verify_cust_submission_numbering()
			throws InterruptedException {
		String Submission_number = AgenciesLandingPageObj.GetCustomer_submission_numbering();
		softAssertion.assertTrue(cslp_Pageobj.ListOfIssue_Numbers.get(cslp_Pageobj.ListOfIssue_Numbers.size() - 1)
				.getText().equals(Submission_number));
		softAssertion.assertTrue(cslp_Pageobj.Get_clear_all_filter_link().size() == 0);
		softAssertion.assertAll();
	}

	@Test(priority = 27)
	public void CRM_151_Verify_existance_of_clearAllFilter_when_filter_applied() throws InterruptedException {
		cslp_Pageobj.Enter_keyword_to_search("issue");
		softAssertion.assertTrue(cslp_Pageobj.Get_clear_all_filter_link().size() == 1);
	}

	@Test(priority = 28)
	public void CRM_152_Verify_existance_of_clearAllFilter_when_filter_applied() throws InterruptedException {
		int Total_number_of_issues = Integer.parseInt(cslp_Pageobj.Table_header.getText().replaceAll("[^0-9]", ""));
		cslp_Pageobj.Enter_keyword_to_search("issue");
		softAssertion.assertTrue(cslp_Pageobj.Get_clear_all_filter_link().size() == 1);

		int NumberOfIssues_afterSearch = Integer.parseInt(cslp_Pageobj.Table_header.getText().replaceAll("[^0-9]", ""));
		softAssertion.assertTrue(NumberOfIssues_afterSearch <= Total_number_of_issues);

		cslp_Pageobj.Click_on_Clear_all_filter();
		int Total_number_of_issues_after_clearAllFilter = Integer
				.parseInt(cslp_Pageobj.Table_header.getText().replaceAll("[^0-9]", ""));
		softAssertion.assertTrue(Total_number_of_issues_after_clearAllFilter == Total_number_of_issues);
		softAssertion.assertAll();
	}

	@Test(priority = 29)
	public void CRM_153_Verify_clearAllFilter_when_filter_withNoRecords_appear() throws InterruptedException {
		int Total_number_of_issues = Integer.parseInt(cslp_Pageobj.Table_header.getText().replaceAll("[^0-9]", ""));
		cslp_Pageobj.Enter_keyword_to_search("no result");
		softAssertion.assertTrue(cslp_Pageobj.Get_clear_all_filter_link().size() == 1);

		int NumberOfIssues_afterSearch = Integer.parseInt(cslp_Pageobj.Table_header.getText().replaceAll("[^0-9]", ""));
		softAssertion.assertTrue(NumberOfIssues_afterSearch == 0);

		cslp_Pageobj.Click_on_Clear_all_filter();
		int Total_number_of_issues_after_clearAllFilter = Integer
				.parseInt(cslp_Pageobj.Table_header.getText().replaceAll("[^0-9]", ""));
		softAssertion.assertTrue(Total_number_of_issues_after_clearAllFilter == Total_number_of_issues);
		softAssertion.assertAll();
	}

	@Test(priority = 30)
	public void CRM_161_CRM_162_Verify_functionality_for_category_filter() throws InterruptedException {

		cslp_Pageobj.select_category_from_category_filter("Inactive_Category");
		for (WebElement ele : cslp_Pageobj.ListOf_Categories_in_issue_table) {
			softAssertion.assertTrue(ele.getText().contains("Inactive_Category"));
		}
		// click somewhere on screen
		cslp_Pageobj.Search_box.click();
		System.out.println(cslp_Pageobj.Category_filter_closed_state.isDisplayed());
		softAssertion.assertTrue(cslp_Pageobj.Category_filter_closed_state.isDisplayed());
		softAssertion.assertAll();
	}

	@Test(priority = 31)
	public void CRM_163_Verify_functionality_when_category_with_noissues_isSelcted() throws InterruptedException {

		cslp_Pageobj.select_category_from_category_filter("10Keywords");
		Assert.assertTrue(cslp_Pageobj.No_data.getText().equals("No customer submissions to display"));
	}

	@Test(priority = 32)
	public void CRM_154_Verify_existance_of_closed_column() throws InterruptedException {

		Assert.assertTrue(cslp_Pageobj.Closed_header.isDisplayed());
	}

	@Test(priority = 33)
	public void CRM_013_Verify_User_permissions_for_Customer_Submission_withBasicUSer() throws InterruptedException {

		AgencySetupPageObj.Login_with_user("Praful.comcate+Basic".concat(AgencyID).concat("@gmail.com"), "Test@123");
		AgenciesLandingPageObj.NavigateToCSLP();
		Assert.assertEquals(cslp_Pageobj.PageHeader.getText(), "Customer Submissions");
	}

	@Test(priority = 34)
	public void CRM_013_Verify_User_permissions_for_Customer_Submission_overWriteUSer() throws InterruptedException {

		AgencySetupPageObj.Login_with_user("Praful.comcate+Overwrite".concat(AgencyID).concat("@gmail.com"), "Test@123");
		AgenciesLandingPageObj.NavigateToCSLP();
		Assert.assertEquals(cslp_Pageobj.PageHeader.getText(), "Customer Submissions");
	}

	@Test(priority = 35)
	public void CRM_013_Verify_User_permissions_for_Customer_Submission_ReadOnlyUSer() throws InterruptedException {

		AgencySetupPageObj.Login_with_user("Praful.comcate+Readonly".concat(AgencyID).concat("@gmail.com"), "Test@123");
		AgenciesLandingPageObj.NavigateToCSLP();
		Assert.assertEquals(cslp_Pageobj.PageHeader.getText(), "Customer Submissions");
	}

	@Test(priority = 36)
	public void CRM_067_Verify_search_field_appearsFor_ReadOnlyUSer() throws InterruptedException {

		AgencySetupPageObj.Login_with_user("Praful.comcate+Readonly".concat(AgencyID).concat("@gmail.com"), "Test@123");
		AgenciesLandingPageObj.NavigateToCSLP();
		Assert.assertTrue(cslp_Pageobj.Search_box.isDisplayed());
	}

	@Test(priority = 36)
	public void CRM_080_Verify_CreatedAt_filter_forAnytime() throws InterruptedException {
		int Total_number_of_issues_beforeFilter = Integer
				.parseInt(cslp_Pageobj.Table_header.getText().replaceAll("[^0-9]", ""));
		cslp_Pageobj.CreatedAt_dropDown.click();
		cslp_Pageobj.ClickOn_CreatedAt_ApplyFilter();
		int Total_number_of_issues_afterFilter = Integer
				.parseInt(cslp_Pageobj.Table_header.getText().replaceAll("[^0-9]", ""));
		Assert.assertTrue(Total_number_of_issues_beforeFilter == Total_number_of_issues_afterFilter);
	}

}
