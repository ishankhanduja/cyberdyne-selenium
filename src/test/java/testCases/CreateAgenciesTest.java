package testCases;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import org.testng.AssertJUnit;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.qa.comcate.base.TestBase;
import com.qa.comcate.pages.AgenciesLandingPage;
import com.qa.comcate.pages.AgencySetupPage;
import com.qa.comcate.pages.DashboardPage;
import com.qa.comcate.pages.LoginPage;
import com.qa.comcate.utils.CommonUtils;

public class CreateAgenciesTest extends TestBase {

	LoginPage LoginPageObj;
	AgenciesLandingPage AgenciesLandingPageObj;
	DashboardPage DashboardPageObj;
	AgencySetupPage AgencySetupPageObj;
	
	CommonUtils utils;

	public CreateAgenciesTest() {
		super();
	}

	@BeforeMethod
	public void setup() {
		initiate();
		LoginPageObj = new LoginPage();
		AgenciesLandingPageObj = new AgenciesLandingPage();
		DashboardPageObj = new DashboardPage();
		AgencySetupPageObj = new AgencySetupPage();
		utils = new CommonUtils();
		LoginPageObj.LoginToApplicationWithValid("support@comcate.com", "test@123");
	}

	@AfterMethod
	public void tearDown() {
		driver.quit();
	}

	//@Test
	public void createNormalAgency() throws InterruptedException {
		AgenciesLandingPageObj.CreateAgencyButton.click();
		AgenciesLandingPageObj.CreateAgencyInfo("Auto_Normal_", "praful.kolhe+automation@comcate.com");
		Thread.sleep(2000);
		AgenciesLandingPageObj.CreateAgencyButtononPopUp.click();
		Thread.sleep(2000);
		WebElement SearchedAgency = AgenciesLandingPageObj.SearchAgency(AgenciesLandingPage.Modified_AgencyName);
		Thread.sleep(3000);
		AssertJUnit.assertEquals(SearchedAgency.getText(), AgenciesLandingPage.Modified_AgencyName);
		System.out.println("Agency created successfully");
	}
	
	//@Test
	public void createAgencyWithGisDirect() throws InterruptedException {
		AgenciesLandingPageObj.CreateAgencyButton.click();
		AgenciesLandingPageObj.CreateAgencyInfo("Auto_Normal_", "praful.kolhe+automation@comcate.com");
		AgenciesLandingPageObj.agency_settings_gis("Direct", "on", "Pune");
		Thread.sleep(2000);
		AgenciesLandingPageObj.CreateAgencyButtononPopUp.click();
		Thread.sleep(2000);
		WebElement SearchedAgency = AgenciesLandingPageObj.SearchAgency(AgenciesLandingPage.Modified_AgencyName);
		Thread.sleep(3000);
		AssertJUnit.assertEquals(SearchedAgency.getText(), AgenciesLandingPage.Modified_AgencyName);
		System.out.println("AgencyWithGisOn created successfully");
	}
	
	//@Test
	public void createAgencyWithGisLite() throws InterruptedException {
		AgenciesLandingPageObj.CreateAgencyButton.click();
		AgenciesLandingPageObj.CreateAgencyInfo("Auto_Normal_", "praful.kolhe+automation@comcate.com");
		AgenciesLandingPageObj.agency_settings_gis("Lite", "on", "Pune");
		Thread.sleep(2000);
		AgenciesLandingPageObj.CreateAgencyButtononPopUp.click();
		Thread.sleep(2000);
		WebElement SearchedAgency = AgenciesLandingPageObj.SearchAgency(AgenciesLandingPage.Modified_AgencyName);
		Thread.sleep(3000);
		AssertJUnit.assertEquals(SearchedAgency.getText(), AgenciesLandingPage.Modified_AgencyName);
		System.out.println("AgencyWithGisOff created successfully");
	}
	
	//@Test
	public void createAgencyWithGisLiteAndNoDefaultCity(){
		AgenciesLandingPageObj.CreateAgencyButton.click();
		AgenciesLandingPageObj.CreateAgencyInfo("Auto_Normal_", "praful.kolhe+automation@comcate.com");
		AgenciesLandingPageObj.agency_settings_gis("Lite", "Off", null);
		AgenciesLandingPageObj.product_configuration_CE("CE", "Active", "off", "off", "Off", "on", "on", "on", "on");
		AgenciesLandingPageObj.ClickOnCreateAgencyButtononPopUp();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WebElement SearchedAgency = AgenciesLandingPageObj.SearchAgency(AgenciesLandingPage.Modified_AgencyName);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		AssertJUnit.assertEquals(SearchedAgency.getText(), AgenciesLandingPage.Modified_AgencyName);
		System.out.println("AgencyWithGisOffAndNoDefaultCity successfully");
	}
	
	@Test(priority=1)
	public void createAgencyWithGisOffAllViolation() throws InterruptedException, IOException 
	{
		AgenciesLandingPageObj.CreateAgencyButton.click();
		AgenciesLandingPageObj.CreateAgencyInfo("ONLY CE_Page layout", "Praful.comcate@gmail.com");
		AgenciesLandingPageObj.agency_settings_gis("On", "On", "Pune");
		//AgencySetupPageObj.ClickOnMultiChoiceButton("Group Permissions", "OFF");
		AgencySetupPageObj.ClickOnMultiChoiceButton("Authentication", "Okta");
		AgenciesLandingPageObj.template_managementSettings("support-ci-3675", "");
		AgenciesLandingPageObj.product_configuration_CE("CE", "Active", "off", "off", "off", "on", "on", "off", "off");
		//AgenciesLandingPageObj.configureCRM("CRM", "On");
		Thread.sleep(3000);
		AgenciesLandingPageObj.ClickOnCreateAgencyButtononPopUp();
		Thread.sleep(5000);
		AgenciesLandingPageObj.SaveAgencyNAme();
		WebElement SearchedAgency = AgenciesLandingPageObj.SearchAgency(utils.ReadFromFile());
		Thread.sleep(2000);
		AgenciesLandingPageObj.SaveAgencyID();
		Assert.assertEquals(SearchedAgency.getText(), utils.ReadFromFile());
	}
	
	//Consider CRM page as a new page in the same application.
	//Also the search method should not be here .Search on a page class.Edit agency.
	//@Test(priority=2)
	public void CreateUser() throws InterruptedException, IOException 
	{
		System.out.println(utils.ReadFromFile());
		WebElement SearchedAgency = AgenciesLandingPageObj.SearchAgency(utils.ReadFromFile());
		Thread.sleep(2000);
		AssertJUnit.assertEquals(SearchedAgency.getText(), utils.ReadFromFile());
		AgenciesLandingPageObj.LoginAsSuperAdmin();
		DashboardPageObj.ClickonGearIcon();
		AgencySetupPageObj.ClickonUsers();
		AgencySetupPageObj.ClickonCreateUser();
		AgencySetupPageObj.CreateUserPersonalinfo("Praful","Kolhe","","Praful.kolhe+1@velotio.com","8657079725");
		AgencySetupPageObj.UserSiteSetting("Yes", "Active");
		AgenciesLandingPageObj.Violation_activation_setting("CE", "On");
		AgencySetupPageObj.ClickOnMultiChoiceButton("Product Admin?", "Yes");
		AgencySetupPageObj.ClickOnMultiChoiceButton("Reports", "All Staff");
		AgencySetupPageObj.ClickOnMultiChoiceButton("Executive Reports", "Yes");
		AgenciesLandingPageObj.Violation_activation_setting("CRM", "On");
		AgencySetupPageObj.ClickOnMultiChoiceButton("Permissions", "Basic");
		AgencySetupPageObj.ClickOnCreateUserPopUpButton();
		AgencySetupPageObj.ClickonLocationsAndMaps();
		AgencySetupPageObj.ClickonMapSettings();
		AgencySetupPageObj.UploadParcel();
		AgencySetupPageObj.EnterLatLong("-94.8992156982421900000000000", "29.3833717078824430000000000");
		AgencySetupPageObj.DummyPrint();
	}	
	/*AgenciesLandingPageObj.Edit_agency.click();
	System.out.println(AgenciesLandingPageObj.CRM_URL.getText());
	AgenciesLandingPageObj.CRM_URL.click();
	AgenciesLandingPageObj.Next_button.click();		
	System.out.println("AgencyWithGisOffAndNoDefaultCity successfully")*/
	
	
	
}
