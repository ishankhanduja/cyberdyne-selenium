package testCases;

import java.io.IOException;
import java.security.GeneralSecurityException;

import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.AssertJUnit;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.qa.Analyzer.Retriable;
import com.qa.comcate.GmailAPiLib.GMail;
import com.qa.comcate.base.TestBase;
import com.qa.comcate.pages.AgenciesLandingPage;
import com.qa.comcate.pages.AgencySetupPage;
import com.qa.comcate.pages.DashboardPage;
import com.qa.comcate.pages.LoginPage;
import com.qa.comcate.utils.CommonUtils;

public class CreateUserTest extends TestBase {

	LoginPage LoginPageObj;
	AgenciesLandingPage AgenciesLandingPageObj;
	DashboardPage DashboardPageObj;
	AgencySetupPage AgencySetupPageObj;
	GMail gmailobj;
	String AgencyID;
	
	CommonUtils utils;

	public CreateUserTest() {
		super();
	}

	@BeforeMethod
	//@Retriable(attempts = 4)
	public void setup() throws Exception {
		initiate();
		LoginPageObj = new LoginPage();
		AgenciesLandingPageObj = new AgenciesLandingPage();
		DashboardPageObj = new DashboardPage();
		AgencySetupPageObj = new AgencySetupPage();
		utils = new CommonUtils();
		gmailobj = new GMail();
		LoginPageObj.LoginToApplicationWithValid("support@comcate.com", "test@123");
		System.out.println(utils.ReadFromFile());
		AgencyID = utils.ReadAgencyIDFromFile();
		WebElement SearchedAgency = AgenciesLandingPageObj.SearchAgency(utils.ReadFromFile());
		Thread.sleep(2000);
		Assert.assertEquals(SearchedAgency.getText(), utils.ReadFromFile());
		AgenciesLandingPageObj.LoginAsSuperAdmin();
	}

	@AfterMethod
	public void tearDown() {
		driver.quit();
	}

	//@Test()
	public void CreateUser_CRMOverwrite() throws Exception  
	{
		int oldMailcount =  GMail.getTotalCountOfMails();
		DashboardPageObj.ClickonGearIcon();
		AgencySetupPageObj.ClickonUsers();
		AgencySetupPageObj.ClickonCreateUser();
		AgencySetupPageObj.CreateUserPersonalinfo
		("CRM Overwrite","Kolhe","","Praful.comcate+Overwrite".concat(AgencyID).concat("@gmail.com"),"8657079725");
		AgencySetupPageObj.UserSiteSetting("Yes", "Active");
		AgenciesLandingPageObj.Violation_activation_setting("CE", "On");
		AgencySetupPageObj.ClickOnMultiChoiceButton("Product Admin?", "Yes");
		AgencySetupPageObj.ClickOnMultiChoiceButton("Reports", "All Staff");
		AgencySetupPageObj.ClickOnMultiChoiceButton("Executive Reports", "Yes");
		AgencySetupPageObj.ClickOnMultiChoiceButton("Animal", "Overwrite");
		AgencySetupPageObj.ClickOnMultiChoiceButton("General", "Overwrite");
		AgenciesLandingPageObj.Violation_activation_setting("CRM", "On");
		AgencySetupPageObj.ClickOnMultiChoiceButton("Permissions", "Overwrite");
		AgencySetupPageObj.ClickOnCreateUserPopUpButton();
		/*int count = 0;
		int newemailcount = GMail.getTotalCountOfMails();
		while ((newemailcount != oldMailcount + 1) && count<=20) {
			Thread.sleep(500);
			newemailcount = GMail.getTotalCountOfMails();
			count++;
			if (count==20) {
				System.out.println("Timed out. Email not received yet");
				break;
			}
		}
		String pwdSuccessmsg = "Password Not set" ;
		if (newemailcount == oldMailcount + 1 ) {
			System.out.println("New count"+newemailcount);
			pwdSuccessmsg = AgencySetupPageObj.SetPassword();
		}
		Assert.assertTrue(pwdSuccessmsg.equals("Password set successfully"));*/
	}
	
	//@Test()
	public void CreateUser_CRMBasic() throws Exception 
	{
		int oldMailcount =  GMail.getTotalCountOfMails();
		DashboardPageObj.ClickonGearIcon();
		AgencySetupPageObj.ClickonUsers();
		AgencySetupPageObj.ClickonCreateUser();
		AgencySetupPageObj.CreateUserPersonalinfo("CRM Basic","Kolhe","","Praful.comcate+Basic".concat(AgencyID).concat("@gmail.com"),"8657079725");
		AgencySetupPageObj.UserSiteSetting("Yes", "Active");
		AgenciesLandingPageObj.Violation_activation_setting("CE", "On");
		AgencySetupPageObj.ClickOnMultiChoiceButton("Product Admin?", "Yes");
		AgencySetupPageObj.ClickOnMultiChoiceButton("Reports", "All Staff");
		AgencySetupPageObj.ClickOnMultiChoiceButton("Executive Reports", "Yes");
		AgencySetupPageObj.ClickOnMultiChoiceButton("Animal", "Basic");
		AgencySetupPageObj.ClickOnMultiChoiceButton("General", "Basic");
		AgenciesLandingPageObj.Violation_activation_setting("CRM", "On");
		AgencySetupPageObj.ClickOnMultiChoiceButton("Permissions", "Basic");
		AgencySetupPageObj.ClickOnCreateUserPopUpButton();
		/*int count = 0;
		int newemailcount = GMail.getTotalCountOfMails();
		while ((newemailcount != oldMailcount + 1) && count<=20) {
			Thread.sleep(500);
			newemailcount = GMail.getTotalCountOfMails();
			count++;
			if (count==20) {
				System.out.println("Timed out. Email not received yet");
				break;
			}
		}
		String pwdSuccessmsg = "Password Not set" ;
		if (newemailcount == oldMailcount + 1 ) {
			System.out.println("New count"+newemailcount);
			pwdSuccessmsg = AgencySetupPageObj.SetPassword();
		}
		Assert.assertTrue(pwdSuccessmsg.equals("Password set successfully"));*/
		// Add assertions here
	}
	
	//@Test()
	public void CreateUser_CRMReadOnly() throws  Exception 
	{
		int oldMailcount =  GMail.getTotalCountOfMails();
		DashboardPageObj.ClickonGearIcon();
		AgencySetupPageObj.ClickonUsers();
		AgencySetupPageObj.ClickonCreateUser();
		AgencySetupPageObj.CreateUserPersonalinfo("CRM Readonly","Kolhe","","Praful.comcate+Readonly".concat(AgencyID).concat("@gmail.com"),"8657079725");
		AgencySetupPageObj.UserSiteSetting("Yes", "Active");
		AgenciesLandingPageObj.Violation_activation_setting("CE", "On");
		AgencySetupPageObj.ClickOnMultiChoiceButton("Product Admin?", "Yes");
		AgencySetupPageObj.ClickOnMultiChoiceButton("Reports", "All Staff");
		AgencySetupPageObj.ClickOnMultiChoiceButton("Executive Reports", "Yes");
		AgenciesLandingPageObj.Violation_activation_setting("CRM", "On");
		AgencySetupPageObj.ClickOnMultiChoiceButton("Permissions", "Read Only");
		AgencySetupPageObj.ClickOnCreateUserPopUpButton();
		/*int count = 0;
		int newemailcount = GMail.getTotalCountOfMails();
		while ((newemailcount != oldMailcount + 1) && count<=20) {
			Thread.sleep(500);
			newemailcount = GMail.getTotalCountOfMails();
			count++;
			if (count==20) {
				System.out.println("Timed out. Email not received yet");
				break;
			}
		}
		String pwdSuccessmsg = "Password Not set" ;
		if (newemailcount == oldMailcount + 1 ) {
			System.out.println("New count"+newemailcount);
			pwdSuccessmsg = AgencySetupPageObj.SetPassword();
		}
		Assert.assertTrue(pwdSuccessmsg.equals("Password set successfully"));*/
	}
	
	
	
	//@Test()
	public void Testemail() throws GeneralSecurityException,IOException,InterruptedException
	{
		int oldMailcount =  GMail.getTotalCountOfMails();
		System.out.println(oldMailcount);
		DashboardPageObj.ClickonGearIcon();
		AgencySetupPageObj.ClickonUsers();
		AgencySetupPageObj.ClickonCreateUser();
		AgencySetupPageObj.CreateUserPersonalinfo("Test EMail","Kolhe","","Praful.comcate+TestEmail".concat(AgencyID).concat("@gmail.com"),"8657079725");
		AgencySetupPageObj.UserSiteSetting("Yes", "Active");
		AgenciesLandingPageObj.Violation_activation_setting("CE", "On");
		AgencySetupPageObj.ClickOnMultiChoiceButton("Product Admin?", "Yes");
		AgencySetupPageObj.ClickOnMultiChoiceButton("Reports", "All Staff");
		AgencySetupPageObj.ClickOnMultiChoiceButton("Executive Reports", "Yes");
		AgenciesLandingPageObj.Violation_activation_setting("CRM", "On");
		AgencySetupPageObj.ClickOnMultiChoiceButton("Permissions", "Read Only");
		AgencySetupPageObj.ClickOnCreateUserPopUpButton();
		/*int count = 0;
		int newemailcount = GMail.getTotalCountOfMails();
		while ((newemailcount != oldMailcount + 1) && count<=20) {
			Thread.sleep(500);
			newemailcount = GMail.getTotalCountOfMails();
			System.out.println(newemailcount);
			count++;
			if (count==20) {
				System.out.println("Timed out. Email not received yet");
				break;
			}
		}
		String pwdSuccessmsg = "Password Not set" ;
		if (newemailcount == oldMailcount + 1 ) {
			System.out.println("New count"+newemailcount);
			pwdSuccessmsg = AgencySetupPageObj.SetPassword();
		}
		Assert.assertTrue(pwdSuccessmsg.equals("Password set successfully"));*/
	}
	
	
	
	//@Test()
		public void CreateUser_CRMOverwriteWithgroups() throws Exception  
		{
			int oldMailcount =  GMail.getTotalCountOfMails();
			DashboardPageObj.ClickonGearIcon();
			AgencySetupPageObj.ClickonUsers();
			AgencySetupPageObj.ClickonCreateUser();
			AgencySetupPageObj.CreateUserPersonalinfo
			("CRM Overwrite","Kolhe","","Praful.comcate+Overwrite".concat(AgencyID).concat("@gmail.com"),"8657079725");
			AgencySetupPageObj.SelectGroups("Admin Group");
			AgencySetupPageObj.ClickOnCreateUserPopUpButton();
			/*int count = 0;
			int newemailcount = GMail.getTotalCountOfMails();
			while ((newemailcount != oldMailcount + 1) && count<=20) {
				Thread.sleep(500);
				newemailcount = GMail.getTotalCountOfMails();
				count++;
				if (count==20) {
					System.out.println("Timed out. Email not received yet");
					break;
				}
			}
			String pwdSuccessmsg = "Password Not set" ;
			if (newemailcount == oldMailcount + 1 ) {
				System.out.println("New count"+newemailcount);
				pwdSuccessmsg = AgencySetupPageObj.SetPassword();
			}
			Assert.assertTrue(pwdSuccessmsg.equals("Password set successfully"));*/
		}
		
		//@Test()
		public void CreateUser_CRMBasicWithgroups() throws Exception 
		{
			int oldMailcount =  GMail.getTotalCountOfMails();
			DashboardPageObj.ClickonGearIcon();
			AgencySetupPageObj.ClickonUsers();
			AgencySetupPageObj.ClickonCreateUser();
			AgencySetupPageObj.CreateUserPersonalinfo("CRM Basic","Kolhe","","Praful.comcate+Basic".concat(AgencyID).concat("@gmail.com"),"8657079725");
			AgencySetupPageObj.SelectGroups("Admin Group");
			AgencySetupPageObj.ClickOnCreateUserPopUpButton();
			/*int count = 0;
			int newemailcount = GMail.getTotalCountOfMails();
			while ((newemailcount != oldMailcount + 1) && count<=20) {
				Thread.sleep(500);
				newemailcount = GMail.getTotalCountOfMails();
				count++;
				if (count==20) {
					System.out.println("Timed out. Email not received yet");
					break;
				}
			}
			String pwdSuccessmsg = "Password Not set" ;
			if (newemailcount == oldMailcount + 1 ) {
				System.out.println("New count"+newemailcount);
				pwdSuccessmsg = AgencySetupPageObj.SetPassword();
			}
			Assert.assertTrue(pwdSuccessmsg.equals("Password set successfully"));*/
			// Add assertions here
		}
		
		//@Test()
		public void CreateUser_CRMReadOnlyWithgroups() throws  Exception 
		{
			int oldMailcount =  GMail.getTotalCountOfMails();
			DashboardPageObj.ClickonGearIcon();
			AgencySetupPageObj.ClickonUsers();
			AgencySetupPageObj.ClickonCreateUser();
			AgencySetupPageObj.CreateUserPersonalinfo("CRM Readonly","Kolhe","","Praful.comcate+Readonly".concat(AgencyID).concat("@gmail.com"),"8657079725");
			AgencySetupPageObj.SelectGroups("Read-only Group");
			AgencySetupPageObj.ClickOnCreateUserPopUpButton();
			/*int count = 0;
			int newemailcount = GMail.getTotalCountOfMails();
			while ((newemailcount != oldMailcount + 1) && count<=20) {
				Thread.sleep(500);
				newemailcount = GMail.getTotalCountOfMails();
				count++;
				if (count==20) {
					System.out.println("Timed out. Email not received yet");
					break;
				}
			}
			String pwdSuccessmsg = "Password Not set" ;
			if (newemailcount == oldMailcount + 1 ) {
				System.out.println("New count"+newemailcount);
				pwdSuccessmsg = AgencySetupPageObj.SetPassword();
			}
			Assert.assertTrue(pwdSuccessmsg.equals("Password set successfully"));*/
		}
		
		
		
		@Test()
		public void TestemailWithgroups() throws GeneralSecurityException,IOException,InterruptedException
		{
			int oldMailcount =  GMail.getTotalCountOfMails();
			System.out.println(oldMailcount);
			DashboardPageObj.ClickonGearIcon();
			AgencySetupPageObj.ClickonUsers();
			AgencySetupPageObj.ClickonCreateUser();
			AgencySetupPageObj.CreateUserPersonalinfo("Test EMail","Kolhe","","Praful.comcate+TestEmail".concat(AgencyID).concat("@gmail.com"),"8657079725");
			AgencySetupPageObj.SelectGroups("Admin Group");
			AgencySetupPageObj.ClickOnCreateUserPopUpButton();
			int count = 0;
			int newemailcount = GMail.getTotalCountOfMails();
			while ((newemailcount != oldMailcount + 1) && count<=20) {
				Thread.sleep(500);
				newemailcount = GMail.getTotalCountOfMails();
				System.out.println(newemailcount);
				count++;
				if (count==20) {
					System.out.println("Timed out. Email not received yet");
					break;
				}
			}
			String pwdSuccessmsg = "Password Not set" ;
			if (newemailcount == oldMailcount + 1 ) {
				System.out.println("New count"+newemailcount);
				pwdSuccessmsg = AgencySetupPageObj.SetPassword();
			}
			Assert.assertTrue(pwdSuccessmsg.equals("Password set successfully"));
		}
	
	
	
	
	
	
	//@Test
	public void setpass() throws InterruptedException,IOException,GeneralSecurityException
	{
		AgencySetupPageObj.SetPassword();
	}
	
	
}
