package testCases;

import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.qa.Analyzer.Retriable;
import com.qa.comcate.base.TestBase;
import com.qa.comcate.pages.AgenciesLandingPage;
import com.qa.comcate.pages.AgencySetupPage;
import com.qa.comcate.pages.ChromelessAppPage;
import com.qa.comcate.pages.DashboardPage;
import com.qa.comcate.pages.InternalSubmissionPage;
import com.qa.comcate.pages.LoginPage;
import com.qa.comcate.utils.CommonUtils;
import com.qa.comcate.utils.SwitchWindows;

public class InternalSubmissionTests extends TestBase {

	LoginPage LoginPageObj;
	AgenciesLandingPage AgenciesLandingPageObj;
	DashboardPage DashboardPageObj;
	AgencySetupPage AgencySetupPageObj;
	ChromelessAppPage ChromelessAppObj;
	InternalSubmissionPage InternalSubmissionPageObj;
	SoftAssert softAssertion;
	SwitchWindows switchWindows;

	CommonUtils utils;

	public InternalSubmissionTests() {
		super();
	}

	@BeforeMethod
	//@Retriable(attempts = 4)
	public void setup() throws Exception {
		initiate();
		softAssertion = new SoftAssert();
		switchWindows = new SwitchWindows();
		LoginPageObj = new LoginPage();
		AgenciesLandingPageObj = new AgenciesLandingPage();
		DashboardPageObj = new DashboardPage();
		AgencySetupPageObj = new AgencySetupPage();
		ChromelessAppObj = new ChromelessAppPage();
		InternalSubmissionPageObj = new InternalSubmissionPage();
		utils = new CommonUtils();
		LoginPageObj.LoginToApplicationWithValid("support@comcate.com", "test@123");
		System.out.println(utils.ReadFromFile());
		WebElement SearchedAgency = AgenciesLandingPageObj.SearchAgency(utils.ReadFromFile());
		Thread.sleep(2000);
		Assert.assertEquals(SearchedAgency.getText(), utils.ReadFromFile());
		AgenciesLandingPageObj.LoginAsSuperAdmin();
		DashboardPageObj.NavigateToInternalsubmission();
	}

	@AfterMethod
	public void tearDown() {
		driver.quit();
	}

	@Test(priority = 1)
	public void CheckOtherCategoryisDisplayed() throws InterruptedException {
		InternalSubmissionPageObj.SelectCategory("Other");
		Assert.assertTrue(InternalSubmissionPageObj.CategorySelected.getText().equals("Other"));
	}
	
	//@Test(priority = 2)
	public void internalsubmiission() throws InterruptedException {
		InternalSubmissionPageObj.SelectCategory("Other");
		Assert.assertTrue(true);
	}
	
	//@Test(priority = 2)
	public void VerifyFunctionalityOfCancelButton() throws InterruptedException {
		String CurrentUrl = driver.getCurrentUrl();
		InternalSubmissionPageObj.CancelButton.click();
		driver.getCurrentUrl();
	}

	//@Test(priority = 2)
	public void VerifyCategoryOrderBasedOnKeywordForPartialMatch() throws InterruptedException {
		ChromelessAppObj.EnterDescription("Testing Duplicate key");
		ChromelessAppObj.UploadMultiplePhotosbyCitizen();
		ChromelessAppObj.ClickOnNExtButton_Description_whenPhotosUploaded();
		ChromelessAppObj.ClickOnShowMore();
		softAssertion.assertTrue(ChromelessAppObj.CategoryTubes.get(0).getText().startsWith("Duplicate"));
		softAssertion.assertTrue(ChromelessAppObj.ActiveCategory.getText().startsWith("Duplicate"));
		// softAssertion.assertTrue(ChromelessAppObj.CategoryTubes.size() == 11);
		System.out.println("Total number of categories appearing" + ChromelessAppObj.CategoryTubes.size());
		softAssertion.assertAll();
	}

	//@Test(priority = 3)
	public void VerifyCategoryOrderBasedOnKeywordForFullMatch() throws InterruptedException {
		ChromelessAppObj.EnterDescription("KeywordKeywordKeywordKeywordKeywordKeywordKeywordK");
		ChromelessAppObj.UploadMultiplePhotosbyCitizen();
		ChromelessAppObj.ClickOnNExtButton_Description_whenPhotosUploaded();
		ChromelessAppObj.ClickOnShowMore();
		softAssertion.assertTrue(ChromelessAppObj.CategoryTubes.get(0).getText().startsWith("LenthOfKeyword"));
		softAssertion.assertTrue(ChromelessAppObj.ActiveCategory.getText().startsWith("LenthOfKeyword"));
		// softAssertion.assertTrue(ChromelessAppObj.CategoryTubes.size() == 11);
		System.out.println("Total number of categories appearing" + ChromelessAppObj.CategoryTubes.size());
		softAssertion.assertAll();

	}

	//@Test(priority = 4)
	public void VerifyCategorySelectedWhenNoKeyword() throws InterruptedException {
		ChromelessAppObj.EnterDescription("This description has no keys");
		ChromelessAppObj.UploadMultiplePhotosbyCitizen();
		ChromelessAppObj.ClickOnNExtButton_Description_whenPhotosUploaded();
		ChromelessAppObj.ClickOnShowMore();
		softAssertion.assertTrue(ChromelessAppObj.CategoryTubes.get(0).getText().startsWith("Inactive_Category"));
		softAssertion.assertTrue(ChromelessAppObj.ActiveCategory.getText().startsWith("Inactive_Category"));
		// softAssertion.assertTrue(ChromelessAppObj.CategoryTubes.size() == 11);
		System.out.println("Total number of categories appearing" + ChromelessAppObj.CategoryTubes.size());
		softAssertion.assertAll();

	}

	//@Test(priority = 5)
	public void Verify_ErrorMessage_When_IssueDescription_IsBlank() throws InterruptedException {
		ChromelessAppObj.EnterDescription("");
		ChromelessAppObj.ClickOnNExtButton_Description_WithoutPhotos();
		System.out.println(utils.GetErrorMessageForFieldName("Describe the issue"));
		Assert.assertTrue(
				utils.GetErrorMessageForFieldName("Describe the issue").equals("The Issue description is required."));

	}

	//@Test(priority = 6)
	public void CRM_158_CRM_159_Verify_Citizen_contactInfo_When_infoSent_and_categories_Display() throws InterruptedException {
		ChromelessAppObj.EnterDescription("This is first issue description");
		ChromelessAppObj.UploadMultiplePhotosbyCitizen();
		ChromelessAppObj.ClickOnNExtButton_Description_whenPhotosUploaded();
		softAssertion.assertTrue(ChromelessAppObj.CategoryTubes.size()>1);
		ChromelessAppObj.SelectCategory("Duplicate");
		ChromelessAppObj.SelectMap("No");
		ChromelessAppObj.EnterCitizenInfo("No", "Praful", "Kolhe", "Praful.kolhe+citizen@gmail.com");
		softAssertion.assertTrue(ChromelessAppObj.Category_name_on_submission_page.getText().contains("Duplicate"));		
		softAssertion.assertTrue(ChromelessAppObj.ContactInfo_Firstname.getText().equals("Praful"));
		softAssertion.assertTrue(ChromelessAppObj.ContactInfo_LastName.getText().equals("Kolhe"));
		softAssertion.assertTrue(ChromelessAppObj.ContactInfo_Email.getText().equals("Praful.kolhe+citizen@gmail.com"));
		softAssertion.assertAll();
	}

	//@Test(priority = 7)
	public void Verify_Citizen_contactInfo_When_NoDetails() throws InterruptedException {
		ChromelessAppObj.EnterDescription("This is first issue description");
		ChromelessAppObj.UploadMultiplePhotosbyCitizen();
		ChromelessAppObj.ClickOnNExtButton_Description_whenPhotosUploaded();
		ChromelessAppObj.SelectCategory("Active_Category");
		ChromelessAppObj.SelectMap("No");
		ChromelessAppObj.EnterCitizenInfo("No", "", "", "");
		softAssertion.assertTrue(ChromelessAppObj.ContactInfo_Firstname.getText().equals("-"));
		softAssertion.assertTrue(ChromelessAppObj.ContactInfo_LastName.getText().equals("-"));
		softAssertion.assertTrue(ChromelessAppObj.ContactInfo_Email.getText().equals("-"));
		softAssertion.assertAll();
	}

	//@Test(priority = 8)
	public void Verif_character_length_for_Firstname() throws InterruptedException {
		ChromelessAppObj.EnterDescription("This is first issue description");
		ChromelessAppObj.UploadMultiplePhotosbyCitizen();
		ChromelessAppObj.ClickOnNExtButton_Description_whenPhotosUploaded();
		ChromelessAppObj.SelectCategory("");
		ChromelessAppObj.SelectMap("No");
		ChromelessAppObj.EnterCitizenInfo("No", "abcdefghijklmnopqstuvwxyz", "abcdefghijklmnopqstuvwxyz", "");
		softAssertion.assertTrue(utils.GetErrorMessageForFieldName("First Name").equals("20 character limit."));
		softAssertion.assertTrue(utils.GetErrorMessageForFieldName("Last Name").equals("20 character limit."));
		softAssertion.assertAll();
	}

	//@Test(priority = 9)
	public void Verify_Citizen_invalid_email_format() throws InterruptedException {
		ChromelessAppObj.EnterDescription("This is first issue description");
		ChromelessAppObj.UploadMultiplePhotosbyCitizen();
		ChromelessAppObj.ClickOnNExtButton_Description_whenPhotosUploaded();
		ChromelessAppObj.SelectCategory("");
		ChromelessAppObj.SelectMap("No");
		ChromelessAppObj.EnterCitizenInfo("No", "", "", "testingEmail.com");
		Assert.assertTrue(utils.GetErrorMessageForFieldName("Email").equals("Email format is invalid."));

	}

	//@Test(priority = 10)
	public void Verify_Post_Anonymous_IsChecked() throws InterruptedException {
		ChromelessAppObj.EnterDescription("This is first issue description");
		ChromelessAppObj.UploadMultiplePhotosbyCitizen();
		ChromelessAppObj.ClickOnNExtButton_Description_whenPhotosUploaded();
		ChromelessAppObj.SelectCategory("");
		ChromelessAppObj.SelectMap("No");
		Assert.assertFalse(ChromelessAppObj.PostAnonymousCheckBox.isSelected());
	}

	//@Test(priority = 11)
	public void Verify_functionality_for_postAnonymous_checkbox() throws InterruptedException {
		ChromelessAppObj.EnterDescription("This is first issue description");
		ChromelessAppObj.UploadMultiplePhotosbyCitizen();
		ChromelessAppObj.ClickOnNExtButton_Description_whenPhotosUploaded();
		ChromelessAppObj.SelectCategory("");
		ChromelessAppObj.SelectMap("No");
		softAssertion.assertTrue(ChromelessAppObj.Citizen_FirstName.isDisplayed());
		softAssertion.assertTrue(ChromelessAppObj.Citizen_LastName.isDisplayed());
		ChromelessAppObj.PostAnonymousCheckBox.click();
		softAssertion.assertFalse(ChromelessAppObj.check_existance_of_field(ChromelessAppObj.Citizen_FirstName));
		softAssertion.assertFalse(ChromelessAppObj.check_existance_of_field(ChromelessAppObj.Citizen_LastName));
		softAssertion.assertAll();
	}

	//@Test(priority = 13)
	public void Verify_MapSection_appears_after_category() throws InterruptedException {
		ChromelessAppObj.EnterDescription("This is first issue description");
		ChromelessAppObj.UploadMultiplePhotosbyCitizen();
		ChromelessAppObj.ClickOnNExtButton_Description_whenPhotosUploaded();
		ChromelessAppObj.SelectCategory("");
		Assert.assertTrue(ChromelessAppObj.Map_component.isDisplayed());
	}

	//@Test(priority = 14)
	public void Verify_Location_when_Dropping_a_pin() throws InterruptedException {
		ChromelessAppObj.EnterDescription("This is first issue description");
		ChromelessAppObj.UploadMultiplePhotosbyCitizen();
		ChromelessAppObj.ClickOnNExtButton_Description_whenPhotosUploaded();
		ChromelessAppObj.SelectCategory("");
		String pin_dropped_address = ChromelessAppObj.SelectMap("Yes");
		ChromelessAppObj.EnterCitizenInfo("No", "Praful", "Kolhe", "Praful.kolhe+citizen@gmail.com");
		Assert.assertTrue(ChromelessAppObj.ContactInfo_Location.getText().equals(pin_dropped_address));
	}

	//@Test(priority = 15)
	public void Verify_Location_when_pin_not_dropped() throws InterruptedException {
		ChromelessAppObj.EnterDescription("This is first issue description");
		ChromelessAppObj.UploadMultiplePhotosbyCitizen();
		ChromelessAppObj.ClickOnNExtButton_Description_whenPhotosUploaded();
		ChromelessAppObj.SelectCategory("");
		String pin_dropped_address = ChromelessAppObj.SelectMap("No");
		ChromelessAppObj.EnterCitizenInfo("No", "Praful", "Kolhe", "Praful.kolhe+citizen@gmail.com");
		Assert.assertTrue(ChromelessAppObj.ContactInfo_Location.getText().equals(pin_dropped_address));
	}

	//@Test(priority = 16)
	public void VerifyIf_Photo_thumbnail_are_appearing_when_multiple_photos_are_uploaded() throws InterruptedException {
		ChromelessAppObj.EnterDescription("This is first issue description");
		//ChromelessAppObj.UploadMultiplePhotosbyCitizen();
		ChromelessAppObj.Upload150PhotosbyCitizen();
		ChromelessAppObj.ClickOnNExtButton_Description_whenPhotosUploaded();
		ChromelessAppObj.SelectCategory("");
		ChromelessAppObj.SelectMap("Yes");
		ChromelessAppObj.EnterCitizenInfo("No", "Praful", "Kolhe", "Praful.kolhe+citizen@gmail.com");
		int Number_of_photo_thumbnail = ChromelessAppObj.Get_count_of_photo_thumbnails();
		System.out.println(Number_of_photo_thumbnail);
		Assert.assertTrue(Number_of_photo_thumbnail == 3);
	}

	//@Test(priority = 17)
	public void VerifyIf_Photo_thumbnail_are_appearing_when_No_photos_uploaded() throws InterruptedException {
		ChromelessAppObj.EnterDescription("This is first issue description");
		ChromelessAppObj.ClickOnNExtButton_Description_WithoutPhotos();
		ChromelessAppObj.SelectCategory("");
		ChromelessAppObj.SelectMap("Yes");
		ChromelessAppObj.EnterCitizenInfo("No", "Praful", "Kolhe", "Praful.kolhe+citizen@gmail.com");
		int Number_of_photo_thumbnail = ChromelessAppObj.Get_count_of_photo_thumbnails();
		System.out.println(Number_of_photo_thumbnail);
		Assert.assertTrue(Number_of_photo_thumbnail == 0);
	}

	//@Test(priority = 18)
	public void Verify_If_Incorrect_file_is_uploaded() throws InterruptedException {
		ChromelessAppObj.EnterDescription("This is first issue description");
		ChromelessAppObj.Upload_invalid_format_Txt_file();
		ChromelessAppObj.ClickOnNExtButton_Description_whenPhotosUploaded();
		Assert.assertTrue(ChromelessAppObj.Get_notification_Message_for_photos()
				.equals("One/more files with unsupported format is/are discarded."));
	}

	//@Test(priority = 19)
	public void Verify_If_File_moreThan_15MB_is_uploaded() throws InterruptedException {
		ChromelessAppObj.EnterDescription("This is first issue description");
		ChromelessAppObj.Upload_file_More_than_15_mb();
		ChromelessAppObj.ClickOnNExtButton_Description_whenPhotosUploaded();
		Assert.assertTrue(
				ChromelessAppObj.Get_notification_Message_for_photos().equals("File size cannot exceed 15MB"));
	}

	//@Test(priority = 20)
	public void Verify_location_mandatory_feature() throws InterruptedException {
		ChromelessAppObj.EnterDescription("This is first issue description");
		ChromelessAppObj.UploadMultiplePhotosbyCitizen();
		ChromelessAppObj.ClickOnNExtButton_Description_whenPhotosUploaded();
		ChromelessAppObj.SelectCategory("Location Required");
		ChromelessAppObj.NextButton_Location.click();
		Assert.assertTrue(ChromelessAppObj.Location_field_Error_message.getText().equals("The location is required."));

	}

	//@Test(priority = 20)
	public void Verify_behavior_when_category_has_no_Location() throws InterruptedException {
		ChromelessAppObj.EnterDescription("Check if Location is present or not");
		ChromelessAppObj.UploadMultiplePhotosbyCitizen();
		ChromelessAppObj.ClickOnNExtButton_Description_whenPhotosUploaded();
		ChromelessAppObj.SelectCategory("Category_with_Loc_No");
		// Commenting the operations to be performed on location tile
		// ChromelessAppObj.NextButton_Location.click();
		ChromelessAppObj.EnterCitizenInfo("No", "Praful", "Kolhe", "Praful.kolhe+citizen@gmail.com");
		Assert.assertFalse(ChromelessAppObj.check_existance_of_field(ChromelessAppObj.ContactInfo_Location));
	}

	//@Test(priority = 21)
	public void Verify_issue_description_with_numbers() throws InterruptedException {
		ChromelessAppObj.EnterDescription("123 issue description starting with numbers");
		ChromelessAppObj.UploadMultiplePhotosbyCitizen();
		ChromelessAppObj.ClickOnNExtButton_Description_whenPhotosUploaded();
		ChromelessAppObj.SelectCategory("");
		ChromelessAppObj.SelectMap("No");
		ChromelessAppObj.EnterCitizenInfo("No", "", "", "");
		softAssertion.assertTrue(ChromelessAppObj.ContactInfo_Firstname.getText().equals("-"));
		softAssertion.assertTrue(ChromelessAppObj.ContactInfo_LastName.getText().equals("-"));
		softAssertion.assertTrue(ChromelessAppObj.ContactInfo_Email.getText().equals("-"));
		softAssertion.assertAll();
	}

	//@Test(priority = 22)
	public void CreateIssueWithPhoto() throws InterruptedException {
		ChromelessAppObj.EnterDescription("This is first issue description");
		ChromelessAppObj.UploadMultiplePhotosbyCitizen();
		ChromelessAppObj.ClickOnNExtButton_Description_whenPhotosUploaded();
		ChromelessAppObj.ClickOnShowMore();
		for (WebElement Category : ChromelessAppObj.CategoryTubes) {
			System.out.println(Category.getText());
		}
		ChromelessAppObj.NextButton_SelectCategory.click();
		Thread.sleep(3000);
		ChromelessAppObj.NextButton_Location.click();
		Thread.sleep(3000);
		ChromelessAppObj.EnterCitizenInfo("No", "Praful", "Kolhe", "Test123@gmail.com");
		ChromelessAppObj.SubmitIssue_button.click();
		Thread.sleep(2000);
		Assert.assertTrue(ChromelessAppObj.ConfirmationMessage.getText().contains("Issue submitted successfully"));
	}

	
	//@Test(priority = 23)
		public void CRM_057Verify_Current_location_feature() throws InterruptedException {
			ChromelessAppObj.EnterDescription("Validate Current Location");
			ChromelessAppObj.UploadMultiplePhotosbyCitizen();
			ChromelessAppObj.ClickOnNExtButton_Description_whenPhotosUploaded();
			ChromelessAppObj.SelectCategory("");
			String pin_dropped_address = ChromelessAppObj.Select_current_location();
			System.out.println("Current Address is "+ pin_dropped_address);
			ChromelessAppObj.NextButton_Location.click();
			softAssertion.assertTrue(ChromelessAppObj.Location_field_Error_message.getText().equals("Missing mandatory location field(s) - Zip"));
			softAssertion.assertTrue(pin_dropped_address.contains("Nagpur"));
			softAssertion.assertAll();
		}
	
	//@Test(priority = 24)
	public void CRM_055Verify_Find_me_icon() throws InterruptedException {
		ChromelessAppObj.EnterDescription("Find Me icon");
		ChromelessAppObj.UploadMultiplePhotosbyCitizen();
		ChromelessAppObj.ClickOnNExtButton_Description_whenPhotosUploaded();
		ChromelessAppObj.SelectCategory("");
		ChromelessAppObj.Find_me_icon.click();
		String pin_dropped_address = ChromelessAppObj.SelectMap("Yes");
		softAssertion.assertTrue(ChromelessAppObj.Location_field_Error_message.getText().equals("Missing mandatory location field(s) - Zip"));
		softAssertion.assertTrue(pin_dropped_address.contains("Nagpur"));
	}
}
