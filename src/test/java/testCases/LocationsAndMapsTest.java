package testCases;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import org.testng.AssertJUnit;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.qa.Analyzer.Retriable;
import com.qa.comcate.base.TestBase;
import com.qa.comcate.pages.AgenciesLandingPage;
import com.qa.comcate.pages.AgencySetupPage;
import com.qa.comcate.pages.DashboardPage;
import com.qa.comcate.pages.LoginPage;
import com.qa.comcate.utils.CommonUtils;

public class LocationsAndMapsTest extends TestBase {

	LoginPage LoginPageObj;
	AgenciesLandingPage AgenciesLandingPageObj;
	DashboardPage DashboardPageObj;
	AgencySetupPage AgencySetupPageObj;
	
	CommonUtils utils;

	public LocationsAndMapsTest() {
		super();
	}

	@BeforeMethod()
	//@Retriable(attempts = 4)
	public void setup() throws Exception {
		initiate();
		LoginPageObj = new LoginPage();
		AgenciesLandingPageObj = new AgenciesLandingPage();
		DashboardPageObj = new DashboardPage();
		AgencySetupPageObj = new AgencySetupPage();
		utils = new CommonUtils();
		LoginPageObj.LoginToApplicationWithValid("support@comcate.com", "test@123");
		System.out.println(utils.ReadFromFile());
		WebElement SearchedAgency = AgenciesLandingPageObj.SearchAgency(utils.ReadFromFile());
		Thread.sleep(2000);
		Assert.assertEquals(SearchedAgency.getText(), utils.ReadFromFile());
		AgenciesLandingPageObj.LoginAsSuperAdmin();
	}

	@AfterMethod
	public void tearDown() {
		driver.quit();
	}

	@Test()
	public void UploadParcelFile() throws InterruptedException, IOException 
	{
		
		DashboardPageObj.ClickonGearIcon();
		AgencySetupPageObj.ClickonLocationsAndMaps();
		AgencySetupPageObj.ClickonMapSettings();
		if (AgencySetupPageObj.Parcel_Filename.getText().contains("Texas_City.gdb.zip")) {
			Assert.assertTrue(true);
		}
		else {
			AgencySetupPageObj.UploadParcel();
		}	
		//AgencySetupPageObj.EnterLatLong("-94.8992156982421900000000000", "29.3833717078824430000000000");
	}
	
	
}
