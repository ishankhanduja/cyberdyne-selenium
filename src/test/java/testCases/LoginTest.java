package testCases;

import java.lang.reflect.Method;

import org.testng.AssertJUnit;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.qa.Analyzer.Retriable;
import com.qa.comcate.base.TestBase;
import com.qa.comcate.pages.AgenciesLandingPage;
import com.qa.comcate.pages.LoginPage;
import com.qa.comcate.utils.CommonUtils;


public class LoginTest extends TestBase {
	

	LoginPage LoginPageObj;
	AgenciesLandingPage AgenciesLandingPageObj;
	CommonUtils utils;
	String error_message;

	//private int counter = 1;

	public LoginTest() {
		super();
	}

	@BeforeMethod()
	@Retriable(attempts = 4)
	public void setup(Method method) {
				initiate();
				System.out.println("Test name: " + method.getName());
		//		AssertJUnit.assertEquals(2, counter++);
				LoginPageObj = new LoginPage();
				AgenciesLandingPageObj = new AgenciesLandingPage();
				utils = new CommonUtils();
				error_message = "";				
		}				
		

	@AfterMethod()
	public void tearDown() {
		driver.quit();
	}

	@Test
	public void LoginToApplicationWithValidCred() {
		LoginPageObj.LoginToApplicationWithValid("support@comcate.com", "test@123");
		AssertJUnit.assertEquals(AgenciesLandingPageObj.createButtonIsdisplayed(), true);
	}

	@Test
	public void LoginToApplicationWithInvalidCred() {
		LoginPageObj.LoginToApplicationWithInvalidDetails("support@comcate.com", "test@1234");
		error_message = utils.WaitForElementToBePresence(LoginPageObj.IncorrectDetails).getText();
		AssertJUnit.assertEquals(error_message, "Password did not match.");
	}

	@Test
	public void LoginToApplicationWithInvalidEmailID() throws InterruptedException {
		LoginPageObj.LoginToApplicationWithInvalidDetails("support1@comcate.com", "test@123");
		error_message = utils.WaitForElementToBePresence(LoginPageObj.IncorrectDetails).getText();
		AssertJUnit.assertEquals(error_message, "Could not find the account");
	}

	@Test
	public void LoginToApplicationWithEmailFieldMissing() throws InterruptedException {
		LoginPageObj.LoginToApplicationWithInvalidDetails("", "test@1234");
		AssertJUnit.assertEquals(LoginPageObj.ValidationMessageForLoginEmailField.getText(), "The Email is required.");
	}

	@Test
	public void LoginToApplicationWithPasswordFieldMissing() throws InterruptedException {
		LoginPageObj.LoginToApplicationWithInvalidDetails("support@comcate.com", "");
		AssertJUnit.assertEquals(LoginPageObj.ValidationMessageForLoginPasswordField.getText(), "The Password is required.");
	}
	
	@Test()
	public void LoginToApplicationWithsqlinEmail() throws InterruptedException {
		LoginPageObj.LoginToApplicationWithInvalidDetails("select*from", "test@123");
		AssertJUnit.assertEquals(LoginPageObj.ValidationMessageForLoginEmailField.getText(), "Enter Valid Email");
	}
	

}
